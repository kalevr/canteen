<?php

// MySQL/MariaDB details
$config['DB_USER'] = '';
$config['DB_PASSWORD'] = '';
$config['DB_NAME'] = 'canteen'; // if using multiple copies of the site, every DB_NAME must be different
$config['DB_SERVER'] = '127.0.0.1';

// prefix session variable names (important if hosting multiple copies of the site on the same server.. prefix must be unique)
$config['SESSION_PREFIX'] = 'wut';

// Menu URL (opens in new window)
$config['MENU_URL'] = '';

// if site is not at website root, specify folder name (example.com/foldername). Should be '/foldername' (or empty string if on root)
$config['SITE_SUBDIR'] = '';

// roles that can edit meals at any time (no time restriction)
$config['EDIT_ANY_TIME_ROLES'] = ['admin', 'observer'];

// Don't try to update automatically if false (good on dev machines)
$config['AUTO_UPDATE_ENABLED'] = true;
$config['LAST_UPDATE_FAILED'] = false;

// does school check user participation (does users check in if they show up for a meal)
$config['PARTICIPATION_DISABLED'] = true;

// --------------------- DON'T CHANGE ANYTHING BEYOND THIS POINT ------------------------------------
// version.txt timestamp (last updated - changed automatically at update)
$config['VERSION_FILE_TIMESTAMP'] = 1696839703;
$config['IS_INSTALLED'] = false;