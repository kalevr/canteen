<?php

if (!defined('DIRECT_ACCESS')) {
    http_response_code(403);
    exit();
}