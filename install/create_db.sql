-- ---------------------------------------------------------------------------------------------------------------------
-- --------------------------------------------- CREATE ALL TABLES -----------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS app_setting (
    id INT(11) NOT NULL AUTO_INCREMENT,
    setting_code VARCHAR(50) NOT NULL UNIQUE COMMENT 'Unique setting code',
    description VARCHAR (200) NULL,
    setting_value VARCHAR (200) NULL,
    PRIMARY KEY (`id`) USING BTREE
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS app_user (
    id INT(11) NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(50) NULL,
    last_name VARCHAR(50) NULL,
    email VARCHAR(110) NOT NULL UNIQUE,
    passwd VARCHAR(255) NULL,
    locked SMALLINT NULL DEFAULT '0' COMMENT 'Is user account locked?',
    comments VARCHAR(200) NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_app_user_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_app_user_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS app_year (
    id INT(11) NOT NULL AUTO_INCREMENT,
    display_name VARCHAR(20) NULL,
    start_month INT(2) NOT NULL,
    start_year INT(4) NOT NULL,
    end_month INT(2) NOT NULL,
    end_year INT(4) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_app_year_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_app_year_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS app_role (
    id INT(11) NOT NULL AUTO_INCREMENT,
    role_name VARCHAR(20) NULL,
    role_code VARCHAR(20) NOT NULL UNIQUE,
    comments VARCHAR(200) NULL,
    PRIMARY KEY (`id`) USING BTREE
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS app_group (
    id INT(11) NOT NULL AUTO_INCREMENT,
    app_year INT(11) NOT NULL,
    group_name VARCHAR(20) NULL,
    starting_from DATE NOT NULL,
    valid_until DATE NULL,
    sort_order INT(11) UNSIGNED NULL DEFAULT 9999999,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_app_group_2_app_year` FOREIGN KEY (`app_year`) REFERENCES `app_year` (`id`),
    CONSTRAINT `fk_app_group_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_app_group_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS kitchen (
    id INT(11) NOT NULL AUTO_INCREMENT,
    kitchen_name VARCHAR(20) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_kitchen_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_kitchen_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS keycard (
    id INT(11) NOT NULL AUTO_INCREMENT,
    visible_nr VARCHAR(20) NULL,
    scannable_nr VARCHAR(20) NOT NULL,
    app_user INT(11) NOT NULL,
    starting_from DATE NULL,
    valid_until DATE NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_keycard_2_app_user` FOREIGN KEY (`app_user`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_keycard_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_keycard_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS app_user_role (
    id INT(11) NOT NULL AUTO_INCREMENT,
    app_user INT(11) NOT NULL,
    app_role INT(11) NOT NULL,
    starting_from DATE NOT NULL,
    valid_until DATE NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_app_user_role_2_app_user` FOREIGN KEY (`app_user`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_app_user_role_2_app_role` FOREIGN KEY (`app_role`) REFERENCES `app_role` (`id`),
    CONSTRAINT `fk_app_user_role_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_app_user_role_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS app_user_group (
    id INT(11) NOT NULL AUTO_INCREMENT,
    app_user INT(11) NOT NULL,
    app_group INT(11) NOT NULL,
    app_role INT(11) NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_app_user_group_2_app_user` FOREIGN KEY (`app_user`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_app_user_group_2_app_group` FOREIGN KEY (`app_group`) REFERENCES `app_group` (`id`),
    CONSTRAINT `fk_app_user_group_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_app_user_group_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS meal_type (
    id INT(11) NOT NULL AUTO_INCREMENT,
    meal_type_name VARCHAR(20) NULL,
    meal_time VARCHAR(20) NULL,
    kitchen INT(11) NOT NULL,
    registration_cutoff SMALLINT NULL,
    comments VARCHAR(200) NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_meal_type_2_kitchen` FOREIGN KEY (`kitchen`) REFERENCES `kitchen` (`id`),
    CONSTRAINT `fk_meal_type_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_meal_type_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS group_meal_type (
    id INT(11) NOT NULL AUTO_INCREMENT,
    app_group INT(11) NOT NULL,
    meal_type INT(11) NOT NULL,
    starting_from DATE NOT NULL,
    valid_until DATE NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_group_meal_type_2_app_group` FOREIGN KEY (`app_group`) REFERENCES `app_group` (`id`),
    CONSTRAINT `fk_group_meal_type_2_meal_type` FOREIGN KEY (`meal_type`) REFERENCES `meal_type` (`id`),
    CONSTRAINT `fk_group_meal_type_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_group_meal_type_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS kitchen_view (
    id INT(11) NOT NULL AUTO_INCREMENT,
    app_year INT(11) NOT NULL,
    kitchen_view_name VARCHAR(20) NOT NULL,
    meal_type INT(11) NOT NULL,
    starting_from DATE NOT NULL,
    valid_until DATE NULL,
    sort_order INT(11) UNSIGNED NULL DEFAULT 9999999,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_kitchen_view_2_app_year` FOREIGN KEY (`app_year`) REFERENCES `app_year` (`id`),
    CONSTRAINT `fk_kitchen_view_2_meal_type` FOREIGN KEY (`meal_type`) REFERENCES `meal_type` (`id`),
    CONSTRAINT `fk_kitchen_view_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_kitchen_view_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS group_kitchen_view (
    id INT(11) NOT NULL AUTO_INCREMENT,
    kitchen_view INT(11) NOT NULL,
    app_group INT(11) NOT NULL,
    starting_from DATE NOT NULL,
    valid_until DATE NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_group_kitchen_view_2_kitchen_view` FOREIGN KEY (`kitchen_view`) REFERENCES `kitchen_view` (`id`),
    CONSTRAINT `fk_group_kitchen_view_2_app_group` FOREIGN KEY (`app_group`) REFERENCES `app_group` (`id`),
    CONSTRAINT `fk_group_kitchen_view_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_group_kitchen_view_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS registration (
    id INT(11) NOT NULL AUTO_INCREMENT,
    meal_type INT(11) NOT NULL,
    app_user INT(11) NOT NULL,
    meal_date DATE NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_registration_2_meal_type` FOREIGN KEY (`meal_type`) REFERENCES `meal_type` (`id`),
    CONSTRAINT `fk_registration_2_app_user` FOREIGN KEY (`app_user`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_registration_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_registration_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS participation (
    id INT(11) NOT NULL AUTO_INCREMENT,
    meal_type INT(11) NOT NULL,
    app_user INT(11) NOT NULL,
    meal_date DATE NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_participation_2_meal_type` FOREIGN KEY (`meal_type`) REFERENCES `meal_type` (`id`),
    CONSTRAINT `fk_participation_2_app_user` FOREIGN KEY (`app_user`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_participation_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_participation_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS log_login (
    id INT(11) NOT NULL AUTO_INCREMENT,
    app_user INT(11) NULL,
    email VARCHAR(110) NULL,
    ip_addr VARCHAR(50) NULL,
    login_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    success SMALLINT NULL,
    user_agent VARCHAR(200) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_log_login_2_app_user` FOREIGN KEY (`app_user`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS irregular_dates (
    id INT(11) NOT NULL AUTO_INCREMENT,
    irr_date Date NOT NULL,
    is_included SMALLINT NOT NULL DEFAULT 0,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    deleted_at TIMESTAMP NULL,
    created_by INT(11) NULL,
    deleted_by INT(11) NULL,
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_irregular_dates_2_app_user1` FOREIGN KEY (`created_by`) REFERENCES `app_user` (`id`),
    CONSTRAINT `fk_irregular_dates_2_app_user2` FOREIGN KEY (`deleted_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS app_log (
    id INT(11) NOT NULL AUTO_INCREMENT,
    tbl_name VARCHAR(50) NOT NULL COMMENT 'Name of the table that was edited',
    record_id INT(11) NOT NULL COMMENT 'Id of the record edited',
    col_name VARCHAR(50) NULL COMMENT 'Name of the column that was changed',
    old_value VARCHAR(200) NULL COMMENT 'Value that was removed!',
    new_value VARCHAR(200) NULL COMMENT 'Value that was added',
    edited_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT 'Timestamp when the edit took place',
    edited_by INT(11) NULL COMMENT 'Id of the user who made the edit',
    PRIMARY KEY (`id`) USING BTREE,
    CONSTRAINT `fk_app_log_2_app_user` FOREIGN KEY (`edited_by`) REFERENCES `app_user` (`id`)
    )
    ENGINE=InnoDB
;


-- ---------------------------------------------------------------------------------------------------------------------
-- ------------------------------------------ INITIALIZE DEFAULT DATA --------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

-- creating roles
INSERT INTO app_role (role_name, role_code, comments)
VALUES ('Admin', 'admin', 'This is the main administrator role');

INSERT INTO app_role (role_name, role_code, comments)
VALUES ('Observer', 'observer', 'This is the sub-admin role (can view/edit most things, but not all)');

INSERT INTO app_role (role_name, role_code, comments)
VALUES ('Cook', 'cook', 'This is the cook');

INSERT INTO app_role (role_name, role_code, comments)
VALUES ('Homeroom-teacher', 'homeroom-teacher', 'This is the homeroom-teacher role (who manages his/her group)');

INSERT INTO app_role (role_name, role_code, comments)
VALUES ('Eater', 'eater', 'This is the regular eating person (there has to be a better name for it)');

-- creating users
INSERT INTO app_user (first_name, last_name, email, locked, comments, created_at)
VALUES ('Admin', 'Admin', 'admin@localhost', 0, 'This is the main admin user', '2021-09-01');

-- creating years
INSERT INTO app_year (display_name, start_month, start_year, end_month, end_year, created_at)
VALUES ('2020/21', 9, 2020, 8, 2021, '2021-09-01');

INSERT INTO app_year (display_name, start_month, start_year, end_month, end_year, created_at)
VALUES ('2021/22', 9, 2021, 8, 2022, '2021-09-01');

INSERT INTO app_year (display_name, start_month, start_year, end_month, end_year, created_at)
VALUES ('2022/23', 9, 2022, 8, 2023, '2021-09-01');

-- create Default kitchen
INSERT INTO kitchen (kitchen_name, created_at)
VALUES ('Default Kitchen', '2021-09-01');

-- set user role
INSERT INTO app_user_role (app_user, app_role, starting_from)
VALUES (1, 1, '2021-09-01');

-- set settings (with empty values)
INSERT INTO app_setting (setting_code, description, setting_value)
VALUES ('AD_URL', 'AD server IP or domain name', '');

INSERT INTO app_setting (setting_code, description, setting_value)
VALUES ('AD_user', 'AD service account username', '');

INSERT INTO app_setting (setting_code, description, setting_value)
VALUES ('AD_pass', 'AD service account password', '');

-- ---------------------------------------------------------------------------------------------------------------------
-- ------------------------------------------ TEST DATA FOR DEVELOPMENT ------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

-- creating users
INSERT INTO app_user (first_name, last_name, email, locked, comments, created_at, created_by)
VALUES ('Kl8', 'Juhataja', 'kl8.juh@localhost', 0, '8. kl juhataja', '2021-09-01', 1);

INSERT INTO app_user (first_name, last_name, email, locked, comments, created_at, created_by)
VALUES ('Kl7', 'Juhataja', 'kl7.juh@localhost', 0, '7. kl juhataja', '2021-09-01', 1);

INSERT INTO app_user (first_name, last_name, email, locked, comments, created_at, created_by)
VALUES ('Kl8', 'Õpilane', 'kl8.opil@localhost', 0, '8. kl õpilane', '2021-09-01', 1);

INSERT INTO app_user (first_name, last_name, email, locked, comments, created_at, created_by)
VALUES ('Kl8', 'Opilane2', 'kl8.opil2@localhost', 0, '8. kl õpilane2', '2021-09-01', 1);

INSERT INTO app_user (first_name, last_name, email, locked, comments, created_at, created_by)
VALUES ('Kl7', 'Õpilane', 'kl7.opil@localhost', 0, '7. kl õpilane', '2021-09-01', 1);

-- create app groups
INSERT INTO app_group (group_name, app_year, starting_from, created_at, created_by)
VALUES ('8. klass', 2, '2021-09-01', '2021-09-01', 1);

INSERT INTO app_group (group_name, app_year, starting_from, created_at, created_by)
VALUES ('7. klass', 2, '2021-09-01', '2021-09-01', 1);

-- set app user group
INSERT INTO app_user_group (app_user, app_group, app_role, created_at, created_by)
VALUES (2, 1, 4, '2021-09-01', 1);

INSERT INTO app_user_group (app_user, app_group, app_role, created_at, created_by)
VALUES (3, 2, 4, '2021-09-01', 1);

INSERT INTO app_user_group (app_user, app_group, app_role, created_at, created_by)
VALUES (4, 1, 5, '2021-09-01', 1);

INSERT INTO app_user_group (app_user, app_group, app_role, created_at, created_by)
VALUES (5, 1, 5, '2021-09-01', 1);

INSERT INTO app_user_group (app_user, app_group, app_role, created_at, created_by)
VALUES (6, 2, 5, '2021-09-01', 1);

-- set user role
INSERT INTO app_user_role (app_user, app_role, starting_from, created_at, created_by)
VALUES (2, 4, '2021-09-01', '2021-09-01', 1);


-- create meal type
INSERT INTO meal_type (meal_type_name, meal_time, kitchen, registration_cutoff, comments, created_at, created_by)
VALUES ('Lunch', '12:25', 1, 24, 'The Default meal type', '2021-09-01', 1);

-- create kitchen view
-- INSERT INTO kitchen_view (kitchen_view_name, meal_type, starting_from, created_at, created_by)
-- VALUES ('5. - 9. klass', 1, '2021-09-01', '2021-09-01', 1);

-- set group metal type
INSERT INTO group_meal_type (app_group, meal_type, starting_from, created_at, created_by)
VALUES (1, 1, '2021-09-01', '2021-09-01', 1);

-- create group kitchen view
INSERT INTO group_kitchen_view (kitchen_view, app_group, starting_from, created_at, created_by)
VALUES (1, 1, '2021-09-01', '2021-09-01', 1);