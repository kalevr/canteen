// selectors (that repeat multiple times)
const IMPORT_AREA = '#import-area';
const BTN_SAVE = '#btn-save';
const MEAL_DATA = '#meal-data';
const CARDS_DATA = '.cards-data'


$(function () {
    // toggle single cell on/off
    $('.dates').on('click', function (event) {
        toggleIrregularCell(event.target);
    });

    $('.irregular-save-btn').on('click', function (event) {
        saveIrregularDates(event);
    });

    // toggle entire row on/off
    $('.toggle-row').on('click', function () {
        toggleRow(this);
    });

    // toggle entire column on/off
    $('.toggle-col').on('click', function () {
        toggleCol(this);
    });

    // toggle all rows/columns on/off
    $('.toggle-all').on('click', function () {
        toggleAll(this);
    });

    $('.participation-name').on('click', function (event) {
        toggleParticipation(event);
    });

    if ($('h4.meal_title_item')) {
        totalMealsInRow();
        totalMealsInCol();
        totalMealsInMonth();
    }

    // load onScan script if id exists
    if ($('#participations-now').length) {
        startScanner();
    }

    if ($(IMPORT_AREA).length) {
        resizeTextarea();
    }

    $('.user-select-ajax').select2({
        ajax: {
            url: $('#users_api_url').val(),
            delay: 250,
            dataType: 'json',
            type: 'GET',
            data: function (params) {
                return {
                    q: params.term // search term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    }),
                };
            }
        },
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        language: {
            inputTooShort: function () {
                return '';
            }
        }
    });


    $('.group-select-ajax').select2({
        ajax: {
            url: $('#groups_api_url').val(),
            delay: 250,
            dataType: 'json',
            type: 'GET',
            data: function (params) {
                return {
                    q: params.term // search term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.group_name,
                            id: item.id
                        }
                    }),
                };
            }
        },
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        language: {
            inputTooShort: function () {
                return '';
            }
        }
    });

    $(IMPORT_AREA).on('input', function () {
        resizeTextarea(this);
    })
});

function toggleIrregularCell(event) {
    if (!$(BTN_SAVE).hasClass('hidden')) {
        if (!$(BTN_SAVE).hasClass('hidden')) {
            const colors = ['disabled-bg', 'enabled-bg', 'no-bg', 'warning-bg'];
            const value = $(event).children('input').val().trim();
            const total = parseInt($(event).siblings('.row_total').text());
            const addToTotal = value === '1' || value === '3' ? -1 : 1;

            $(event).removeClass(colors[value]);
            if (value >= 2) {
                $(event).children('input').val(5 - value);
                $(event).addClass(colors[5 - value]);
            } else {
                $(event).children('input').val(1 - value);
                $(event).addClass(colors[1 - value]);
            }
            $(event).siblings('.row_total').text(total + addToTotal);

            if ($('.toggle-col').length) {
                let day = $(event).text().trim();
                const totalSelector = $('#col_total_' + day);
                const colTotal = parseInt(totalSelector.text());
                totalSelector.text(colTotal + addToTotal);
            }

            totalMealsInMonth();
        }
    }
}

function cancel() {
    location.reload();
}

function resizeTextarea() {
    const element = $(IMPORT_AREA)[0];
    element.style.height = 'auto';
    element.style.height = (element.scrollHeight) + 2 + 'px';
}

function edit() {
    $(BTN_SAVE).removeClass('hidden');
    $('#btn-cancel').removeClass('hidden');
    $('.btn-select-all').removeClass('hidden');
    $('#arrow-row').removeClass('hidden');
    $('.arrow-col').removeClass('hidden');
    $('#btn-edit').addClass('hidden');
    $('.arrow').children().removeAttr('href');
    $('.dates').removeClass('half-visible');
    $('.toggle-row,.toggle-col').children('i').removeClass('hidden');
}

function totalMealsInRow() {
    let total = 0;
    $('.meal-row').each(function (index, element) {
        total = 0;
        $(element).children().children('input').each(function () {
            if ($(this).val() === '1' || $(this).val() === '3') {
                total++;
            }
        });
        $(element).children('.row_total').text(total);
    });
}

function totalMealsInCol() {
    $('[class^=col_]').each(function () {
        const classNames = this.className.match(/col_\d+/);
        const day = classNames[0].split('_')[1];

        let total = 0;

        $('.day_' + day).each(function () {
            let value = $(this).children('input').val();

            if (value === '1' || value === '3') {
                total++;
            }
        });

        $('#col_total_' + day).text(total);
    });
}

function totalMealsInMonth() {
    let total = 0;
    $('.total-row').children('.toggle-col').each(function () {
        total += parseInt($(this).text());
    });

    $('#grand-total').text(total);
}

function toggleRow(event) {
    const colors = ['disabled-bg', 'enabled-bg'];
    const arrowColors = ['disabled-arrow', 'enabled-arrow'];
    const arrow = $(event).hasClass('enabled-arrow') ? 1 : 0;

    $(event).siblings('.dates').each(function (index, element) {
        $(element).children('input').val(arrow);
        $(element).removeClass(colors[1 - arrow]).addClass(colors[arrow]);
    });

    $(event).removeClass(arrowColors[arrow]).addClass(arrowColors[1 - arrow]);
    totalMealsInRow();
    totalMealsInCol();
    totalMealsInMonth();
}

function toggleCol(event) {
    const colors = ['disabled-bg', 'enabled-bg'];
    const arrowColors = ['disabled-arrow', 'enabled-arrow'];
    const arrow = $(event).hasClass('enabled-arrow') ? 1 : 0;

    const className = event.className.match(/toggle_day_\d+/);
    const day = className[0].split('_')[2]

    $('.day_' + day).each(function () {
        $(this).children('input').val(arrow);
        $(this).removeClass(colors[1 - arrow]).addClass(colors[arrow]);
    });

    $(event).removeClass(arrowColors[arrow]).addClass(arrowColors[1 - arrow]);
    totalMealsInCol();
    totalMealsInRow();
    totalMealsInMonth();
}

function toggleAll(event) {
    const colors = ['disabled-bg', 'enabled-bg'];
    const arrowColors = ['disabled-arrow', 'enabled-arrow'];
    const arrow = $(event).hasClass('enabled-arrow') ? 1 : 0;

    $('.meal-row').each(function (index, element) {
        //console.log('index: ', index, ' - element: ', element);

        $(element).children('.dates').each(function (index2, element2) {
            $(element2).children('input').val(arrow);
            $(element2).removeClass(colors[1 - arrow]).addClass(colors[arrow]);
        });
    });

    $(event).removeClass(arrowColors[arrow]).addClass(arrowColors[1 - arrow]);
    totalMealsInRow();
    totalMealsInCol();
    totalMealsInMonth();
}

function selectMonthMobile(element) {
    const idName = element.id;
    const id = idName.split("-")[2];
    const colors = ['disabled-bg', 'enabled-bg'];
    const btnColors = ['disable-btn', 'enable-btn'];
    const btn = $(element).hasClass('enable-btn') ? 1 : 0;

    $(".user_" + id).each(function () {
        $(this).children('input').val(1 - btn)
        $(this).removeClass(colors[btn]).addClass(colors[1 - btn]);
    });

    $(element).removeClass(btnColors[btn]).addClass(btnColors[1 - btn]);
}

function startScanner() {
    // initialize scanner (with options)
    onScan.attachTo(document, {
        reactToPaste: true,
        onScan: function(sCode, iQty) { // Alternative to document.addEventListener('scan')
            let mealType = $(MEAL_DATA).data('mealtype');
            let date = $(MEAL_DATA).data('date');

            registerParticipation(sCode, mealType, date);

        } /*,
        onKeyDetect: function(iKeyCode){ // output all potentially relevant key events - great for debugging!
            console.log('Pressed: ' + iKeyCode);
        }
        */
    });
}

function registerParticipation(sCode, mealType, date) {
    if (sCode == null || sCode == '' || mealType == null || mealType == '' || date == null || date == '') throw "exit";

    const data = {
        sCode: sCode,
        mealtype: mealType,
        date: date
    };

    $.ajax({
        url: $('#participation_api_url').val(),
        delay: 250,
        dataType: 'json',
        type: 'POST',
        data: data,
        success: function (data) {
            changeParticipationColors(data.uid, data.status, data.name, data.message);
        },
        error: function (data) {
            console.log('error: ', data);
        }
    });
}

function toggleParticipation(event) {
    const userId = $(event.target).attr('id');
    toggleParticipationById(userId);
}

function toggleParticipationById(userId) {
    if(userId == null || userId == '') throw "exit";

    const mealType = $(MEAL_DATA).data('mealtype');

    const data = {
        userid: userId,
        mealtype: mealType
    };

    $.ajax({
        url: $('#participation_api2_url').val(),
        delay: 250,
        dataType: 'json',
        type: 'POST',
        data: data,
        success: function (data) {
            changeParticipationColors(data.uid, data.status, data.name, data.message);
        },
        error: function (data) {
            console.log('error: ', JSON.stringify(data, null, 2));
        }
    });
}

function changeParticipationColors(userId, status, name, message) {
    const classes = ['participation-missing-noreg', 'participation-missing', 'participation-ok', 'participation-noreg'];

    $('#name').text(name);
    $('#message').text(message);
    let icon = '';

    switch (status) {
        case 1:
            icon = '<i class="fa-solid fa-circle-question"></i>';
            break;
        case 2:
            icon = '<i class="fa-solid fa-circle-check"></i>';
            break;
        case 3:
            icon = '<i class="fa-solid fa-square-check"></i>';
            break;

    }

    if ([0, 1, 2, 3].includes(status)) {
        const userSelector = '#' + userId;
        const oldClass = $(userSelector).attr('class');
        $(userSelector).removeClass(oldClass).addClass('participation-name cursor-pointer ' + classes[status]);
        $(userSelector).children('.participation-icon').empty().html(icon);
    }
}

function saveIrregularDates(event) {
    const date = $('#irr_date').val();
    let totalMeals = 0;
    const inputElements = [];


    $('.dates').each(function () {

        if ($(this).children('input').val() == 0 && $(this).data('total') > 0) {
            totalMeals += $(this).data('total');
            let thisDate = date.slice(0, 8) + $(this).attr('id').split('_')[1];
            inputElements.push('<input type="hidden" id="delete_meals[]" name="delete_meals[]" value="' + thisDate + '" >')
        }
    });

    const plural = totalMeals == 1 ? 'söögikord' : 'söögikorda';

    if (totalMeals) {
        // alternative would be to show message with Bootstrap 5 Modal
        if (!confirm('Oled sa kindel? (' + totalMeals + ' ' + plural + ' kustutatakse ära).')) {
            event.preventDefault();
        } else {
            inputElements.forEach(function (item, index) {
                $('#irregular-form').append(item);
            });
        }
    }
}

function searchUsers(name) {
    const results = $('#search_results');
    const site_subdir = $('#site-subdir').val();
    if (name.length > 2) {
        $.ajax({
            url: $('#users_api_url').val(),
            delay: 250,
            dataType: 'json',
            type: 'GET',
            data: {
                    q: name, // search term
                    all: true
            },
            success: function (data) {
                results.empty();
                results.show();

                if (data.length && data != '{}') {
                    data.forEach(function (item) {
                        results.append('<a href="' + site_subdir + '/users/edit/' + item.id + '">' + item.name + '</a>');
                    });
                } else {
                    results.hide();
                }
            },
            error: function (data) {
                console.log('error: ', JSON.stringify(data, null, 2));
            }
        });
    } else {
        results.empty();
        results.hide();
    }
}

function searchCards(name, page) {
    const results = $(CARDS_DATA);

    if (name.length > 2 || name.length == 0) {
        $.ajax({
            url: $('#cards_api_url').val(),
            delay: 250,
            dataType: 'json',
            type: 'GET',
            data: {
                q: name, // search term
                page: page
            },
            success: function (data) {
                results.empty();

                if (data && data != '{}' && data['cards']) {
                    // do stuff
                    $(CARDS_DATA).html('');

                    for (const card of data['cards']) {
                        let is_expired = card.is_card_expired == 1 ? ' class="error-msg">' : '>';
                        let is_deleted = card.is_user_deleted == 1 ? ' class="error-msg">' : '>';

                        $(CARDS_DATA).append('<tr>\n' +
                            '<input name="id" type="hidden" value="' + card.id + '">\n' +
                            '  <td>' + (card.visible_nr ? card.visible_nr : '') + '</td>\n' +
                            '  <td>' + card.scannable_nr + '</td>\n' +
                            '  <td>' + card.starting_from + '</td>\n' +
                            '  <td' + is_expired + (card.valid_until ? card.valid_until : '') + '</td>\n' +
                            '  <td' + is_deleted + (card.first_name ? card.first_name : '') + '</td>\n' +
                            '  <td' + is_deleted + (card.last_name ? card.last_name : '') + '</td>\n' +
                            '  <td><a href="' + card.edit_url + '">Muuda</a></td>\n' +
                            '</tr>')
                    }

                } else {
                    // do some other stuff
                    console.log('fail: ', JSON.stringify(data, null, 2));
                    console.log(data.length);
                    $(CARDS_DATA).html('<tr><td colspan="7">Otsing ei andnud ühtegi tulemust.</td></tr>');
                }
            },
            error: function (data) {
                console.log('error: ', data);
                $(CARDS_DATA).html('<tr><td colspan="7">Andmete toomine ebaõnnestus.</td></tr>');
            }
        });
    } else {
        results.empty();
    }
}

function confirmDelete(url) {
    if (confirm("Oled sa kindel")) {
        document.location = url;
    }
}