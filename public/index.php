<?php
/**
 * This PHPDoc makes it so IDE wouldn't show $config as undefined variable
 * @var array $config
 */
const DS = DIRECTORY_SEPARATOR;
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . DS . '..'));
defined('DIRECT_ACCESS') || define('DIRECT_ACCESS', 'not a good idea');

$modelsPath = APPLICATION_PATH . DS . 'src'. DS . 'models' . DS;
$viewsPath = APPLICATION_PATH . DS . 'src'. DS . 'views' . DS;
$controllersPath = APPLICATION_PATH . DS . 'src'. DS . 'controllers' . DS;
$corePath = APPLICATION_PATH . DS . 'src' . DS . 'core' . DS;

if (!file_exists(APPLICATION_PATH . DS . 'config' . DS . 'client_conf.php')) {
    copy(APPLICATION_PATH . DS . 'config' . DS . 'client_conf_example.php',
        APPLICATION_PATH . DS . 'config' . DS . 'client_conf.php');
}

require APPLICATION_PATH . DS . 'config' . DS . 'client_conf.php';
require $corePath . DS . 'Core_Database.php';
require APPLICATION_PATH . DS . 'vendor/autoload.php';
require $controllersPath . 'BaseController.php';
require $modelsPath . 'BaseModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'UpdateApp.php';

// Twig & PHRoute stuff (https://stackoverflow.com/questions/40526990/difference-between-include-extends-use-macro-embed-in-twig)
use Phroute\Phroute\Exception\HttpMethodNotAllowedException;
use Phroute\Phroute\Exception\HttpRouteNotFoundException;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;

$loader = new FilesystemLoader($viewsPath);
$twig = new Environment($loader);

// https://github.com/mrjgreen/phroute

$paths = [
    'home' => $controllersPath . 'HomeController.php',
    'error' => $controllersPath . 'ErrorController.php',
    'kitchens' => $controllersPath . 'KitchenController.php',
    'users' => $controllersPath . 'UserController.php',
    'admin' => $controllersPath . 'AdminController.php',
    'irregular-dates' => $controllersPath . 'IrregularDatesController.php',
    'meals' => $controllersPath . 'MealController.php',
    'groups' => $controllersPath . 'GroupController.php',
    'auth' => $controllersPath . 'AuthController.php',
    'meal-types' => $controllersPath . 'MealTypeController.php',
    'kitchen-views' => $controllersPath . 'KitchenViewController.php',
    'kitchen-meal' => $controllersPath . 'KitchenMealController.php',
    'participations' => $controllersPath . 'ParticipationController.php',
    'reports' => $controllersPath . 'ReportController.php',
    'import' => $controllersPath . 'ImportController.php',
    'cards' => $controllersPath . 'CardController.php',
    'install' => $controllersPath . 'InstallController.php'
];

$root = '';
if (isset($config)) {
    $root = $config['SITE_SUBDIR'];
}

$router = new RouteCollector();

$router->get([$root . '/install', 'install'], function () {
    global $paths;
    require $paths['install'];

    $controller = new InstallController();
    return $controller->getInstall();
});

$router->post([$root . '/install', 'install_post'], function () {
    global $paths;
    require $paths['install'];

    $controller = new InstallController();
    $controller->postInstall();
});

$DB = null;

try {
    $DB = new Core_Database($config['DB_USER'], $config['DB_PASSWORD'], $config['DB_NAME'], $config['DB_SERVER']);
} catch (Exception $e) {
}

// ---------- UPDATE CHECKER ----------
$keyFilePath = APPLICATION_PATH . DS . 'config' . DS . 'crypto.key';
if (file_exists($keyFilePath) && $config['AUTO_UPDATE_ENABLED'] && !$config['LAST_UPDATE_FAILED']) {
    $updater = new UpdateApp();
    $updatesAvailable = $updater->checkForUpdatesLazy();

    if ($updatesAvailable) {
        $updater->installUpdates();
    }
}
// ------------------------------------

// default route
$router->any([$root . '/', 'home'], function () {
    require '../src/controllers/HomeController.php';

    $controller = new HomeController();
    return $controller->getHome();
});

$router->any([$root . '/testing', 'testing'], function () {
    global $paths;
    require $paths['home'];

    $controller = new HomeController();
    return $controller->getTest();
});

$router->any([$root . '/error', 'error'], function () {
    global $paths;
    require $paths['error'];

    $controller = new ErrorController();
    return $controller->getError();
});

$router->get([$root . '/kitchens', 'kitchens'], function () {
    global $paths;
    require $paths['kitchens'];

    $controller = new KitchenController();
    return $controller->getKitchens();
});

$router->get([$root . '/kitchens/edit/{id:i}', 'kitchens_edit'], function (int $id) {
    global $paths;
    require $paths['kitchens'];

    $controller = new KitchenController();
    return $controller->getEdit($id);
});

$router->post([$root . '/kitchens/edit', 'kitchens_edit_post'], function () {
    global $paths;
    require $paths['kitchens'];

    $controller = new KitchenController();
    $controller->postCreateEdit();
});

$router->get([$root . '/users/role/{name:c}?', 'users_by_role'], function ($name = 'admin') {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    return $controller->getRoleUsers($name);
});

$router->get([$root . '/users/group/{id:i}', 'users_by_group'], function ($id) {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    return $controller->getGroupUsers($id);
});

$router->get([$root . '/users/create', 'users_create'], function () {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    return $controller->getCreate();
});

$router->post([$root . '/users/create', 'users_create_post'] , function () {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    $controller->postCreate();
});

$router->get([$root . '/users/edit/{id:i}', 'users_edit'], function ($id) {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    return $controller->getEdit($id);
});

$router->post([$root . '/users/edit', 'users_edit_post'], function () {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    $controller->postEdit();
});

$router->get([$root . '/users/delete/{id}', 'users_delete'], function ($id) {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    $controller->getDelete($id);
});

$router->get([$root . '/users/restore/{id}', 'users_restore'], function ($id) {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    $controller->getRestore($id);
});

$router->get([$root . '/admin', 'admin'], function () {
    global $paths;
    require $paths['admin'];

    $controller = new AdminController();
    return $controller->getAdminMenu();
});

$router->get([$root . '/login', 'login'], function () {
    global $paths;
    require $paths['auth'];

    $controller = new AuthController();
    return $controller->getLogin();
});

$router->post([$root . '/login', 'login_post'], function () {
    global $paths;
    require $paths['auth'];

    $controller = new AuthController();
    $controller->postLogin();
});

$router->get([$root . '/domain-login', 'domain-login'], function () {
    global $paths;
    require $paths['auth'];

    $controller = new AuthController();
    return $controller->getLogin(true);
});

$router->get([$root . '/logout', 'logout'], function () {
    global $paths;
    require $paths['auth'];

    $controller = new AuthController();
    $controller->getLogout();
});

$router->get([$root . '/irregular-dates/{mealType:i}?/{date:c}?', 'irregulars'], function (int $mealType = 0, string $date = '') {
    global $paths;
    require $paths['irregular-dates'];

    $controller = new IrregularDatesController();
    return $controller->getIrregularDates($mealType, $date);
});

$router->post([$root . '/irregular-date', 'irregulars_post'], function () {
    global $paths;
    require $paths['irregular-dates'];

    $controller = new IrregularDatesController();
    $controller->postIrregularDates();
});

$router->get([$root . '/meals/{mealType:i}/{groupId:i}/{date:c}?', 'meals'], function (int $mealType, int $groupId, string $date = '') {
    global $paths;
    require $paths['meals'];

    $controller = new MealController();
    return $controller->getMeals($mealType, $groupId, $date);
});

$router->post([$root . '/meals/mass-save', 'meals_post'], function () {
    global $paths;
    require $paths['meals'];

    $controller = new MealController();
    $controller->postMeals();
});

$router->get([$root . '/meals/history/{mealType:i}/{userId:i}/{date:c}?', 'meal_history'], function (int $mealType, int $userId, string $date = '') {
    global $paths;
    require $paths['meals'];

    $controller = new MealController();
    return $controller->getMealHistoryForUser($mealType, $userId, $date);
});

$router->get([$root . '/my-meals', 'random_my-meal'], function () {
    global $paths;
    require $paths['meals'];

    $controller = new MealController();
    return $controller->getRandomMyMeal();
});

$router->get([$root . '/my-meals/{userId:i}/{mealType:i}/{groupId:i}/{date:c}?', 'my-meals'], function (int $userId, int $mealType, int $groupId, string $date = '') {
    global $paths;
    require $paths['meals'];

    $controller = new MealController();
    return $controller->getMyMeal($userId, $mealType, $groupId, $date);
});

$router->get([$root . '/groups', 'groups'], function () {
    global $paths;
    require $paths['groups'];

    $controller = new GroupController();
    return $controller->getGroups();
});

$router->get([$root . '/groups/edit/{groupId:i}', 'groups_edit'], function (int $groupId) {
    global $paths;
    require $paths['groups'];

    $controller = new GroupController();
    return $controller->getEdit($groupId);
});

$router->post([$root . '/groups/edit', 'groups_edit_post'], function () {
    global $paths;
    require $paths['groups'];

    $controller = new GroupController();
    $controller->postCreateEdit();
});

$router->get([$root . '/groups/create', 'groups_create'], function () {
    global $paths;
    require $paths['groups'];

    $controller = new GroupController();
    return $controller->getCreate();
});

$router->get([$root . '/meal-types', 'meal-types'], function () {
    global $paths;
    require $paths['meal-types'];

    $controller = new MealTypeController();
    return $controller->getMealTypes();
});

$router->get([$root . '/meal-types/edit/{id:i}', 'meal-types_edit'], function(int $id) {
    global $paths;
    require $paths['meal-types'];

    $controller = new MealTypeController();
    return $controller->getEdit($id);
});

$router->post([$root . '/meal-types/edit', 'meal-types_edit_post'], function () {
    global $paths;
    require $paths['meal-types'];

    $controller = new MealTypeController();
    $controller->postEdit();
});

$router->get([$root . '/meal-types/create', 'meal-types_create'], function () {
    global $paths;
    require $paths['meal-types'];

    $controller = new MealTypeController();
    return $controller->getCreate();
});

$router->get([$root . '/kitchen-views', 'kitchen-views'], function () {
    global $paths;
    require $paths['kitchen-views'];

    $controller = new KitchenViewController();
    return $controller->getKitchenViews();
});

$router->get([$root . '/kitchen-views/edit/{id:i}', 'kitchen-views_edit'], function (int $id) {
    global $paths;
    require $paths['kitchen-views'];

    $controller = new KitchenViewController();
    return $controller->getEdit($id);
});

$router->post([$root . '/kitchen-views/edit', 'kitchen-views_edit_post'], function () {
    global $paths;
    require $paths['kitchen-views'];

    $controller = new KitchenViewController();
    $controller->postCreateEdit();
});

$router->get([$root . '/kitchen-views/create', 'kitchen-views_create'], function () {
    global $paths;
    require $paths['kitchen-views'];

    $controller = new KitchenViewController();
    return $controller->getCreate();
});

$router->get([$root . '/kitchen-meal', 'random-kitchen-meal'], function () {
    global $paths;
    require $paths['kitchen-meal'];

    $controller = new KitchenMealController();
    return $controller->getRandomKitchenMeal();
});

$router->get([$root . '/kitchen-meal/{mealTypeId:i}/{date:c}?', 'kitchen-meal'], function (int $mealTypeId, string $date = '') {
    global $paths;
    require $paths['kitchen-meal'];

    $controller = new KitchenMealController();
    return $controller->getKitchenMeal($mealTypeId, $date);
});

$router->get([$root . '/participations/{mealType:i}/{groupId:i}/{date:c}?', 'participations'], function (int $mealType, int $groupId, string $date = '') {
    global $paths;
    require $paths['participations'];

    $controller = new ParticipationController();
    return $controller->getParticipations($mealType, $groupId, $date);
});

$router->post([$root . '/participations/mass-post', 'participations_post'], function () {
    global $paths;
    require $paths['participations'];

    $controller = new ParticipationController();
    $controller->postParticipations();
});

$router->get([$root . '/current-participations/{mealType:i}?', 'current-participations'], function (int $mealType = null) {
    global $paths;
    require $paths['participations'];

    $controller = new ParticipationController();
    return $controller->getCurrentParticipations($mealType);
});

$router->get([$root . '/participations/history/{mealType:i}/{userId:i}/{date:c}?', 'participation_history'], function (int $mealType, int $userId, string $date = '') {
    global $paths;
    require $paths['participations'];

    $controller = new ParticipationController();
    return $controller->getParticipationHistoryForUser($mealType, $userId, $date);
});

$router->get([$root . '/reports/general', 'random_report'], function () {
    global $paths;
    require $paths['reports'];

    $controller = new ReportController();
    return $controller->getRandomGeneralReport();
});

$router->get([$root . '/reports/general/{mealTypeId:i}/{date:c}?', 'general_report'], function (int $mealTypeId, string $date = '') {
    global $paths;
    require $paths['reports'];

    $controller = new ReportController();
    return $controller->getGeneralReportForMealTypePerMonth($mealTypeId, $date);
});

$router->get([$root . '/import', 'import'], function () {
    global $paths;
    require $paths['import'];

    $controller = new ImportController();
    return $controller->getImport();
});

$router->post([$root . '/import', 'import_post'], function () {
    global $paths;
    require $paths['import'];

    $controller = new ImportController();
    $controller->postImport();
});

$router->get([$root . '/import-preview', 'import-preview'], function () {
    global $paths;
    require $paths['import'];

    $controller = new ImportController();
    return $controller->getImportPreview();
});

$router->post([$root . '/import-preview', 'import_preview_post'], function () {
    global $paths;
    require $paths['import'];

    $controller = new ImportController();
    $controller->postImportPreview();
});

$router->get([$root . '/import-results', 'import_results'], function () {
    global $paths;
    require $paths['import'];

    $controller = new ImportController();
    return $controller->getImportResults();
});

$router->get([$root . '/import/download-template', 'import_download_template'], function () {
    global $paths;
    require $paths['import'];

    $controller = new ImportController();
    $controller->getTemplateFile();
});

$router->get([$root . '/cards/create', 'cards_create'], function () {
    global $paths;
    require $paths['cards'];

    $controller = new CardController();
    return $controller->getCreate();
});

$router->get([$root . '/cards/edit/{id:i}', 'cards_edit'], function (int $id) {
    global $paths;
    require $paths['cards'];

    $controller = new CardController();
    return $controller->getEdit($id);
});

$router->post([$root . '/cards/edit', 'cards_edit_post'], function () {
    global $paths;
    require $paths['cards'];

    $controller = new CardController();
    $controller->postCreateEdit();
});

$router->get([$root . '/cards/{page:i}/{search:c}?', 'cards'], function (int $page = 1, string $search = "") {
   global $paths;
   require $paths['cards'];

   $controller = new CardController();
   return $controller->getCards($page, $search);
});

$router->get([$root . '/cards/delete/{id}', 'cards_delete'], function ($id) {
    global $paths;
    require $paths['cards'];

    $controller = new CardController();
    $controller->getDelete($id);
});




$router->get([$root . '/api/users', 'api_users_search'], function () {
    global $paths;
    require $paths['users'];

    $controller = new UserController();
    return $controller->getUsersApi();
});

$router->get([$root . '/api/groups', 'api_groups_search'], function () {
    global $paths;
    require $paths['groups'];

    $controller = new GroupController();
    return $controller->getGroupsApi();
});

$router->post([$root . '/api/participation/add', 'api_participation_add'], function () {
    global $paths;
    require $paths['participations'];

    $controller = new ParticipationController();
    return $controller->postParticipationAddApi();
});

$router->post([$root . '/api/app/participation/add', 'api_app_participation_add'], function () {
    global $paths;
    require $paths['participations'];

    $controller = new ParticipationController();
    return $controller->postParticipationAppApiAdd();
});

$router->post([$root . '/api/participation/add-by-id', 'api_participation_add-by-id'], function () {
    global $paths;
    require $paths['participations'];

    $controller = new ParticipationController();
    return $controller->postParticipationApiAddById();
});

$router->get([$root . '/api/cards', 'api_cards_search'], function () {
    global $paths;
    require $paths['cards'];

    $controller = new CardController();
    return $controller->getCardsApi();
});








// NB. You can cache the return value from $router->getData() so you don't have to create the routes each request - massive speed gains
$dispatcher = new Dispatcher($router->getData());

try {
    $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

    // Print out the value returned from the dispatched function
    echo $response;
} catch (HttpRouteNotFoundException $e) {
    $data = [];
    $data['content'] = 'error.twig';
    $data['site_subdir'] = $root;
    $data['error_msg'] = 'Error 404: Page not found!';
    error_log('Error 404: ' . $e);
    echo twigRender($data);;
} catch (HttpMethodNotAllowedException $e) {
    error_log('Method not allowed: ' . $e);
}




/**
 * Render twig templates (moved it into a separate function, so i have to catch errors only once).
 * @param array $data Array containing variables used by Twig template
 * @return string Returns
 */
function twigRender(array $data): string
{
    global $twig;

    try {
        return $twig->render($data['content'], $data);
    } catch (Exception $e) {
        error_log('Twig rendering failed: ' . $e);
    }
    return 'ERROR: Something went wrong with templates!';
}

/*
function print_mem()
{
    // Currently used memory
    $mem_usage = memory_get_usage();

    // Peak memory usage
    $mem_peak = memory_get_peak_usage();

    echo 'The script is now using: <strong>' . round($mem_usage / 1024) . 'KB</strong> of memory. ' . __METHOD__ . '<br>';
    echo 'Peak usage: <strong>' . round($mem_peak / 1024) . 'KB</strong> of memory.<br><br>';
}
*/