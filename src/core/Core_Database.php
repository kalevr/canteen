<?php

class Core_Database
{
    public mysqli $conn;

    public function __construct($user, $pass, $database, $server, $die = true)
    {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        try {
            $this->conn =new mysqli($server,$user, $pass, $database);
            $this->conn->set_charset("utf8");
        } catch (mysqli_sql_exception $e) {
            if ($die) {
                echo $e->getMessage();
                echo "<br><br>Database -- NOT -- loaded successfully .. ";
                error_log("Database connection failed!");
                die( "<br><br>Query Closed !!!");
            }

            throw new mysqli_sql_exception('Oh noes... something went wrong. Wrong username/password/db_name/server_name?)');
        }
    }


    /**
     * Sanitize and add variables to SQL statement in correct places
     * @param string $txt - SQL statment (with ?0 ?1 as placeholders)
     * @param array $params - variables to be placed into SQL statement (first var replaces ?0 etc..)
     * @return string - returns SQL statement with variables inserted
     */
    public function prepareSQL(string $txt, array $params = []): string {

        for ($i = 0; $i < sizeof($params); $i++) {
            if (is_array($params[$i])) {
                $sep = is_string($params[$i][0]) ? "'" : "";
                $tmpTxt = "";

                foreach ($params[$i] as $item) {
                    $tmpTxt != "" && $tmpTxt .= ", ";
                    $tmpTxt .= $sep . $this->escape($item) . $sep;
                }

                $txt = str_replace('?' . $i, $tmpTxt, $txt);

            } else {
                $txt = str_replace('?' . $i, $this->escape($params[$i]), $txt);
            }
        }

        if (preg_match('/\?\d/', $txt)) {
            throw new \http\Exception\InvalidArgumentException("Not enough variables in prepareSQL method!");
        }

        return $txt;
    }

    /**
     * Escape MySQL variables
     * @param $value - variable to escape
     * @return mixed - returns escaped variable or original variable (if not string)
     */
    public function escape($value): mixed
    {
        if (gettype($value) == "string") {
            return mysqli_real_escape_string($this->conn, $value);
        }

        return $value;
    }

    /**
     * Make an SQL query
     * @param $sql - SQL statement
     * @return array - return query results as an array (or empty array)
     */
    public function query($sql): array
    {
        $result = $this->conn->query($sql);
        $data = [];

        if ($result && $result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }
        return $data;
    }

    /**
     * Makes SQL query that changes the database  (use query() method for SELECT queries).
     * @param $sql - SQL statement
     * @return bool - Returns true if query succeeded
     */
    public function db_edit($sql): bool
    {
        $res = $this->conn->query($sql);
        if (!$res) {
            error_log("err: " . $this->conn->error);
        }

        return $res;
    }

    /**
     * Returns the value generated for an AUTO_INCREMENT column by the last query.
     * @return mixed
     */
    public function get_last_insert_id() {
        return $this->conn->insert_id;
    }

    /**
     * Puts together and executes update SQL statement
     * @param string $table - name of the table to update
     * @param array $ins - array containing (table) column names and vaules ( ['column' => 'value'])
     * @param int $id - id of the record to be updated
     * @return bool - returns true if the operation succeeded.
     */
    public function update(string $table, array $ins, int $id): bool
    {
        $set = [];

        foreach ($ins as $key => $value) {
            // if $value is string, put single quotes around it
            $sq = gettype($value) == 'string' ? "'" : "";

            $set[] = $key . ' = ' . $sq . $this->escape($value) . $sq;
        }

        $txt = "UPDATE " . $table . " SET " . implode(', ', $set) . " WHERE id = " . $id;
        return $this->db_edit($txt);
    }

    /**
     * Puts together and executes insert SQL statement
     * @param string $table - name of the table to update
     * @param array $ins - array containing (table) column names and vaules ( ['column' => 'value'])
     * @return int - returns the value generated for an AUTO_INCREMENT column (or 0 if insert failed).
     */
    public function insert(string $table, array $ins): int {
        $set = [];

        foreach ($ins as $key => $value) {
            $sq = gettype($value) == 'string' ? "'" : "";
            $set[$key] = $sq . $this->escape($value) . $sq;
        }

        $txt = 'INSERT INTO ' . $table . ' (' . implode(', ', array_keys($set)) . ')
        VALUES (' . implode(', ', $set) . ')';

        $this->db_edit($txt);

        return $this->get_last_insert_id();
    }


    public function processSqlFile(string $text): bool
    {
        if (empty($text)) {
            return false;
        }

        $sql = $this->cleanSqlFile($text);
        $queries = explode(';', $sql);

        try {
            foreach ($queries as $query) {
                if (!empty(trim($query))) {
                    self::db_edit($query);
                }
            }
        } catch (Exception $e) {
            error_log(__METHOD__ . ' failed: ' . $e);
            return false;
        }

        return true;
    }

    private function cleanSqlFile(string $text): string
    {
        $result = '';

        $textRows = explode(PHP_EOL, $text);

        foreach ($textRows as $row) {
            if (!str_starts_with(ltrim($row, ' '), '--') && !empty(trim($row))) {
                $tmpRow = explode('--', $row); // if row ends with comment, this should get rid of it.
                $result .= $tmpRow[0] . PHP_EOL;
            }
        }

        return $result;
    }
}