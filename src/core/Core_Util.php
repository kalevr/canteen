<?php

class Core_Util
{
    /**
     * Retrieve GET or POST parameter (in that order)
     * @param $key - parameter name
     * @param string $def - default value (if nothing is found)
     * @return string | array GET or POST value (or $def if $key not found in GET/POST)
     */
    public static function param(string $key, string $def = ''): string|array
    {
        if (isset($_GET[$key])) {
            return self::testInput($_GET[$key]);
        } else if (isset($_POST[$key])) {
            return self::testInput($_POST[$key]);
        } else {
            return $def;
        }
    }

    /**
     * Try to find out if agent string belongs to mobile device (using regex).
     * @param string $agent Browser agent string
     * @return false|int
     */
    public static function isMobileDevice(string $agent): bool|int
    {
        return preg_match(
            "/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i",
            $agent);
    }

    /***
     * Get client/user IP address from headers
     * @return mixed
     */
    public static function getUserIP(): mixed
    {
        $address = 'UNKNOWN';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $address = $_SERVER['REMOTE_ADDR'];
        }

        return $address;
    }

    /**
     * Check if the number is positive integer.
     * @param $number
     * @return bool
     */
    public static function isPositiveInteger($number): bool
    {
        if ((is_int($number) || ctype_digit($number)) && (int)$number > 0) {
            return true;
        }

        return false;
    }

    /**
     * Check if string is valid JSON
     * @param $string
     * @return bool
     */
    public static function isJson($string): bool
    {
        return is_numeric($string)
            || in_array($string, ['null', 'true', 'false'])
                || (is_bool($string) && $string)
                || (
                    !empty($string)
                    && is_string($string)
                    && is_array(json_decode($string, true))
                    && json_last_error() === JSON_ERROR_NONE
                );
    }

    /**
     * Check if JSON has data
     * @param string $string
     * @return bool
     */
    public static function hasJsonData(string $string): bool
    {
        return !empty($string)
            && is_string($string)
            && is_array($array = json_decode($string, true))
            && !empty($array)
            && json_last_error() === JSON_ERROR_NONE;
    }

    private static function testInput($data): array|string
    {
        if (is_array($data)) {
            $escaped = [];
            foreach ($data as $item) {
                $escaped[] = self::testInput($item);
            }

            return $escaped;
        }

        return self::escapeInput($data);
    }

    /**
     * Escapes input (trim -> stripslashes -> htmlspecialchars)
     * @param string $data
     * @return string
     */
    private static function escapeInput(string $data): string
    {
        $data = trim($data);
        $data = stripslashes($data);
        return htmlspecialchars($data);
    }
}