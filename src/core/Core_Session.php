<?php

class Core_Session
{
    private static string $prefix;

    public function __construct()
    {
        global $config;

        if (!isset($_SESSION)) {
            session_start();
        }

        Core_Session::$prefix = $config['SESSION_PREFIX'];
    }

    /**
     * Set session variable (automatically adds prefix from config to key - if exists)
     * @param string $key - session variable key (without prefix)
     * @param string|array $value - session variable value
     */
    public static function setVar(string $key, string | array $value) {
        if (Core_Session::$prefix) {
            $key = Core_Session::$prefix . '_' . $key;
        }

        $_SESSION[$key] = $value;
    }

    /**
     * Sets array of variables
     * @param array $vars
     * @return void
     */
    public static function massSetVar(array $vars): void
    {
        if ($vars) {
            foreach ($vars as $key => $value) {
                if ($key) {
                    self::setVar($key, $value);
                }
            }
        }
    }

    /**
     * Get session variable (automatically adds prefix from config to key - if exists)
     * @param string $key - session variable key (without prefix)
     * @return mixed
     */
    public static function getVar(string $key): mixed
    {
        if (Core_Session::$prefix) {
            $key = Core_Session::$prefix . '_' . $key;
        }

        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return null;
    }

    /**
     * Unsets session variable (automatically adds prefix from config to key - if exists)
     * @param string $key - session variable key (without prefix)
     */
    public static function unsetVar(string $key) {
        if (Core_Session::$prefix) {
            $key = Core_Session::$prefix . '_' . $key;
        }

        unset($_SESSION[$key]);
    }

    /**
     * Unsets array of variables (using Core_Session::unsetVar)
     * @param array $keys
     * @return void
     */
    public static function massUnsetVar(array $keys) {
        if ($keys) {
            foreach ($keys as $key) {
                if ($key) {
                    self::unsetVar($key);
                }
            }
        }
    }

    /**
     * Destroys the session (using session_destroy)
     */
    public static function destroy() {
        session_destroy();
    }
}