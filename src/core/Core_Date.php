<?php
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Util.php';

class Core_Date
{
    // day names long
    public const DAYS_MONDAY = 'esmaspäev';
    public const DAYS_TUESDAY = 'teisipäev';
    public const DAYS_WENDSDAY = 'kolmapäev';
    public const DAYS_THURDSDAY = 'neljapäev';
    public const DAYS_FRIDAY = 'reede';
    public const DAYS_SATURDAY = 'laupäev';
    public const DAYS_SUNDAY = 'pühapäev';

    // day names short
    public const DAYS_MONDAY_SHORT = 'E';
    public const DAYS_TUESDAY_SHORT = 'T';
    public const DAYS_WENDSDAY_SHORT = 'K';
    public const DAYS_THURDSDAY_SHORT = 'N';
    public const DAYS_FRIDAY_SHORT = 'R';
    public const DAYS_SATURDAY_SHORT = 'L';
    public const DAYS_SUNDAY_SHORT = 'P';

    // month names long
    public const MONTHS_JANUARY = 'jaanuar';
    public const MONTHS_FEBRUARY = 'veebruar';
    public const MONTHS_MARCH = 'märts';
    public const MONTHS_APRIL = 'aprill';
    public const MONTHS_MAY = 'mai';
    public const MONTHS_JUNE = 'juuni';
    public const MONTHS_JULY = 'juuli';
    public const MONTHS_AUGUST = 'august';
    public const MONTHS_SEPTEMBER = 'september';
    public const MONTHS_OCTOBER = 'oktoober';
    public const MONTHS_NOVEMBER = 'november';
    public const MONTHS_DECEMBER = 'detsember';

    // month names short
    public const MONTHS_JANUARY_SHORT = 'jaan';
    public const MONTHS_FEBRUARY_SHORT = 'veebr';
    public const MONTHS_MARCH_SHORT = 'mär';
    public const MONTHS_APRIL_SHORT = 'apr';
    public const MONTHS_MAY_SHORT = 'mai';
    public const MONTHS_JUNE_SHORT = 'jun';
    public const MONTHS_JULY_SHORT = 'juul';
    public const MONTHS_AUGUST_SHORT = 'aug';
    public const MONTHS_SEPTEMBER_SHORT = 'sept';
    public const MONTHS_OCTOBER_SHORT = 'okt';
    public const MONTHS_NOVEMBER_SHORT = 'nov';
    public const MONTHS_DECEMBER_SHORT = 'dets';


    /**
     * Checks if given string is a valid date
     * @param string $date - date string to validate
     * @return DateTime - returns DateTime object if valid, throws Exception if not
     * @throws Exception
     */
    static function validateDate(string $date): DateTime
    {
        try {
            $dt = new DateTime($date);
        } catch (Exception $e) {
            throw new Exception($e);
        }

        return $dt;
    }

    /**
     * Returns current date (in preferred format)
     * @param string $format - Specify date format
     * @return string - date string
     */
    static function now(string $format = 'Y-m-d'): string
    {
        $dt = new DateTime();
        return $dt->format($format);
    }

    /**
     * Get and validate date string (if format not specified, returns object instead)
     * @param string $date - date to get
     * @param string $format - format of the output
     * @return DateTime|string - returns DateTime object or a string representation of it
     * @throws Exception
     */
    static function get(string $date, string $format = '') {
        if (!$date || gettype($date) != 'string') {
            throw new Exception("Not a valid string");
        }

        $dt = self::validateDate($date);

        if ($format) {
            return $dt->format($format);
        }

        return $dt;
    }

    /**
     * Finds the first day of month of the date entered
     * @param string $date - date string - defaults to now()
     * @param string $format - specify date format
     * @return string
     * @throws Exception
     */
    static function firstDayOfMonth(string $date, string $format = 'Y-m-d'): string
    {
        $dt = self::get($date);
        $dt->modify('first day of this month');

        return $dt->format($format);
    }

    /**
     * Finds the last day of month of the date entered
     * @param string $date - date string - defaults to now()
     * @param string $format - specify date format
     * @return mixed
     * @throws Exception
     */
    static function lastDayOfMonth(string $date, string $format = 'Y-m-d')
    {
        $dt = self::get($date);
        $dt->modify('last day of this month');

        return $dt->format($format);
    }

    /**
     * Finds what day of the week given date is (also validates date)
     * @param string $date - date in question - default now()
     * @return string - returns a numeric representation of given day of week (1-7)
     * @throws Exception
     */
    static function dayOfWeek(string $date): string
    {
        return self::get($date, 'N');
    }

    /**
     * Get month name (full or short)
     * @param string $date - date string to find the month of
     * @param bool $short - if true, returns short version of the month name
     * @return string - returns long or short version of month's name
     * @throws Exception
     */
    static function getMonthName(string $date, bool $short = false) {
        $month = self::get($date, 'm');

        return match ($month) {
            '01' => $short ? self::MONTHS_JANUARY_SHORT : self::MONTHS_JANUARY,
            '02' => $short ? self::MONTHS_FEBRUARY_SHORT : self::MONTHS_FEBRUARY,
            '03' => $short ? self::MONTHS_MARCH_SHORT : self::MONTHS_MARCH,
            '04' => $short ? self::MONTHS_APRIL_SHORT : self::MONTHS_APRIL,
            '05' => $short ? self::MONTHS_MAY_SHORT : self::MONTHS_MAY,
            '06' => $short ? self::MONTHS_JUNE_SHORT : self::MONTHS_JUNE,
            '07' => $short ? self::MONTHS_JULY_SHORT : self::MONTHS_JULY,
            '08' => $short ? self::MONTHS_AUGUST_SHORT : self::MONTHS_AUGUST,
            '09' => $short ? self::MONTHS_SEPTEMBER_SHORT : self::MONTHS_SEPTEMBER,
            '10' => $short ? self::MONTHS_OCTOBER_SHORT : self::MONTHS_OCTOBER,
            '11' => $short ? self::MONTHS_NOVEMBER_SHORT : self::MONTHS_NOVEMBER,
            '12' => $short ? self::MONTHS_DECEMBER_SHORT : self::MONTHS_DECEMBER,
            default => '',
        };
    }

    /**
     * Get day name (long or short version)
     * @param string $date - date string to find the month of
     * @param bool $short - if true, returns short version of the day name
     * @return string
     * @throws Exception
     */
    static function getDayName(string $date, bool $short = false): string {
        $dayOfWeek = self::dayOfWeek($date);

        return match ($dayOfWeek) {
            '1' => $short ? self::DAYS_MONDAY_SHORT : self::DAYS_MONDAY,
            '2' => $short ? self::DAYS_TUESDAY_SHORT : self::DAYS_TUESDAY,
            '3' => $short ? self::DAYS_WENDSDAY_SHORT : self::DAYS_WENDSDAY,
            '4' => $short ? self::DAYS_THURDSDAY_SHORT : self::DAYS_THURDSDAY,
            '5' => $short ? self::DAYS_FRIDAY_SHORT : self::DAYS_FRIDAY,
            '6' => $short ? self::DAYS_SATURDAY_SHORT : self::DAYS_SATURDAY,
            '7' => $short ? self::DAYS_SUNDAY_SHORT : self::DAYS_SUNDAY,
            default => 'freak',
        };

    }

    static function getWeekdayNamesShort() {
        return [self::DAYS_MONDAY_SHORT, self::DAYS_TUESDAY_SHORT, self::DAYS_WENDSDAY_SHORT, self::DAYS_THURDSDAY_SHORT,
            self::DAYS_FRIDAY_SHORT, self::DAYS_SATURDAY_SHORT, self::DAYS_SUNDAY_SHORT];
    }

    /**
     * Add an interval to date string and return new DateTime object
     * @param string $date - base date
     * @param string $interval - interval to add (examples: 'P1M', 'PT10H30S', 'P6Y5M4D3H2M1S')
     * @return DateTime - returns new date
     * @throws Exception
     */
    static function add(string $date, string $interval): DateTime{
        if (!$interval) {
            throw new Exception("Date interval missing!");
        }

        $dt = self::get($date);
        $intervalObj = new DateInterval($interval);
        return  $dt->add($intervalObj);
    }

    /**
     * Subtract an interval to date string and return new DateTime object
     * @param string $date - base date
     * @param string $interval - interval to subtract (examples: 'P1M', 'PT10H30S', 'P6Y5M4D3H2M1S')
     * @return DateTime - returns new date
     * @throws Exception
     */
    static function sub(string $date, string $interval): DateTime{
        if (!$interval) {
            throw new Exception("Date interval missing!");
        }

        $dt = self::get($date);
        $intervalObj = new DateInterval($interval);
        return  $dt->sub($intervalObj);
    }

    /**
     * Get all days in month (day numbers only)
     * @param string $date - date string ('Y-m-d')
     * @return array - returns an array of integers (days)
     * @throws Exception
     */
    static function getAllDaysInMonth(string $date): array
    {
        $finalDay = self::lastDayOfMonth($date);
        $finalDay = substr($finalDay, -2);
        $days = [];

        for ($i = 1; $i <= $finalDay; $i++) {
            $days[] = $i;
        }

        return $days;
    }
}