<?php
require __DIR__ . '/../../preventDirectAccess.php';

class KitchenView
{
    const ID = 'id';
    const APP_YEAR = 'app_year';
    const NAME = 'kitchen_view_name';
    const MEAL_TYPE_ID = 'meal_type';
    const MEAL_TYPE_NAME = 'meal_type_name';
    const STARTING_FROM = 'starting_from';
    const VALID_UNTIL = 'valid_until';
    const SORT_ORDER = 'sort_order';
    const CREATED_BY = 'created_by';
    const GROUPS = 'groups';
    const NAME_ERROR = 'kv_name_error';
    const MEAL_TYPE_ID_ERROR = 'meal_type_error';
    const STARTING_FROM_ERROR = 'starting_from_error';
    const VALID_UNTIL_ERROR = 'valid_until_error';
    const SORT_ORDER_ERROR = 'sort_order_error';
    const GROUPS_ERROR = 'groups_error';

    private ?int $id = null;
    private ?int $appYear = null;
    private string $kitchenViewName;
    private ?int $mealTypeId = null;
    private ?string $mealTypeName = null;
    private ?string $startingFrom = null;
    private ?string $validUntil = null;
    private ?int $sortOrder = null;
    private array $groups = [];
    private ?string $editUrl = null;
    private ?string $nameError = null;
    private ?string $mealTypeError = null;
    private ?string $startingFromError = null;
    private ?string $validUntilError = null;
    private ?string $sortOrderError = null;
    private ?string $groupsError = null;

    public function __construct(string $kitchenViewName) {
        $this->kitchenViewName = $kitchenViewName;
    }

    /**
     * @return ?int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        if ($id > 0) {
            $this->id = $id;
        }
    }

    /**
     * @return ?string
     */
    public function getKitchenViewName(): ?string
    {
        return $this->kitchenViewName;
    }

    /**
     * @param string $kitchenViewName
     */
    public function setKitchenViewName(string $kitchenViewName): void
    {
        if (!empty($kitchenViewName)) {
            $this->kitchenViewName = $kitchenViewName;
        } else {
            $this->nameError = 'Nimi on kohustuslik!';
        }
    }

    /**
     * @return ?int
     */
    public function getMealTypeId(): ?int
    {
        return $this->mealTypeId;
    }

    /**
     * @param ?int $mealTypeId
     */
    public function setMealTypeId(?int $mealTypeId): void
    {
        if ($mealTypeId > 0) {
            $this->mealTypeId = $mealTypeId;
        } else {
            $this->mealTypeError = 'Söögikorra ID peab olema täisarv';
        }
    }

    /**
     * @return int|null
     */
    public function getAppYear(): ?int
    {
        return $this->appYear;
    }

    /**
     * @param int|null $appYear
     */
    public function setAppYear(?int $appYear): void
    {
        $this->appYear = $appYear;
    }

    /**
     * @return ?string
     */
    public function getMealTypeName(): ?string
    {
        return $this->mealTypeName;
    }

    /**
     * @param string $mealTypeName
     */
    public function setMealTypeName(string $mealTypeName): void
    {
        $this->mealTypeName = $mealTypeName;
    }

    /**
     * @return string
     */
    public function getStartingFrom(): string
    {
        return $this->startingFrom;
    }

    /**
     * @param string $startingFrom
     */
    public function setStartingFrom(string $startingFrom): void
    {
        try {
            Core_Date::get($startingFrom);
            $this->startingFrom = $startingFrom;
        } catch (Exception) {
            $this->startingFromError = 'Algus kuupäev ei ole sobiv!';
        }
    }

    /**
     * @return ?string
     */
    public function getValidUntil(): ?string
    {
        return $this->validUntil;
    }

    /**
     * @param ?string $validUntil
     */
    public function setValidUntil(?string $validUntil): void
    {
        if ($validUntil) {
            try {
                Core_Date::get($validUntil);
                $this->validUntil = $validUntil;
            } catch (Exception) {
                $this->startingFromError = 'Algus kuupäev ei ole sobiv!';
            }
        }
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param int|null $sortOrder
     */
    public function setSortOrder(?int $sortOrder): void
    {
        if (!$sortOrder || $sortOrder < 0) {
            $this->sortOrderError = "Jrk. nr peab olema positiivne täisarv!";
            return;
        }
        $this->sortOrder = $sortOrder;
    }

    /**
     * @return array
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param array $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = $groups;
    }

    /**
     * @return ?string
     */
    public function getEditUrl(): ?string
    {
        return $this->editUrl;
    }

    /**
     * @param ?string $editUrl
     */
    public function setEditUrl(?string $editUrl): void
    {
        $this->editUrl = $editUrl;
    }

    /**
     * @return ?string
     */
    public function getNameError(): ?string
    {
        return $this->nameError;
    }

    /**
     * @param ?string $nameError
     */
    public function setNameError(?string $nameError): void
    {
        $this->nameError = $nameError;
    }

    /**
     * @return ?string
     */
    public function getMealTypeError(): ?string
    {
        return $this->mealTypeError;
    }

    /**
     * @param ?string $mealTypeError
     */
    public function setMealTypeError(?string $mealTypeError): void
    {
        $this->mealTypeError = $mealTypeError;
    }

    /**
     * @return ?string
     */
    public function getStartingFromError(): ?string
    {
        return $this->startingFromError;
    }

    /**
     * @param ?string $startingFromError
     */
    public function setStartingFromError(?string $startingFromError): void
    {
        $this->startingFromError = $startingFromError;
    }

    /**
     * @return ?string
     */
    public function getValidUntilError(): ?string
    {
        return $this->validUntilError;
    }

    /**
     * @param ?string $validUntilError
     */
    public function setValidUntilError(?string $validUntilError): void
    {
        $this->validUntilError = $validUntilError;
    }

    /**
     * @return string|null
     */
    public function getSortOrderError(): ?string
    {
        return $this->sortOrderError;
    }

    /**
     * @param string|null $sortOrderError
     */
    public function setSortOrderError(?string $sortOrderError): void
    {
        $this->sortOrderError = $sortOrderError;
    }

    /**
     * @return string|null
     */
    public function getGroupsError(): ?string
    {
        return $this->groupsError;
    }

    /**
     * @param string|null $groupsError
     */
    public function setGroupsError(?string $groupsError): void
    {
        $this->groupsError = $groupsError;
    }

    public function getAllErrors() {
        $errors = [];

        if (empty($this->kitchenViewName) && empty($this->nameError)) {
            $this->nameError = 'Nimi on kohustuslik!';
        }

        if ($this->nameError) {
            $errors[self::NAME_ERROR] = $this->nameError;
        }

        if ($this->mealTypeError) {
            $errors[self::MEAL_TYPE_ID_ERROR] = $this->mealTypeError;
        }

        if ($this->startingFromError) {
            $errors[self::STARTING_FROM_ERROR] = $this->startingFromError;
        }

        if ($this->validUntilError) {
            $errors[self::VALID_UNTIL_ERROR] = $this->validUntilError;
        }

        if ($this->sortOrderError) {
            $errors[self::SORT_ORDER_ERROR] = $this->sortOrderError;
        }

        if ($this->groupsError) {
            $errors[self::GROUPS_ERROR] = $this->groupsError;
        }

        return $errors;
    }
}