<?php
require __DIR__ . '/../../preventDirectAccess.php';

class LogRow
{
    private string $tableName;
    private int $recordId;
    private string $colName;
    private string $oldValue;
    private string $newValue;
    private string $timestamp;
    private string $changedBy;

    public function __construct(
        string $tableName, int $recordId, string $colName, string $oldValue, string $newValue, string $changedBy)
    {
        $this->tableName = $tableName;
        $this->recordId = $recordId;
        $this->colName = $colName;
        $this->oldValue = $oldValue;
        $this->newValue = $newValue;
        $this->timestamp = Core_Date::now('Y-m-d H:i:s');
        $this->changedBy = $changedBy;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @return int
     */
    public function getRecordId(): int
    {
        return $this->recordId;
    }

    /**
     * @return string
     */
    public function getColName(): string
    {
        return $this->colName;
    }

    /**
     * @return string
     */
    public function getOldValue(): string
    {
        return $this->oldValue;
    }

    /**
     * @return string
     */
    public function getNewValue(): string
    {
        return $this->newValue;
    }

    /**
     * @return string
     */
    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    /**
     * @return string
     */
    public function getChangedBy(): string
    {
        return $this->changedBy;
    }

}