<?php

class UpdateConfig
{
    private array $confFromFile = [];
    private string $filePath = APPLICATION_PATH . DS . 'config' . DS . 'client_conf.php';

    public function __construct() {
        $this->confFromFile = file($this->filePath,FILE_SKIP_EMPTY_LINES);
    }

    /**
     * Update a single variable in config.
     * @param string $name - variable name (LIKE_THIS for example)
     * @param string $newValue - new value for the variable (if value is string, use updateStrVariable instead)
     * @return bool - return true if successful
     */
    public function updateVariable(string $name, string $newValue = ''): bool
    {
        $isSuccessful = false;

        foreach ($this->confFromFile as $key => $row) {
            if (!empty($name) && str_contains($row, $name)) {
                $rowArray = explode('=', $row);
                $this->confFromFile[$key] = trim($rowArray[0]) . " = " . $newValue . ';' . PHP_EOL;
                $isSuccessful = true;
            }
        }

        if (!$isSuccessful) {
            error_log('Updating settings failed for k=>v pair ' . $name . ' => ' . $newValue . '.');
        }

        return $isSuccessful;
    }

    /**
     * Wrapper for updateVariable (adds PHP ' signs before and after the $newValue, so PHP would see it as a string)
     * @param string $name - variable name (LIKE_THIS for example)
     * @param string $newValue - new value for the variable (type string)
     * @return bool
     */
    public function updateStrVariable(string $name, string $newValue = ''): bool
    {
        return $this->updateVariable($name, "'" . $newValue . "'");
    }

    /**
     * Update multiple variables in config (at the same time).
     * @param array $kvPairs key => value pairs (for example if value is string ['VARIABLE_NAME' => "'variable value'"])
     * @return bool returns true if all updates were successful (false if even one failed).
     */
    public function updateMultipleVariables(array $kvPairs): bool
    {
        $updatesDone = 0;
        $updateCount = count($kvPairs);

        foreach ($this->confFromFile as $key => $row) {
            foreach ($kvPairs as $name => $newValue) {
                if (str_contains($row, $name)) {
                    $rowArray = explode('=', $row);
                    $this->confFromFile[$key] = trim($rowArray[0]) . ' = ' . $newValue . ';' . PHP_EOL;
                    $updatesDone++;
                }
            }
        }

        if ($updatesDone != $updateCount) {
            error_log('Updating settings failed. ' . $updatesDone . ' of ' . $updateCount . ' updates succeeded.');
            return false;
        }

        return true;
    }

    /**
     * Insert a totally new variable into config (not string).
     * @param string $name - name of the new variable (should be unique and in all caps LIKE_THIS)
     * @param string $value - value to the new variable (if string use insertNewStrVariable instead)
     * @param int $rowNr - which row to insert into (if unset, then insert at the end)
     * @return bool - returns true if successful
     */
    public function insertNewVariable(string $name, string $value = '', int $rowNr = -1): bool
    {
        if (empty($name)) {
            error_log(__METHOD__ . ' failed. Variable name cannot be empty!');
        }

        // mark down how big the array is before we try to insert anything into it
        $oldArray = count($this->confFromFile);

        if ($rowNr == -1) {
            $this->confFromFile[] = "\$config['" . $name . "'] = " . $value . ';' . PHP_EOL;
        } else {

            array_splice($this->confFromFile, $rowNr, 0, "\$config['" . $name . "'] = " . $value . ';' . PHP_EOL);
        }

        // if array size didn't change, then we obviously failed
        if (count($this->confFromFile) == $oldArray) {
            error_log('Insert setting failed. Could not add element to array');
            return false;
        }

        return true;
    }

    /**
     * Wrapper for insertNewVariable (adds PHP ' signs before and after value, so PHP would see it as a string).
     * @param string $name - name of the new variable (should be unique and preferably in all caps LIKE_THIS)
     * @param string $value - value to the new (string) variable
     * @param int $rowNr - which row to insert into (if unset, then insert at the end)
     * @return bool
     */
    public function insertNewStrVariable(string $name, string $value = '', int $rowNr = -1): bool
    {
        if (empty($name)) {
            error_log(__METHOD__ . ' failed. Variable name cannot be empty!');
            return false;
        }

        // check that variable name doesn't already exist
        if ($this->checkIfVariableExists($name)) {
            error_log('Insert setting failed. Setting with that name (key) already exists.');
            return false;
        }

        return $this->insertNewVariable($name, "'" . $value . "'", $rowNr);
    }

    /**
     * Insert PHP comment into the config file
     * @param string $comment
     * @param int $rowNr
     * @return bool
     */
    public function insertComment(string $comment, int $rowNr = -1): bool
    {
        if (empty($comment)) {
            error_log('Insert comment to config file failed. Comment cannot be an empty string!');
            return false;
        }

        // mark down how big the array is before we try to insert anything into it
        $oldArray = count($this->confFromFile);

        if ($rowNr == -1) {
            $this->confFromFile[] = '// ' . $comment . PHP_EOL;
        } else {

            array_splice($this->confFromFile, $rowNr, 0, '// ' . $comment . PHP_EOL);
        }

        // if array size didn't change, then we obviously failed
        if (count($this->confFromFile) == $oldArray) {
            error_log('Inserting comment to conf file failed. Could not add element to array');
            return false;
        }

        return true;
    }

    /**
     * Insert empty row at the suggested position.
     * @param int $row which row to insert into (if unset, then insert at the end)
     * @return void
     */
    public function insertEmptyRow(int $row = -1): void
    {
        if ($row == -1) {
            $this->confFromFile[] = '' . PHP_EOL;
        } else {
            array_splice($this->confFromFile, $row, 0, '' . PHP_EOL);
        }
    }

    /**
     * Get all config name => value pairs in this object.
     * @return array
     */
    public function getVariablePairs(): array
    {
        $variables = [];

        foreach ($this->confFromFile as $row) {
            if (strlen($row) > 0 && !str_starts_with($row, '//') && str_contains($row, "['")) {
                $rowObj = explode('=', $row);
                $name = explode("'", $rowObj[0])[1];
                $value = explode('//', $rowObj[1])[0];
                $value = substr(trim($value), 0, -1);
                $variables[$name] = trim($value);
            }
        }

        return $variables;
    }

    /**
     * Return the value of config variable (returns empty string if variable with that name doesn't exist)
     * @param string $varName
     * @return string
     */
    public function getVariable(string $varName): string
    {
        foreach ($this->confFromFile as $row) {
            if (strlen($row) > 0 && !str_starts_with($row, '//') && str_contains($row, "['")) {
                $rowObj = explode('=', $row);
                $name = explode("'", $rowObj[0])[1];
                $value = explode('//', $rowObj[1])[0];
                $value = substr(trim($value), 0, -1);

                if ($name == $varName) {
                    return $value;
                }
            }
        }

        return '';
    }

    /**
     * Save configuration into client_config.php file  (overwrite)
     * @return bool|int
     */
    public function saveToFile(): bool|int
    {
        return file_put_contents($this->filePath, $this->confFromFile);
    }

    /**
     * Check if variable exists
     * @param string $varName
     * @return bool
     */
    public function checkIfVariableExists(string $varName): bool
    {
        foreach ($this->confFromFile as $key => $row) {
            if (!empty($name) && str_contains($row, $name)) {
                return true;
            }
        }

        return false;
    }

}