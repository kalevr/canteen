<?php
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'UpdateConfig.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Session.php';

class UpdateApp
{
    private string $appVerInDb = '';
    private string $appVerInFile = '';
    private string $verFilePath = APPLICATION_PATH . DS . 'config' . DS . 'version.txt';

    public function populateAppVersionFromDb(): void
    {
        global $DB;

        $txt = "
            SELECT setting_value
            FROM app_setting s
            WHERE s.setting_code = 'APP_VERSION'
        ";

        $res = $DB->query($txt);

        if ($res && $res[0]) {
            $this->appVerInDb = trim($res[0]['setting_value']);
        }
    }

    public function populateAppVersionFromFile(): void
    {
        if (file_exists($this->verFilePath)) {
            $f = fopen($this->verFilePath, 'r');
            $line = fgets($f);
            fclose($f);

            if ($line) {
                $this->appVerInFile = trim($line);
            }
        }
    }

    public function checkForUpdatesLazy(): bool
    {
        global $config;

        $lastModified = filemtime($this->verFilePath);

        if ($config['VERSION_FILE_TIMESTAMP'] != $lastModified) {
            $this->populateAppVersionFromFile();
            $this->populateAppVersionFromDb();
            return $this->checkForUpdates();
        }

        return false;
    }

    public function checkForUpdates(): bool
    {
        error_log('Checking for updates.');
        if (empty($this->appVerInFile) || empty($this->appVerInDb)) {
            error_log('Checking for updates failed, because of insufficient data (appVerInFile: '
                . $this->appVerInFile . ' - appVerInDb: ' . $this->appVerInDb);
            return false;
        }

        $hasUpdates = version_compare($this->appVerInDb, $this->appVerInFile, '<');

        // if updates are not needed, change the timestamp in conf file (or it will check every time)
        if (!$hasUpdates) {
            $this->updateDbAndConfFile('', true);
        }

        return $hasUpdates;
    }

    public function installUpdates(): bool
    {
        if (empty($this->appVerInDb)) {
            error_log('Installing updates failed, because appVerInDb was empty!');
            return false;
        }
        $dir = APPLICATION_PATH . DS . 'migration';
        $isUpdated = false;
        $updateFound = false;
        $dirContents = array_diff(scandir($dir), array('..', '.'));
        natsort($dirContents);

        foreach ($dirContents as $item) {
            if (is_dir($dir . DS . $item)) {
                // if app version (folder name) is greater than DB version
                if (version_compare($item, $this->appVerInDb, '>')) {
                    $updateFound = true;
                    $successful = $this->processFolder($dir . DS . $item, $item);

                    if (!$successful) {
                        $this->setLastUpdateFailed();
                    } else {
                        $this->updateDbAndConfFile($item);
                        $isUpdated = true;
                    }
                }
            }
        }

        // if no updates were installed (and no errors detected), let's update the conf file (so the update won't run with every request).
        if (!$isUpdated && !$updateFound) {
            $this->updateDbAndConfFile('', true);
        }

        return true;
    }

    private function processFolder(string $path, string $version)
    {
        $dirContents = array_diff(scandir($path), array('..', '.'));
        natsort($dirContents);

        foreach ($dirContents as $file) {
            if (!is_dir($file)) {
                $extension = pathinfo($file, PATHINFO_EXTENSION);
                try {
                    if ($extension == 'php') {
                        $this->processPhpFile($path . DS . $file);
                    } elseif ($extension == 'sql') {
                        $this->processSqlFile($path . DS . $file);
                    }
                } catch (Exception $e) {
                    error_log('Updater failed to process file "' . $file . '" with the following Exception:');
                    error_log($e);
                    return false;
                }

            }
        }

        return true;
    }

    private function processPhpFile($path)
    {
        try {
            require $path;
            error_log('Ran updates in file ' . $path);
        } catch (Exception $e) {
            error_log(__METHOD__ . ' failed. ' . print_r($e, true));
            exit();
        }
    }

    /**
     * Process the .sql file needed to update the app (it is assumed, that there are no syntax errors in file).
     * @throws Exception
     */
    private function processSqlFile($path)
    {
        global $DB;

        $sql = file_get_contents($path);
        $res = $DB->processSqlFile($sql);

        if ($res) {
            error_log('Ran updates in file ' . $path);
        } else {
            error_log('Failed to edit DB. (reason can be find before this line in error log). ');
            throw new Exception();
        }
    }

    /**
     * Write new version into DB and update VERSION_FILE_TIMESTAMP in client_config.php file.
     * @param string $version - version of the webapp
     * @param bool $ignoreDb - if set to true, only conf file will be update (but not the DB)
     * @return void
     */
    private function updateDbAndConfFile(string $version = "", bool $ignoreDb = false): void
    {
        global $DB;

        // update app version in DB (if $updateDB = true and version is set)
        if ($version && !$ignoreDb) {
            $txt = "UPDATE app_setting
                SET setting_value = '?0'
                WHERE setting_code = 'APP_VERSION'";
            $sql = $DB->prepareSQL($txt, [$version]);
            $res = $DB->db_edit($sql);

            if ($res) {
                error_log('Changed app version in DB to ' . $version);
            } else {
                error_log('Changing app version in DB to ' . $version . ' failed!');
            }
        }

        // update VERSION_FILE_TIMESTAMP in config file
        $newTimestamp = filemtime($this->verFilePath);
        $conf = new UpdateConfig();
        $conf->updateVariable('VERSION_FILE_TIMESTAMP', filemtime($this->verFilePath));
        $saved = $conf->saveToFile();

        if ($saved) {
            error_log('Changed version.txt timestamp in client_conf.php file! (to ' . $newTimestamp . ')');
        } else {
            error_log('Failed changing version.txt timestamp in client_conf.php file! (to ' . $newTimestamp . ')');
        }

    }

    private function setLastUpdateFailed(bool $hasFailed = true): void
    {
        $conf = new UpdateConfig();
        $conf->updateVariable('LAST_UPDATE_FAILED', ($hasFailed ? 'true' : 'false'));
        $conf->saveToFile();
    }
}