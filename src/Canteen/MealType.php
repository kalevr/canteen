<?php
require __DIR__ . '/../../preventDirectAccess.php';

class MealType
{
    // For ease of use, I'm using the same names for Session and POST variables (and in twig files)
    const ID = 'meal_type_id'; // db uses 'id', but that would be a bad name for session var
    const NAME = 'meal_type_name';
    const MEAL_TIME = 'meal_time';
    const KITCHEN_ID = 'kitchen'; // TODO: kitchen_id is better name (in db as well)
    const BUSINESS_DAYS = 'business_days';
    const KITCHEN_NAME = 'kitchen_name';
    const REG_CUTOFF = 'registration_cutoff';
    const COMMENTS = 'comments';
    const GROUPS = 'groups';
    const CREATED_BY = 'created_by';
    const NAME_ERROR = 'meal_type_name_error';
    const BUSINESS_DAYS_ERROR = 'business_days_error';
    const MEAL_TIME_ERROR = 'meal_time_error';
    const KITCHEN_ID_ERROR = 'kitchen_error';
    const REG_CUTOFF_ERROR = 'registration_cutoff_error';
    const GROUPS_ERROR = 'groups_error';
    const GENERAL_ERROR = 'general_error';
    const ENTIRE_WEEK = [1 => 'E', 2 => 'T', 3 => 'K', 4 => 'N', 5 => 'R', 6 => 'L', 7 => 'P'];

    // private/class variables
    private int $id;
    private string $name;
    private array $businessDays = [];
    private string $mealTime;
    private int $kitchenId;
    private string $kitchenName;
    private int $registrationCutoff;
    private string $comments;
    private array $groups;
    private string $editUrl;
    private string $nameError;
    private string $businessDaysError;
    private string $mealTimeError;
    private string $kitchenError;
    private string $registrationCutoffError;
    private string $commentsError;
    private string $groupsError;
    private string $generalError;
    private array $allErrors;



    public function __construct(string $name, string $mealTime, int $registrationCutoff = 0)
    {
        $this->name = $name;
        $this->mealTime = $mealTime;
        $this->registrationCutoff = $registrationCutoff;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getBusinessDays(): array
    {
        return $this->businessDays;
    }

    /**
     * @param array $businessDays
     */
    public function setBusinessDays(array $businessDays): void
    {
        if (count($businessDays) == count(array_intersect($businessDays, [1,2,3,4,5,6,7]))) {
            $this->businessDays = $businessDays;
        }
    }

    /**
     * @return string
     */
    public function getMealTime(): string
    {
        return $this->mealTime;
    }

    /**
     * @param string $mealTime
     * @throws Exception
     */
    public function setMealTime(string $mealTime): void
    {
        if (self::isValidMealTime($mealTime)) {
            $this->mealTime = $mealTime;
        } else {
            throw new Exception("This is not a valid time (" . $mealTime . ")");
        }
    }

    /**
     * @return int
     */
    public function getKitchenId(): int
    {
        return $this->kitchenId;
    }

    /**
     * @param int $kitchenId
     */
    public function setKitchenId(int $kitchenId): void
    {
        $this->kitchenId = $kitchenId;
    }

    /**
     * @return string
     */
    public function getKitchenName(): string
    {
        return $this->kitchenName;
    }

    /**
     * @param string $kitchenName
     */
    public function setKitchenName(string $kitchenName): void
    {
        $this->kitchenName = $kitchenName;
    }

    /**
     * @return int
     */
    public function getRegistrationCutoff(): int
    {
        return $this->registrationCutoff;
    }

    /**
     * @param int $registrationCutoff
     */
    public function setRegistrationCutoff(int $registrationCutoff): void
    {
        $this->registrationCutoff = $registrationCutoff;
    }

    /**
     * @return string
     */
    public function getComments(): string
    {
        if ($this->comments) {
            return $this->comments;
        }

        return "";
    }

    /**
     * @param string $comments
     */
    public function setComments(string $comments): void
    {
        $this->comments = $comments;
    }

    /**
     * @return array
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param array $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = $groups;
    }

    /**
     * Check if mealTime is valid
     * @param string $mealTime
     * @return bool
     */
    public function isValidMealTime(string $mealTime): bool
    {
        try {
            Core_Date::get(Core_Date::now() . ' ' . $mealTime);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getEditUrl(): string
    {
        return $this->editUrl;
    }

    /**
     * @param string $editUrl
     */
    public function setEditUrl(string $editUrl): void
    {
        $this->editUrl = $editUrl;
    }

    /**
     * @return string
     */
    public function getNameError(): string
    {
        return $this->nameError;
    }

    /**
     * @param string $nameError
     */
    public function setNameError(string $nameError): void
    {
        $this->nameError = $nameError;
    }

    /**
     * @return string
     */
    public function getBusinessDaysError(): string
    {
        return $this->businessDaysError;
    }

    /**
     * @param string $businessDaysError
     */
    public function setBusinessDaysError(string $businessDaysError): void
    {
        $this->businessDaysError = $businessDaysError;
    }

    /**
     * @return string
     */
    public function getMealTimeError(): string
    {
        return $this->mealTimeError;
    }

    /**
     * @param string $mealTimeError
     */
    public function setMealTimeError(string $mealTimeError): void
    {
        $this->mealTimeError = $mealTimeError;
    }

    /**
     * @return string
     */
    public function getKitchenError(): string
    {
        return $this->kitchenError;
    }

    /**
     * @param string $kitchenError
     */
    public function setKitchenError(string $kitchenError): void
    {
        $this->kitchenError = $kitchenError;
    }

    /**
     * @return string
     */
    public function getRegistrationCutoffError(): string
    {
        return $this->registrationCutoffError;
    }

    /**
     * @param string $registrationCutoffError
     */
    public function setRegistrationCutoffError(string $registrationCutoffError): void
    {
        $this->registrationCutoffError = $registrationCutoffError;
    }

    /**
     * @return string
     */
    public function getCommentsError(): string
    {
        return $this->commentsError;
    }

    /**
     * @param string $commentsError
     */
    public function setCommentsError(string $commentsError): void
    {
        $this->commentsError = $commentsError;
    }

    /**
     * @return string
     */
    public function getGroupsError(): string
    {
        return $this->groupsError;
    }

    /**
     * @param string $groupsError
     */
    public function setGroupsError(string $groupsError): void
    {
        $this->groupsError = $groupsError;
    }

    /**
     * @return string
     */
    public function getGeneralError(): string
    {
        return $this->generalError;
    }

    /**
     * @param string $generalError
     */
    public function setGeneralError(string $generalError): void
    {
        $this->generalError = $generalError;
    }

    /**
     * @return array
     */
    public function getAllErrors(): array
    {
        return $this->allErrors;
    }

    /**
     * Check if this MealType object has valid values inside it (set error messages if not).
     * @param int $nrOfGroupsdFromDb - number of Groups found in Database
     * @return bool
     */
    public function isValid(int $nrOfGroupsdFromDb): bool
    {
        $isValid = true;

        if (empty($this->name)) {
            $this->nameError = 'Söögikorra nimi on kohustuslik';
            $this->allErrors[self::NAME_ERROR] = $this->nameError;
            $isValid = false;
        }

        if (empty($this->businessDays)) {
            $this->businessDaysError = 'Vähemalt üks nädalapäev peab valitud olema';
            $this->allErrors[self::BUSINESS_DAYS_ERROR] = $this->businessDaysError;
            $isValid = false;
        }

        try {
            Core_Date::get(Core_Date::now() . ' ' . $this->mealTime);
        } catch (Exception) {
            $this->mealTimeError = 'Sellist aega ei saa söögikorrale määrata!';
            $this->allErrors[self::MEAL_TIME_ERROR] = $this->mealTimeError;
            $isValid = false;
        }

        if (empty($this->registrationCutoff) || $this->registrationCutoff < 0) {
            $this->registrationCutoffError = 'Peab olema positiivne täisarv!';
            $this->allErrors[self::REG_CUTOFF_ERROR] = $this->registrationCutoffError;
            $isValid = false;
        }

        if (empty($this->kitchenId) || !KitchenModel::getKitchenById($this->kitchenId)) {
            $this->kitchenError = 'Valitud kööki ei ole olemas';
            $this->allErrors[self::KITCHEN_ID_ERROR] = $this->kitchenError;
            $isValid = false;
        }

        if (count($this->groups) != $nrOfGroupsdFromDb) {
            $this->groupsError = 'Valitud gruppide hulgas on grupp, keda süsteemist ei leitud!';
            $this->allErrors[self::GROUPS_ERROR] = $this->groupsError;
            $isValid = false;
        }

        return $isValid;
    }

    /**
     * Check if this meal type is enabled on specific weekday (1-7)
     * @param int $dayNr - Day of the week (1 - Monday ... 7 - Sunday)
     * @return bool
     */
    public function isDayEnabled(int $dayNr): bool
    {
        if (!empty($dayNr) && in_array($dayNr, $this->businessDays)) {
            return true;
        }

        return false;
    }

    /**
     * Returns business days as a string
     * @param string $sep - Separator (that separates business days - default is space)
     * @return string
     */
    public function getEnabledDaysStr(string $sep = ' '): string
    {
        $resultArr = [];

        foreach ($this->businessDays as $businessDay) {
            $resultArr[] = self::ENTIRE_WEEK[$businessDay];
        }

        return implode($sep, $resultArr);
    }
}