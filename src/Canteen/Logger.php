<?php
require __DIR__ . '/../../preventDirectAccess.php';

class Logger
{
    /** @var LogRow[] $logRows */
    private array $logRows = [];

    public function addLogRow(LogRow $logRow) {
        $this->logRows[] = $logRow;
    }

    public function save(): void
    {
        global $DB;

        if (empty($this->logRows)) {
            error_log("Can't save log, because there are no rows to save!");
            return;
        }

        $txt = "
            INSERT INTO app_log (tbl_name, record_id, col_name, old_value, new_value, edited_at, edited_by) 
            VALUES " . $this->logRowsToStr();

        // SQL was escaped in logRowToStr() method, so no need to do it again (skip $DB->prepareSql())
        $DB->db_edit($txt);
    }

    /**
     *
     * @return string
     */
    private function logRowsToStr(): string
    {
        global $DB;

        $rows = [];

        foreach ($this->logRows as $logRow) {
            $txt = "('?0', ?1, '?2', '?3', '?4', '?5', '?6')";
            $sql = $DB->prepareSQL($txt, [
                $logRow->getTableName(),
                $logRow->getRecordId(),
                $logRow->getColName(),
                $logRow->getOldValue(),
                $logRow->getNewValue(),
                $logRow->getTimestamp(),
                $logRow->getChangedBy()
            ]);
            $rows[] = $sql;
        }

        return implode(', ', $rows);
    }
}