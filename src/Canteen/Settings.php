<?php
require __DIR__ . '/../../preventDirectAccess.php';

class Settings
{
    const AD_URL = 'AD_URL';
    const CODE = 'setting_code';
    const VALUE = 'setting_value';
    const DESCRIPTION = 'description';

    private string $domainUrl;
    private string $domainUrlDescription;

    /**
     * Populates this object with values from DB.
     * @return void
     */
    public function populateSettings(): void
    {
        global $DB;

        $txt = 'SELECT * FROM app_setting';
        $data = $DB->query($txt);

        foreach ($data as $item) {
            if ($item[self::CODE] == self::AD_URL) {
                $this->domainUrl = $item[self::VALUE] ?: '';
                $this->domainUrlDescription = $item[self::DESCRIPTION];
            }
        }
    }

    /**
     * @return string
     */
    public function getDomainUrl(): string
    {
        return $this->domainUrl;
    }

    /**
     * @param string $domainUrl
     */
    public function setDomainUrl(string $domainUrl): void
    {
        $this->domainUrl = $domainUrl ?: "";
    }

    /**
     * @return string
     */
    public function getDomainUrlDescription(): string
    {
        return $this->domainUrlDescription;
    }

    /**
     * @param string $domainUrlDescription
     */
    public function setDomainUrlDescription(string $domainUrlDescription): void
    {
        $this->domainUrlDescription = $domainUrlDescription;
    }
}