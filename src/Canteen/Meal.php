<?php
require __DIR__ . '/../../preventDirectAccess.php';

class Meal
{
    private int $mealType;
    private DateTime $mealDate;
    private string $day;
    private bool $isEnabled = true;
    private bool $isSelected = false;
    private bool $didParticipate = false;

    public function __construct(int $mealType, string $mealDate = "")
    {
        $this->mealType = $mealType;
        if ($mealDate) {
            $date = Core_Date::get($mealDate);
            $this->mealDate = $date;
            $this->day = $date->format('d');
        }
    }

    /**
     * @return int
     */
    public function getMealType(): int
    {
        return $this->mealType;
    }

    /**
     * @param int $mealType
     */
    public function setMealType(int $mealType): void
    {
        $this->mealType = $mealType;
    }

    /**
     * @return DateTime
     */
    public function getMealDate(): DateTime
    {
        return $this->mealDate;
    }

    /**
     * @param string $mealDate
     */
    public function setMealDate(string $mealDate): void
    {
        $date = Core_Date::get($mealDate);
        $this->day = $date->format('d');
        $this->mealDate = $date;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getMealDateStr(string $format = 'Y-m-d H:i:s'): string
    {
        if (!empty($this->mealDate)) {
            return $this->mealDate->format($format);
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDay(): string
    {
        return $this->day;
    }

    public function setDay(string $day) {
        $this->day = $day;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     */
    public function setIsEnabled(bool $isEnabled): void
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * @return bool
     */
    public function isSelected(): bool
    {
        return $this->isSelected;
    }

    /**
     * @param bool $isSelected
     */
    public function setIsSelected(bool $isSelected): void
    {
        $this->isSelected = $isSelected;
    }

    /**
     * @return bool
     */
    public function isDidParticipate(): bool
    {
        return $this->didParticipate;
    }

    /**
     * @param bool $didParticipate
     */
    public function setDidParticipate(bool $didParticipate): void
    {
        $this->didParticipate = $didParticipate;
    }
}