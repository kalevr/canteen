<?php
require __DIR__ . '/../../preventDirectAccess.php';

class AcademicYear
{
    /**
     * Find first and last day of an academic year (by academic year id)
     * @param int $yearId - app_year ID
     * @return array - Returns array of first and last date [first, last]
     */
    public static function getFirstAndLastDay(int $yearId) {
        global $DB;

        $txt = "
            SELECT * from app_year
            WHERE id = ?0
        ";
        $sql = $DB->prepareSQL($txt, [$yearId]);
        $res = $DB->query($sql);
        $firstDayOfMonth = '9999-12-31';
        $lastDayOfMonth = '0000-01-01';

        if($res && $res[0]) {
            try {
                $firstDayOfMonth =
                    Core_Date::firstDayOfMonth($res[0]['start_year'] . '-' . $res[0]['start_month'] . '-01');
                $lastDayOfMonth =
                    Core_Date::lastDayOfMonth($res[0]['end_year'] . '-' . $res[0]['end_month'] . '-01');
            } catch (Exception) {
                // do nothing - if Core_Date methods throw exception, default values will be used (impossible dates)
            }

        }

        return [$firstDayOfMonth, $lastDayOfMonth];
    }

    /**
     * Get ID of current academic year
     * @return int
     */
    public static function getCurrent() {
        global $DB;

        $now = Core_Date::now();
        $nowSplit = explode('-', $now);

        $month = $nowSplit[1];
        $year = $nowSplit[0];

        $txt = "
            SELECT id FROM app_year 
            WHERE (start_year = ?0 AND start_month <= ?1) OR 
                  (end_year = ?0 AND end_month >= ?1)
        ";
        $sql = $DB->prepareSQL($txt, [$year, $month]);
        $res = $DB->query($sql);

        if ($res && $res[0] && $res[0]['id']) {
            return $res[0]['id'];
        }

        return -1;
    }
}