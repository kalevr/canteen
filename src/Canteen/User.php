<?php
require __DIR__ . '/../../preventDirectAccess.php';

class User
{
    private int $id;
    private string $firstName = "";
    private string $lastName = "";
    private string $historyUrl = "";
    private string $deletedAt = "";
    /** @var Meal[] $meals */
    private array $meals = [];

    public function __construct(string $firstName = "", string $lastName = "")
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getFullname()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @return string
     */
    public function getHistoryUrl(): string
    {
        return $this->historyUrl;
    }

    /**
     * @param string $historyUrl
     */
    public function setHistoryUrl(string $historyUrl): void
    {
        $this->historyUrl = $historyUrl;
    }

    /**
     * @return string
     */
    public function getDeletedAt(): string
    {
        return $this->deletedAt;
    }

    /**
     * @param string $deletedAt
     */
    public function setDeletedAt(string $deletedAt = ''): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return Meal[]
     */
    public function getMeals(): array
    {
        return $this->meals;
    }

    public function addMeal(Meal $meal)
    {
        $this->meals[$meal->getDay()] = $meal;
    }

    /**
     * Add days when meal is not ordered (and then sort by day)
     * @param array $allDaysInMonth
     * @param int $mealTypeId
     * @return void
     */
    public function addEmptyMeals(array $allDaysInMonth, int $mealTypeId)
    {
        foreach ($allDaysInMonth as $day => $value) {
            $dayPadded = str_pad($day, 2, '0', STR_PAD_LEFT);

            if (!array_key_exists($dayPadded, $this->meals)) {
                $this->meals[$dayPadded] = new Meal($mealTypeId);
                $this->meals[$dayPadded]->setIsEnabled($value['is_enabled']);
                $this->meals[$dayPadded]->setDay($dayPadded);
            }
        }

        ksort($this->meals);
    }

    /**
     * Finds meals that the second user object does not have.
     * @param User $secondUser User object to compare against (must have same id)
     * @return Meal[] Returns array of meals that are not present in second user object
     * @throws Exception
     */
    public function findUniqueMealsForUser(User $secondUser): array
    {
        if ($this->id !== $secondUser->getId()) {
            throw new Exception("Both users must have the same id!");
        }

        $secUserMeals = $secondUser->getMeals();
        $unique = [];

        foreach ($this->meals as $day => $meal) {
            if (!array_key_exists($day, $secUserMeals)) {
                $unique[$day] = $meal;
            }
        }

        return $unique;
    }

    /**
     * Finds participations that the second user object does not have.
     * @param User $secondUser User object to compare against (must have same id)
     * @return Meal[] Returns array of participations that are not present in second user object
     * @throws Exception
     */
    public function findUniqueParticipationsForUser(User $secondUser): array
    {
        if ($this->id !== $secondUser->getId()) {
            throw new Exception("Both users must have the same id!");
        }

        $secUserMeals = $secondUser->getMeals();
        $unique = [];

        foreach ($this->meals as $day => $meal) {
            if (!array_key_exists($day, $secUserMeals) || $meal->isDidParticipate() != $secUserMeals[$day]->isDidParticipate()) {
                $unique[$day] = $meal;
            }
        }

        return $unique;
    }

    /**
     * Gets Meal object if user has a meal on given date (otherwise returns null).
     * @param string $date
     * @return Meal|null
     */
    public function getMealByDate(string $date): ?Meal
    {
        if (!$date) {
            return null;
        }

        foreach ($this->meals as $meal) {
            if ($meal->getMealDate()->format('Y-m-d') == $date) {
                return $meal;
            }
        }

        return null;
    }
}