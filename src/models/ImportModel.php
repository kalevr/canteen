<?php
require __DIR__ . '/../../preventDirectAccess.php';

class ImportModel extends BaseModel
{
    /**
     * Check if any of the e-mails provided are already in the database.
     *
     * @param array $emails - Array of e-mails you wish to check
     * @return array - Returns an array of e-mails that had a match in the database.
     */
    public static function findEmailsAlreadyInDb(array $emails): array
    {
        global $DB;

        if (!$emails) {
            return [];
        }

        $txt = "
            SELECT email FROM app_user
            WHERE email IN (?0)
        ";
        $sql = $DB->prepareSQL($txt, [$emails]);
        return $DB->query($sql);
    }
}