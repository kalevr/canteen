<?php
require __DIR__ . '/../../preventDirectAccess.php';

class MealTypeModel extends BaseModel
{
    public static function getAllMealTypes($yearId = 0)
    {
        global $DB;
        $filter = "";

        if ($yearId) {
            $filter = " AND g.app_year = " . $yearId;
        }

        $txt = "
            SELECT mt.id, (
                SELECT gmt.app_group 
                FROM group_meal_type gmt 
                LEFT JOIN app_group g ON gmt.app_group = g.id
                WHERE mt.id = gmt.meal_type {$filter}
                ORDER BY g.sort_order LIMIT 1) AS app_group,
                mt.meal_type_name, mt.meal_time, k.kitchen_name, mt.registration_cutoff, mt.comments, mt.business_days 
            FROM meal_type mt
            LEFT JOIN kitchen k on mt.kitchen = k.id
            WHERE mt.created_at <= '?0' AND (mt.deleted_at IS NULL OR mt.deleted_at >= '?0')
            ORDER BY mt.meal_time
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function getAllMealTypesForUser(int $userId, bool $isHomeroomTeacher = false, int $yearId = 0) {
        global $DB;

        $filter = "";

        if ($yearId) {
            $filter = " AND g.app_year = " . $yearId;
        }

        $query = $isHomeroomTeacher ? 'AND ug.app_role = 4' : 'AND ug.app_role = 5';
        $groupBy = $isHomeroomTeacher ? 'GROUP BY mt.meal_type_name' : '';

        $txt = "
            SELECT mt.id, mt.meal_type_name, gmt.app_group 
            FROM meal_type mt
            JOIN group_meal_type gmt ON mt.id = gmt.meal_type 
                AND gmt.created_at <= '?1' AND (gmt.deleted_at IS NULL OR gmt.deleted_at >= '?1')
            JOIN app_user_group ug ON gmt.app_group = ug.app_group {$query}
            LEFT JOIN app_group g ON gmt.app_group = g.id
            WHERE ug.app_user = ?0 {$filter} AND ug.deleted_by IS NULL
            {$groupBy}
            ORDER BY mt.meal_time
        ";
        $sql = $DB->prepareSQL($txt, [$userId, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function getMealTypeById(int $mealTypeId, int $yearId = 0): array
    {
        global $DB;

        $filter = "";

        if ($yearId) {
            $filter = "AND g.app_year = " . $yearId;
        }

        $txt = "
            SELECT mt.id, mt.meal_type_name, mt.business_days, mt.meal_time, mt.kitchen, mt.registration_cutoff, 
                   mt.comments, g.id AS app_group, g.group_name
            FROM meal_type mt
            LEFT JOIN group_meal_type gmt ON mt.id = gmt.meal_type 
                AND gmt.created_at <= '?1' AND (gmt.deleted_at IS NULL OR gmt.deleted_at >= '?1')
            LEFT JOIN app_group g ON gmt.app_group = g.id {$filter}
            WHERE mt.id = ?0
            ORDER BY g.sort_order
        ";

        $sql = $DB->prepareSQL($txt, [$mealTypeId, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function addGroupMealType(int $groupId, int $mealTypeId, int $createdBy): bool {
        global $DB;

        $txt = "
            INSERT INTO group_meal_type (app_group, meal_type, starting_from, created_at, created_by)   
            VALUES (?0, ?1, '?2', '?3', ?4)
        ";
        $sql = $DB->prepareSQL($txt, [
            $groupId,
            $mealTypeId,
            Core_Date::now(),
            Core_Date::now('Y-m-d H:i:s'),
            $createdBy]);
        return $DB->db_edit($sql);
    }

    public static function removeGroupMealType(int $groupMealTypeId, int $deletedBy): bool {
        global $DB;

        $txt = "
            UPDATE group_meal_type
            SET deleted_at = '?0', deleted_by = ?1
            WHERE id = ?2
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $deletedBy, $groupMealTypeId]);
        return $DB->db_edit($sql);
    }

    public static function getIdsOfGroupsInMealType(int $mealTypeId) {
        global $DB;

        $txt = "
            SELECT mt.id, gmt.app_group, g.group_name, gmt.id AS group_mt_id
            FROM meal_type mt
            LEFT JOIN group_meal_type gmt ON mt.id = gmt.meal_type
            LEFT JOIN app_group g ON gmt.app_group = g.id
            WHERE mt.id = ?0 AND mt.created_at <= '?1' AND (mt.deleted_at IS NULL OR mt.deleted_at >= '?1')
                AND gmt.created_at <= '?1' AND (gmt.deleted_at IS NULL OR gmt.deleted_at >= '?1') 
            ORDER BY g.sort_order
        ";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    /**
     * Creates MealType object from input. Input (array) must contain at least minimal amount of parameters to
     * create MealType object.
     * @param array $data - Key => Value pairs to build MealType object with
     * @return MealType
     * @throws Exception - throws exception when mandatory info is not present in the array!
     */
    public static function createMealTypeObj(array $data): MealType
    {
        if (!array_key_exists(MealType::NAME, $data) || !array_key_exists(MealType::MEAL_TIME, $data)) {
            throw new Exception("Can't create object from this array!");
        }
        $mealType = new MealType($data[MealType::NAME], $data[MealType::MEAL_TIME]);

        if (array_key_exists('id', $data)) {
            $mealType->setId($data['id']);
        }

        if (array_key_exists(MealType::KITCHEN_ID, $data)) {
            $mealType->setKitchenId($data[MealType::KITCHEN_ID]);
        }

        if (array_key_exists(MealType::KITCHEN_NAME, $data)) {
            $mealType->setKitchenName($data[MealType::KITCHEN_NAME]);
        }

        if (array_key_exists(MealType::BUSINESS_DAYS, $data)) {
            $business_days = explode(',', $data[MealType::BUSINESS_DAYS]);
            $mealType->setBusinessDays($business_days ?: []);
        }

        if (array_key_exists(MealType::REG_CUTOFF, $data)) {
            $mealType->setRegistrationCutoff($data[MealType::REG_CUTOFF]);
        }

        if (array_key_exists(MealType::COMMENTS, $data)) {
            $mealType->setComments($data[MealType::COMMENTS]);
        }

        if (array_key_exists(MealType::NAME_ERROR, $data)) {
            $mealType->setNameError($data[MealType::NAME_ERROR]);
        }

        if (array_key_exists(MealType::MEAL_TIME_ERROR, $data)) {
            $mealType->setMealTimeError($data[MealType::MEAL_TIME_ERROR]);
        }

        if (array_key_exists(MealType::KITCHEN_ID_ERROR, $data)) {
            $mealType->setKitchenError($data[MealType::KITCHEN_ID_ERROR]);
        }

        if (array_key_exists(MealType::REG_CUTOFF_ERROR, $data)) {
            $mealType->setRegistrationCutoffError($data[MealType::REG_CUTOFF_ERROR]);
        }

        return $mealType;
    }

    public static function updateMealType(array $ins, int $id): bool
    {
        global $DB;

        return $DB->update('meal_type', $ins, $id);
    }

    public static function insertMealType(array $ins) {
        global $DB;

        return $DB->insert('meal_type', $ins);
    }
}