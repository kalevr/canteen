<?php
require __DIR__ . '/../../preventDirectAccess.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Date.php';

class UserModel extends BaseModel
{
    public static function getUsersByRoleName(string $roleName, int $yearId = null): array
    {
        global $DB;

        if (!$roleName) {
            return [];
        }

        $join1 = 'app_user_role ur ON u.id = ur.app_user AND ur.deleted_at IS NULL';
        $join2 = 'ur.app_role = r.id';

        // homeroom-teacher is assigned in app_user_group table (also in app_user_role)
        if ($roleName == 'homeroom-teacher') {
            $join1 = 'app_user_group ug ON u.id = ug.app_user AND ug.deleted_at IS NULL 
             JOIN app_group g ON ug.app_group = g.id AND g.app_year = ?1';
            $join2 = 'ug.app_role = r.id';
        }

        $txt = "
            SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) AS name, u.email, u.locked, u.comments, (
                SELECT l.login_time
                FROM log_login l
                WHERE u.id = l.app_user AND l.success
                ORDER BY l.login_time DESC
                LIMIT 1) as last_login 
            FROM app_user u
            JOIN {$join1} 
            JOIN app_role r ON {$join2} 
            WHERE r.role_code = '?0' AND u.deleted_at IS NULL
        ";
        $sql = $DB->prepareSQL($txt, [$roleName, $yearId]);
        return $DB->query($sql);
    }

    public static function getUsersByGroupId(int $groupId)
    {
        global $DB;

        $txt = "
            SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) AS name, u.email, u.locked, u.comments, (
                SELECT l.login_time
                FROM log_login l
                WHERE u.id = l.app_user AND l.success
                ORDER BY l.login_time DESC
                LIMIT 1) as last_login 
            FROM app_user u
            JOIN app_user_group ug ON u.id = ug.app_user AND ug.deleted_at IS NULL
            JOIN app_group g ON ug.app_group = g.id 
            WHERE g.id = ?0 AND u.deleted_at IS NULL AND ug.app_role = 5
        ";
        $sql = $DB->prepareSQL($txt, [$groupId]);
        return $DB->query($sql);
    }

    public static function getUsersWithoutRoleOrGroup(int $yearId)
    {
        global $DB;

        $txt = "
            SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) AS name, u.email, u.locked, u.comments, (
                SELECT l.login_time
                FROM log_login l
                WHERE u.id = l.app_user AND l.success
                ORDER BY l.login_time DESC
                LIMIT 1) as last_login
            FROM app_user u
            WHERE NOT EXISTS 
                (SELECT *
                FROM app_user_role ur
                WHERE u.id = ur.app_user)
            AND NOT EXISTS
                (SELECT *
                FROM app_user_group ug
                JOIN app_group g ON ug.app_group = g.id 
                WHERE u.id = ug.app_user AND g.app_year = ?0)
            AND u.deleted_at IS NULL
        ";

        $sql = $DB->prepareSQL($txt, [$yearId]);
        return $DB->query($sql);
    }

    public static function searchUsers(string $search, bool $onlyActive = false): array
    {
        global $DB;

        $where = '';

        if ($onlyActive) {
            $where = "created_at <= '?0' AND (deleted_at IS NULL OR deleted_at > '?0') AND";
        }

        $txt = "SELECT id, CONCAT(first_name, ' ', last_name, if(deleted_by IS NULL, '', ' (kustutatud)')) AS name
                FROM app_user 
                WHERE {$where} CONCAT(first_name, ' ', last_name) LIKE '%?1%'
                ORDER BY first_name";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $search]);

        return $DB->query($sql);
    }

    public static function getUser($id): array
    {
        global $DB;

        $txt = "
            SELECT u.id, u.first_name, u.last_name, u.email, u.locked, u.comments, ur.app_role AS user_role, 
                   IF(u.deleted_at < NOW(), 1, 0) AS is_deleted
            FROM app_user u 
            LEFT JOIN app_user_role ur ON u.id = ur.app_user AND ur.app_role NOT IN (4, 5) AND ur.deleted_at IS NULL
            WHERE u.id = ?0
        ";

        $sql = $DB->prepareSQL($txt, [$id]);
        return $DB->query($sql);
    }

    public static function getUsersById(array $ids): array
    {
        global $DB;

        $idsStr = implode(',', $ids);

        $txt = "
            SELECT id, CONCAT(first_name, ' ', last_name) as user_name
            FROM app_user 
            WHERE id IN (?0)
        ";
        $sql = $DB->prepareSQL($txt, [$idsStr]);

        return $DB->query($sql);
    }

    /**
     * Lists all roles and how many users there are in any given role (homeroom-teacher is taken from
     * app_user_group table, not from app_user_role table). Also shows number of users without any role/group.
     * @param int $yearId
     * @return array
     */
    public static function getUserCountsByRole(int $yearId): array
    {
        global $DB;

        $txt = "
            SELECT r.id, r.role_code, 
                (
                    IF(r.role_code = 'homeroom-teacher',
                        (
                            SELECT COUNT(*)
                            FROM app_user_group ug
                            LEFT JOIN app_user u ON ug.app_user = u.id
                            JOIN app_group g ON ug.app_group = g.id AND g.app_year = ?1
                            WHERE ug.app_role = r.id AND ug.deleted_at IS NULL AND u.deleted_at IS NULL
                        ),
                        (
                            SELECT COUNT(*)
                            FROM app_user_role ur
                            LEFT JOIN app_user u ON ur.app_user = u.id
                            WHERE ur.app_role = r.id AND ur.starting_from <= '?0' 
                              AND (ur.valid_until IS NULL OR ur.valid_until >= '?0') 
                              AND ur.deleted_at IS NULL AND u.deleted_at IS NULL
                ))) AS role_count,
                (
                    SELECT COUNT(*)
                    FROM app_user u
                    WHERE NOT EXISTS (
                        SELECT *
                        FROM app_user_role ur
                        WHERE u.id = ur.app_user
                    )
                    AND NOT EXISTS 
                    (
                        SELECT *
                        FROM app_user_group ug
                        JOIN app_group g ON ug.app_group = g.id 
                        WHERE u.id = ug.app_user AND g.app_year = ?1
                    )
                    AND u.deleted_at IS NULL
                ) AS no_role_group
            FROM app_role r
            WHERE r.role_code IN ('admin', 'observer', 'accountant', 'cook', 'homeroom-teacher')
            ORDER BY r.id        
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now(), $yearId]);
        return $DB->query($sql);
    }

    /**
     * Get all special roles (all except 'eater' and 'homeroom-teacher')
     * @return array
     */
    public static function getAllSpecialRoles(): array
    {
        global $DB;

        $txt = "
            SELECT * 
            FROM app_role r
            WHERE r.role_code != 'eater' AND r.role_code != 'homeroom-teacher'        
        ";

        return $DB->query($txt);
    }

    /**
     * Gets all roles that a user has.
     * @param int $userId - app_user ID
     * @return array
     */
    public static function getUserRoles(int $userId): array
    {
        global $DB;

        $txt = "
            SELECT r.role_code FROM app_user_role ur
            JOIN app_user u ON ur.app_user = u.id
            JOIN app_role r ON ur.app_role = r.id
            WHERE u.id = ?0 AND ur.deleted_by IS NULL
        ";

        $sql = $DB->prepareSQL($txt, [$userId]);
        return $DB->query($sql);
    }

    /**
     * Sets user role.
     * @param int $userId - app_user ID
     * @param int $roleId - app_role ID
     * @param int $createdBy - app_user ID
     * @return bool - Returns true, if successful, false if not.
     */
    public static function setUserRole(int $userId, int $roleId, int $createdBy): bool
    {
        global $DB;

        if (empty($userId) || empty($roleId) || $userId < 0 || $roleId < 0) {
            return false;
        }

        $txt = "
            INSERT INTO app_user_role (app_user, app_role, starting_from, created_at, created_by)
            VALUES (?0, ?1, '?2', '?3', ?4);        
        ";
        $sql = $DB->prepareSQL($txt, [
            $userId,
            $roleId,
            Core_Date::now(),
            Core_Date::now('Y-m-d H:i:s'),
            $createdBy
        ]);

        return $DB->db_edit($sql);
    }

    /**
     * Remove user role.
     * @param int $userId - app_user ID
     * @param int $roleId - app_role ID
     * @param int $deletedBy - app_user ID
     * @return bool
     */
    public static function removeUserRole(int $userId, int $roleId, int $deletedBy): bool
    {
        global $DB;

        if (empty($userId) || empty($roleId) || $userId < 0 || $roleId < 0) {
            return false;
        }

        $txt = "
            UPDATE app_user_role ur
            SET ur.deleted_at = '?3', ur.deleted_by = ?2
            WHERE ur.app_user = ?0 AND ur.app_role = ?1 AND ur.deleted_at IS NULL
        ";
        $sql = $DB->prepareSQL($txt, [
            $userId,
            $roleId,
            $deletedBy,
            Core_Date::now('Y-m-d H:i:s'),
        ]);

        return $DB->db_edit($sql);
    }


    /**
     * Mark user as deleted in app_user table.
     * @param int $id - app_user ID (of the person about to be deleted)
     * @param $deletedBy - app_user ID (of the person doing the deleting)
     * @return bool
     */
    public static function markUserAsDeleted(int $id, string $deletedBy): bool
    {
        global $DB;

        if (!$id) {
            return false;
        }

        $txt = "
            UPDATE app_user u
            SET u.deleted_at = '?1', u.deleted_by = ?2
            WHERE u.id = ?0 AND u.deleted_at IS NULL
        ";
        $sql = $DB->prepareSQL($txt, [$id, Core_Date::now('Y-m-d H:i:s'), $deletedBy]);

        return $DB->db_edit($sql);
    }

    /**
     * Mark user as not deleted in app_user table.
     * @param int $id - app_user ID (of the person about to be restored)
     * @return bool
     */
    public static function markUserAsNotDeleted(int $id): bool
    {
        global $DB;

        if (!$id) {
            return false;
        }

        $txt = "
            UPDATE app_user u
            SET u.deleted_at = NULL, u.deleted_by = NULL
            WHERE u.id = ?0 AND deleted_at IS NOT NULL
        ";
        $sql = $DB->prepareSQL($txt, [$id]);
        return $DB->db_edit($sql);
    }

    public
    static function createUser(array $data): int
    {
        global $DB;

        $txt = "INSERT INTO app_user 
                (first_name, last_name, email, passwd, locked, comments, created_by)
                VALUES ('?0', '?1', '?2', '?3', '?4', '?5', ?6)";

        $sql = $DB->prepareSQL($txt, [
            $data['first_name'],
            $data['last_name'],
            $data['email'],
            $data['passwd'],
            $data['locked'],
            $data['comments'],
            $data['created_by']]);

        $res = $DB->db_edit($sql);

        if (!$res) {
            return -1;
        }

        return $DB->get_last_insert_id();
    }

    /**
     * Edit user.
     * @param string $table - name of the users table
     * @param array $ins - array of key/value pairs that need to be changed.
     * @param int $id - app_user ID (id of the user being changed)
     * @return bool - returns true if update is successful (or if $ins is empty)
     */
    public
    static function editUser(string $table, array $ins, int $id): bool
    {
        global $DB;

        if (!$ins) {
            return true;
        }

        return $DB->update($table, $ins, $id);
    }
}