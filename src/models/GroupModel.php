<?php
require __DIR__ . '/../../preventDirectAccess.php';

class GroupModel extends BaseModel
{
    public static function getAllGroups($yearId, $includeDeletedUsers = true) : array
    {
        global $DB;

        $inc = '';
        if (!$includeDeletedUsers) {
            $inc = "AND u.deleted_at IS NULL";
        }

        $txt = "
            SELECT g.id, g.group_name, g.starting_from, g.valid_until, 
                    (
                        SELECT COUNT(*) 
                        FROM app_user_group ug
                        LEFT JOIN app_user u ON ug.app_user = u.id
                        WHERE ug.app_role = 5 AND ug.app_group = g.id AND ug.created_at <= '?0' 
                            AND (ug.deleted_at IS NULL OR ug.deleted_at >= '?0') {$inc}
                    ) AS nr_of_eaters, 
                    (
                        SELECT COUNT(*) 
                        FROM app_user_group ug
                        LEFT JOIN app_user u ON ug.app_user = u.id
                        WHERE ug.app_role = 4 AND ug.app_group = g.id AND ug.created_at <= '?0' 
                            AND (ug.deleted_at IS NULL OR ug.deleted_at >= '?0') {$inc}
                    ) AS nr_of_teachers  
            FROM app_group g
            WHERE g.created_at <= '?0' AND (g.deleted_at IS NULL OR g.deleted_at >= '?0') AND app_year = ?1
            ORDER BY g.sort_order;
        ";


        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $yearId]);
        return $DB->query($sql);
    }

    public static function getGroupWithMembers(int $groupId): array
    {
        global $DB;

        $txt = "
            SELECT u.id, g.group_name, g.starting_from, g.valid_until, g.sort_order, 
                   CONCAT(u.first_name, ' ', u.last_name) as user_name, ug.app_role
            FROM app_group g
            LEFT JOIN app_user_group ug ON ug.app_group = g.id
            LEFT JOIN app_user u ON u.id = ug.app_user 
                AND ug.created_at <= '?1' AND (ug.deleted_at IS NULL OR ug.deleted_at >= '?1')
            WHERE g.id = ?0 AND g.created_at <= '?1' AND (g.deleted_at IS NULL OR g.deleted_at >= '?1') 
                AND ug.deleted_by IS NULL
            ORDER BY u.last_name, u.first_name
        ";
        $sql = $DB->prepareSQL($txt, [$groupId, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function searchGroups(string $search, int $currentYearId): array
    {
        global $DB;

        $txt = "
            SELECT g.id, g.group_name 
            FROM app_group g
            WHERE g.app_year = ?0 AND g.group_name LIKE '%?1%'
            ORDER BY g.sort_order
        ";
        $sql = $DB->prepareSQL($txt, [$currentYearId, $search]);
        return $DB->query($sql);
    }

    public static function getGroupsById(array $ids): array
    {
        global $DB;

        $idsStr = implode(',', $ids);

        $txt = "
            SELECT id, group_name
            FROM app_group
            WHERE id in (?0)
        ";
        $sql = $DB->prepareSQL($txt, [$idsStr]);
        return $DB->query($sql);
    }

    public static function getIdsOfGroupMembers(int $groupId): array
    {
        global $DB;

        $txt = "
            SELECT u.id, ug.app_role, ug.id as user_group_id
            FROM app_group g
            LEFT JOIN app_user_group ug on g.id = ug.app_group
            LEFT JOIN app_user u on ug.app_user = u.id
            WHERE g.id = ?0 AND g.created_at <= '?1' AND (g.deleted_at IS NULL OR g.deleted_at >= '?1')
                AND ug.created_at <= '?1' AND (ug.deleted_at IS NULL OR ug.deleted_at >= '?1')
        ";
        $sql = $DB->prepareSQL($txt, [$groupId, Core_Date::now('Y-m-d H:i:s')]);

        return $DB->query($sql);
    }

    public static function updateGroup(int $groupId, string $groupName, string $startingFrom, string $validUntil, int $sortOrder): bool
    {
        global $DB;

        $setValidUntil = "";
        if ($validUntil) {
            $setValidUntil = ", valid_until = '" . $DB->escape($validUntil) . "'";
        }

        $txt = "
            UPDATE app_group
            SET group_name = '?0', sort_order = ?1, starting_from = '?2'{$setValidUntil} 
            WHERE id = ?3
        ";
        $sql = $DB->prepareSQL($txt, [$groupName, $sortOrder, $startingFrom, $groupId]);

        return $DB->db_edit($sql);
    }

    public static function insertGroup(int $yearId, string $groupName, string $startingFrom, string $validUntil,
                                       int $sortOrder, int $createdBy): bool|int
    {
        global $DB;

        $query = "'?3'";

        // if validUntil is empty, then set value in DB as NULL (without single-quotes)
        if (!$validUntil) {
            $validUntil = "NULL";
            $query = "?3";
        }

        $txt = "
            INSERT INTO app_group (group_name, app_year, starting_from, valid_until, sort_order, created_at, created_by) 
            VALUES ('?0', ?1, '?2', " . $query . ", ?4, '?5', ?6)
        ";
        $sql = $DB->prepareSQL($txt, [
            $groupName,
            $yearId,
            $startingFrom,
            $validUntil,
            $sortOrder,
            Core_Date::now('Y-m-d H:i:s'),
            $createdBy]);

        $res = $DB->db_edit($sql);

        if ($res) {
            return $DB->get_last_insert_id();
        }

        return false;
    }

    public static function addUserToGroup(int $groupId, int $userId, int $roleId, int $createdBy): bool
    {
        global $DB;

        $txt = "
            INSERT INTO app_user_group (app_user, app_group, app_role, created_at, created_by) 
            VALUES (?0, ?1, ?2, '?3', ?4)
        ";
        $sql = $DB->prepareSQL($txt, [$userId, $groupId, $roleId, Core_Date::now('Y-m-d H:i:s'), $createdBy]);
        $res = $DB->db_edit($sql);

        // if role is homeroom-teacher, we have to set that role
        if ($res && $roleId = 4) {
            $isRoleAdded = UserModel::setUserRole($userId, $roleId, $createdBy);  // TODO: What if add group succeeds but add role fails..?

            if (!$isRoleAdded) {
                error_log('Failed to add homeroom-teacher role to user ' . $userId);
            }
        }
        return $res;
    }

    public static function removeUserFromGroup(int $userGroupId, int $userId, int $roleId, int $deletedBy): bool
    {
        global $DB;

        $txt = "
            UPDATE app_user_group 
            SET deleted_at = '?0', deleted_by = ?1
            WHERE id = ?2
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $deletedBy, $userGroupId]);
        $res = $DB->db_edit($sql);

        // if role is homeroom-teacher, we have to remove that role
        if ($res && $roleId = 4) {
            $isRoleRemoved = UserModel::removeUserRole($userId, $roleId, $deletedBy);

            if (!$isRoleRemoved) {
                error_log('Failed to remove homeroom-teacher role from user ' . $userId);
            }
        }
        return $res;
    }
}