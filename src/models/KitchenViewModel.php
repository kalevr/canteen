<?php
require __DIR__ . '/../../preventDirectAccess.php';

class KitchenViewModel extends BaseModel
{
    public static function getAllKitchenViews(int $yearId): array
    {
        global $DB;

        $txt = "
            SELECT kv.id, kv.kitchen_view_name, kv.meal_type, mt.meal_type_name, kv.starting_from, kv.valid_until, kv.sort_order
            FROM kitchen_view kv
            JOIN meal_type mt on kv.meal_type = mt.id
            WHERE kv.app_year = ?1 AND kv.created_at <= '?0' AND (kv.deleted_at IS NULL OR kv.deleted_at >= '?0')
            ORDER BY mt.meal_time, kv.sort_order
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $yearId]);

        return $DB->query($sql);
    }

    public static function getKitchenViewById(int $id) : array {
        global $DB;

        $txt = "
            SELECT id, kitchen_view_name, meal_type, starting_from, valid_until, sort_order
            FROM kitchen_view
            WHERE id = ?0 AND created_at <= '?1' AND (deleted_at IS NULL OR deleted_at >= '?1')
        ";
        $sql = $DB->prepareSQL($txt, [$id, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function getIdsOfGroupsInKitchenView(int $kitchenViewId): array
    {
        global $DB;

        $txt = "
            SELECT kv.id, gkv.app_group, g.group_name, gkv.id AS group_kv_id
            FROM kitchen_view kv
            LEFT JOIN group_kitchen_view gkv ON gkv.kitchen_view = kv.id
            LEFT JOIN app_group g ON g.id = gkv.app_group
            WHERE kv.id = ?0 AND kv.created_at <= '?1' AND (kv.deleted_at IS NULL OR kv.deleted_at >= '?1')
                AND gkv.created_at <= '?1' AND (gkv.deleted_at IS NULL OR gkv.deleted_at >= '?1')
            ORDER BY g.sort_order
        ";
        $sql = $DB->prepareSQL($txt, [$kitchenViewId, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function updateKitchenView(array $ins, int $id) {
        global $DB;

        return $DB->update('kitchen_view', $ins, $id);
    }

    public static function insertKitchenView(array $ins) {
        global $DB;

        return $DB->insert('kitchen_view', $ins);
    }

    /**
     * Add group to kitchen view (to group_kitchen_view DB table).
     * @param int $groupId - app_group ID
     * @param int $kitchenViewId - kitchen_view ID
     * @param string $createdBy - name of the user who adds the group to kitchen view
     * @return bool
     */
    public static function addGroupKitchenView(int $groupId, int $kitchenViewId, int $createdBy): bool {
        global $DB;

        $txt = "
            INSERT INTO group_kitchen_view (kitchen_view, app_group, starting_from, created_at, created_by) 
            VALUES (?0, ?1, '?2', '?3', ?4)
        ";
        $sql = $DB->prepareSQL($txt, [
            $kitchenViewId,
            $groupId,
            Core_Date::now(),
            Core_Date::now('Y-m-d H:i:s'),
            $createdBy
        ]);
        return $DB->db_edit($sql);
    }

    public static function removeGroupKitchenView(int $groupKitchenViewId, int $deletedBy): bool {
        global $DB;

        $txt = "
            UPDATE group_kitchen_view
            SET deleted_at = '?0', deleted_by = ?1
            WHERE id = ?2
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $deletedBy, $groupKitchenViewId]);
        return $DB->db_edit($sql);
    }

    /**
     * Creates KitchenView object from input. Input (array) must contain at least minimal amount of parameters to
     * create KitchenView object.
     * @param array $data - Key => Value pairs to build KitchenViews object with
     * @return KitchenView
     * @throws Exception - throws exception when mandatory info is not present in the array (or dates are invalid)!
     */
    public static function createKitchenViewObj(array $data, array $groupsInKv = []): KitchenView
    {
        if (!array_key_exists(KitchenView::NAME, $data)) {
            error_log('ERROR: createKitchenViewObj - data: ' . print_r($data, true) . 'key: ' . MealType::NAME);
            throw new Exception("Can't create object from this array!");
        }

        $kitchenView = new KitchenView($data[KitchenView::NAME], $data[KitchenView::MEAL_TYPE_ID]);

        if (array_key_exists(KitchenView::ID, $data)) {
            $kitchenView->setId($data[KitchenView::ID]);
        }

        if (array_key_exists(KitchenView::APP_YEAR, $data)) {
            $kitchenView->setAppYear($data[KitchenView::APP_YEAR]);
        }

        if (array_key_exists(KitchenView::MEAL_TYPE_ID, $data)) {
            $kitchenView->setMealTypeId($data[KitchenView::MEAL_TYPE_ID]);
        }

        if (array_key_exists(KitchenView::MEAL_TYPE_NAME, $data)) {
            $kitchenView->setMealTypeName($data[KitchenView::MEAL_TYPE_NAME]);
        }

        if (array_key_exists(KitchenView::STARTING_FROM, $data)) {
            $kitchenView->setStartingFrom($data[KitchenView::STARTING_FROM]);
        }

        if (array_key_exists(KitchenView::VALID_UNTIL, $data)) {
            $kitchenView->setValidUntil($data[KitchenView::VALID_UNTIL]);
        }

        if (array_key_exists(KitchenView::SORT_ORDER, $data)) {
            $kitchenView->setSortOrder($data[KitchenView::SORT_ORDER]);
        }

        if (array_key_exists(KitchenView::NAME_ERROR, $data)) {
            $kitchenView->setNameError($data[KitchenView::NAME_ERROR]);
        }

        if (array_key_exists(KitchenView::MEAL_TYPE_ID_ERROR, $data)) {
            $kitchenView->setMealTypeError($data[KitchenView::MEAL_TYPE_ID_ERROR]);
        }

        if (array_key_exists(KitchenView::STARTING_FROM_ERROR, $data)) {
            $kitchenView->setStartingFromError($data[KitchenView::STARTING_FROM_ERROR]);
        }

        if (array_key_exists(KitchenView::VALID_UNTIL_ERROR, $data)) {
            $kitchenView->setValidUntilError($data[KitchenView::VALID_UNTIL_ERROR]);
        }

        if ($groupsInKv) {
            $groups = [];
            foreach ($groupsInKv as $group) {
                $groups[] = ['id' => $group['app_group'], 'group_name' => $group['group_name']];
            }
            $kitchenView->setGroups($groups);
        }

        return $kitchenView;
}
}