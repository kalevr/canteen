<?php
require __DIR__ . '/../../preventDirectAccess.php';

class KitchenMealModel extends BaseModel
{
    public static function getFirstMealType(): array
    {
        return self::getMealTypeById();
    }

    public static function getMealTypeById(int $id = null): array {
        global $DB;

        $query = "";

        if ($id) {
            $query = "mt.id = " . $DB->escape($id) . " AND";
        }

        $txt = "
            SELECT mt.id, mt.meal_type_name, mt.meal_time, mt.registration_cutoff, mt.business_days 
            FROM meal_type mt
            LEFT JOIN kitchen_view kv on mt.id = kv.meal_type
            WHERE {$query} mt.created_at <= '?1' AND (mt.deleted_at IS NULL OR mt.deleted_at >= '?1')
            ORDER BY mt.meal_time
            LIMIT 1       
        ";
        $sql = $DB->prepareSQL($txt, [$id, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function getKitchenMealTotalsByMealTypeId(int $mealTypeId, string $from, string $until, int $yearId) {
        global $DB;

        $txt = "
            SELECT kv.id, kv.kitchen_view_name, r.meal_date, (
                SELECT COUNT(*)
                FROM registration reg
                JOIN app_user_group ug2 ON ug2.app_user = reg.app_user AND ug2.app_role = 5 
                    AND ug2.created_at <= '?3' AND (ug2.deleted_at IS NULL OR ug2.deleted_at >= '?3')
                JOIN kitchen_view kv2 ON kv2.meal_type = reg.meal_type AND kv2.id = gkv.kitchen_view
                JOIN group_kitchen_view gkv2 ON gkv2.kitchen_view = kv2.id AND gkv2.app_group = ug2.app_group 
                    AND (gkv2.deleted_at IS NULL OR gkv2.deleted_at >= '?3')
                WHERE reg.meal_date = r.meal_date AND reg.created_at <= '?3' AND (reg.deleted_at IS NULL OR reg.deleted_at >= '?3') 
            ) AS total
            FROM group_kitchen_view gkv
            JOIN kitchen_view kv ON kv.id = gkv.kitchen_view
            LEFT JOIN app_user_group ug ON ug.app_group = gkv.app_group AND ug.app_role = 5
            LEFT JOIN registration r ON r.meal_type = kv.meal_type AND r.meal_date BETWEEN '?1' AND '?2'
            WHERE kv.meal_type = ?0 AND kv.app_year = ?4
            GROUP BY kv.sort_order, r.meal_date
        ";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, $from, $until, Core_Date::now('Y-m-d H:i:s'), $yearId]);
        return $DB->query($sql);
    }
}