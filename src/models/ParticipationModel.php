<?php
require __DIR__ . '/../../preventDirectAccess.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'AcademicYear.php';

class ParticipationModel extends BaseModel
{
    /**
     * Get participation by id.
     * @param int $id
     */
    public static function getParticipationById(int $id): array
    {
        global $DB;

        $txt = "
            SELECT *
            FROM participation
            where id = ?0
        ";
        $sql = $DB->prepareSQL($txt, [$id]);
        return $DB->query($sql);
    }

    public static function getCurrentParticipations(int $mealTypeId, int $yearId): array
    {
        global $DB;

        $date = Core_Date::now();

        $txt = "
            SELECT mt.id AS meal_type_id, u.id as app_user, u.first_name, u.last_name, g.id AS group_id, g.group_name, 
	            IF(reg.app_user IS NULL OR reg.deleted_at IS NOT NULL, 0, 1) AS registration, 
	            IF(p.app_user IS NULL OR p.deleted_at IS NOT NULL, 0, 1) AS participation
            FROM meal_type mt
            JOIN group_meal_type gmt ON gmt.meal_type = mt.id AND (gmt.deleted_at IS NULL OR gmt.deleted_at >= '?2')
            JOIN app_user_group ug ON ug.app_group = gmt.app_group AND ug.app_role = 5 AND (ug.deleted_at IS NULL OR ug.deleted_at >= '?2')
            JOIN app_group g ON ug.app_group = g.id AND (g.deleted_at IS NULL OR g.deleted_at >= '?2')
            JOIN app_user u ON ug.app_user = u.id AND (u.deleted_at IS NULL OR u.deleted_at >= '?2')
            LEFT JOIN registration reg ON reg.app_user = u.id AND reg.meal_date = '?1' AND reg.meal_type = mt.id 
                AND reg.deleted_at IS NULL OR reg.deleted_at >= '?2'
            LEFT JOIN participation p ON p.app_user = u.id AND p.meal_date = '?1' AND p.meal_type = mt.id
                AND p.deleted_at IS NULL OR p.deleted_at >= '?2'
            WHERE mt.id = ?0 AND g.app_year = ?3
            ORDER BY g.sort_order, u.first_name, u.last_name
        ";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, $date, Core_Date::now('Y-m-d H:i:s'), $yearId]);
        return $DB->query($sql);
    }

    public static function getUserIdByCardCode(string $sCode) {
        global $DB;

        $txt = "
            SELECT u.id
            FROM keycard kc
            JOIN app_user u ON kc.app_user = u.id AND (u.deleted_at IS NULL || u.deleted_at >= '?1')
            WHERE kc.scannable_nr = '?0' 
            AND (kc.starting_from IS NULL || kc.starting_from <= '?2') AND (kc.valid_until IS NULL || kc.valid_until >= '?2')
            AND (kc.deleted_at IS NULL || kc.deleted_at >= '?1')
        ";
        $sql = $DB->prepareSQL($txt, [$sCode, Core_Date::now('Y-m-d H:i:s'), Core_Date::now()]);
        return $DB->query($sql);
    }

    public static function getCurrentParticipationByUserIdAndMealTypeId(int $userId, int $mealTypeId) {
        global $DB;

        $now = Core_Date::now();
        // now = '2022-04-15'; // for debugging
        $txt = "
            SELECT u.id as app_user, u.first_name, u.last_name, 
                   IF(reg.app_user IS NULL OR reg.deleted_at IS NOT NULL, 0, 1) AS registration, 
                   IF(p.app_user IS NULL OR p.deleted_at IS NOT NULL, 0, 1) AS participation
            FROM app_user u
            LEFT JOIN registration reg ON reg.app_user = u.id AND reg.meal_type = ?0 AND reg.meal_date = '?2'
                AND reg.deleted_at IS NULL OR reg.deleted_at >= '?3'
            LEFT JOIN participation p ON p.app_user = u.id AND p.meal_type = ?0 AND p.meal_date = '?2'
                AND p.deleted_at IS NULL OR p.deleted_at >= '?3'
            LEFT JOIN meal_type mt ON reg.meal_type = mt.id
            WHERE u.id = ?1  AND reg.deleted_at IS NULL
        ";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, $userId, $now, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    /**
     * Add user participation (of mealType) on a specific date.
     * @param int $mealTypeId - meal_type ID
     * @param int $userId - app_user ID (person who is registered for meal)
     * @param string $mealDate - meal date (Y-m-d)
     * @param int $createdBy - name of the user who adds meal record
     * @return int
     */
    public static function addParticipation(int $mealTypeId, int $userId, string $mealDate, int $createdBy): int
    {
        global $DB;

        if (!$mealTypeId || !$userId || !$mealDate) {
            return false;
        }

        $txt = "
            INSERT INTO participation (meal_type, app_user, meal_date, created_at, created_by) 
            VALUES (?0, ?1, '?2', '?3', ?4);
        ";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, $userId, $mealDate, Core_Date::now('Y-m-d H:i:s'), $createdBy]);
        $res = $DB->db_edit($sql);

        return $res ? $DB->get_last_insert_id() : 0;
    }

    /**
     * Mark user's participation as deleted (does not delete the record from DB)
     * @param int $mealTypeId - meal_type ID
     * @param int $userId - app_user ID
     * @param string $mealDate - meal date (Y-m-d)
     * @param string $deletedBy - name of the user who marked meal record as deleted
     * @return bool
     */
    public static function removeParticipation(int $mealTypeId, int $userId, string $mealDate, int $deletedBy): bool
    {
        global $DB;

        if (!$userId || !$mealTypeId || !$mealDate) {
            return false;
        }

        $txt = "
            UPDATE participation
            SET deleted_at = '?0', deleted_by = ?1
            WHERE meal_type = ?2 AND app_user = ?3 AND meal_date = '?4' AND deleted_at IS NULL
        ";

        $sql = $DB->prepareSQL($txt, [
            Core_Date::now('Y-m-d H:i:s'),
            $deletedBy,
            $mealTypeId,
            $userId,
            $mealDate
        ]);

        return $DB->db_edit($sql);
    }

    public static function getAllMealTypes() {
        global $DB;

        $txt = "
            SELECT mt.id, mt.meal_type_name, mt.meal_time 
            FROM meal_type mt
            WHERE mt.deleted_at IS NULL OR mt.deleted_at >= '?0'
            ORDER BY mt.meal_time
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    /**
     * Returns all participations (for meal type) for all users in a group in a given timeframe
     * @param int $mealTypeId meal_type ID
     * @param int $groupId app_group ID
     * @param string $from starting date
     * @param string $until end date
     * @return array
     */
    public static function getParticipationsForTypeForGroupBetweenDates(int $mealTypeId, int $groupId, string $from, string $until): array
    {
        global $DB;

        $txt = "SELECT u.id, u.first_name, u.last_name, p.meal_date 
            FROM app_user u
            JOIN app_user_group ug ON ug.app_user = u.id
            LEFT JOIN  participation p ON u.id = p.app_user AND p.meal_type = ?1 AND p.meal_date BETWEEN '?2' AND '?3'
                AND p.created_at <= '?0' AND (p.deleted_at IS NULL OR p.deleted_at > '?0')
            WHERE ug.app_group = ?4 AND ug.app_role = 5 AND (u.deleted_at IS NULL OR u.deleted_at > '?2')
            ORDER BY CONCAT(u.first_name, ' ', u.last_name), p.meal_date
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $mealTypeId, $from, $until, $groupId]);
        return $DB->query($sql);
    }

    public static function getParticipationHistoryForUser(int $mealTypeId, int $userId, string $from, string $until): array {
        global $DB;

        $txt = "
            SELECT CONCAT(u.first_name, ' ' , u.last_name) AS full_name , p.meal_date, 
                   p.created_at, CONCAT(u2.first_name, ' ', u2.last_name) as created_by,
                    p.deleted_at, CONCAT(u3.first_name, ' ', u3.last_name) as deleted_by 
            FROM participation p
            JOIN app_user u ON p.app_user = u.id AND (u.deleted_at IS NULL OR u.deleted_at >= '?2')
            LEFT JOIN app_user u2 ON p.created_by = u2.id
            LEFT JOIN app_user u3 ON p.deleted_by = u3.id
            WHERE p.meal_type = ?0 AND p.app_user = ?1 AND p.meal_date BETWEEN '?2' AND '?3'
            ORDER BY p.meal_date DESC, p.created_at, p.deleted_at
            LIMIT 100
        ";

        $sql = $DB->prepareSQL($txt, [$mealTypeId, $userId, $from, $until]);

        return $DB->query($sql);
    }

}