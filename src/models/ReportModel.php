<?php
require __DIR__ . '/../../preventDirectAccess.php';

class ReportModel extends BaseModel
{
    /**
     * Get random mealTypeId (sorted by meal_time).
     * @return array
     */
    public static function getRandomMealTypeId(): array
    {
        global $DB;

        $txt = "
            SELECT * 
            FROM meal_type mt
            WHERE mt.deleted_at IS NULL OR mt.deleted_at >= '?0'
            ORDER BY mt.meal_time
            LIMIT 1
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function getRegistrationAndParticipationSummaryForMealType(int $mealTypeId, string $from, string $until, int $yearId, bool $currentMonth = false)
    {
        global $DB;

        $filter = "";
        if ($currentMonth) {
            $filter = ",
                 (SELECT COUNT(reg.meal_date) 
                 FROM registration reg 
                 WHERE reg.app_user = u.id AND reg.meal_type = ?0 
                   AND reg.meal_date <= '?3' AND reg.meal_date >= '?1' AND reg.deleted_at IS NULL
                  ) AS reg_in_past";
        }

        $txt = "
            SELECT g.id, g.group_name, u.first_name, u.last_name, 
                (SELECT COUNT(reg.meal_date) 
                 FROM registration reg 
                 WHERE reg.app_user = u.id AND reg.meal_type = ?0 
                   AND reg.meal_date <= '?2' AND reg.meal_date >= '?1' AND reg.deleted_at IS NULL
                 ) AS registrations, 
                (SELECT COUNT(part.meal_date) 
                 FROM participation part 
                 WHERE part.app_user = u.id AND part.meal_type = ?0 
                   AND part.meal_date <= '?2' AND part.meal_date >= '?1' AND part.deleted_at IS NULL
                 ) AS participations {$filter}
            FROM app_user_group ug
            JOIN app_user u ON ug.app_user = u.id AND (u.created_at <= '?2' OR (u.deleted_at IS NULL OR u.deleted_at >= '?1'))
            JOIN app_group g ON ug.app_group = g.id
            JOIN group_meal_type gmt ON gmt.meal_type = ?0 AND gmt.app_group = g.id
            WHERE ug.app_role = 5 AND (ug.deleted_at IS NULL OR ug.deleted_at >= '?2') AND g.app_year = ?4
            GROUP BY g.group_name, ug.app_user 
            ORDER BY g.sort_order, u.first_name
        ";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, $from, $until, Core_Date::now(), $yearId]);
        return $DB->query($sql);
    }

    public static function getMealTotalsByKitchenViews(int $mealTypeId, string $from, string $until, int $yearId) {
        global $DB;

        $txt = "
            SELECT kv.kitchen_view_name,
                SUM((
                    SELECT COUNT(*) 
                    FROM registration reg
                    JOIN app_user_group ug ON reg.app_user = ug.app_user 
                    WHERE reg.meal_date >= '?0' AND reg.meal_date <= '?1' AND reg.meal_type = ?2 
                      AND (reg.deleted_at IS NULL OR reg.deleted_at >= '?4') AND ug.app_group = gkv.app_group
                    )) AS total
            FROM group_kitchen_view gkv
            JOIN kitchen_view kv ON gkv.kitchen_view = kv.id AND kv.app_year = ?3 AND kv.meal_type = ?2
            WHERE (gkv.deleted_at IS NULL OR gkv.deleted_at >= '?4') AND (kv.deleted_at IS NULL OR kv.deleted_at >= '?4')
            GROUP BY kv.sort_order
            ORDER BY kv.sort_order
        ";
        $sql = $DB->prepareSQL($txt, [$from, $until, $mealTypeId, $yearId, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }
}