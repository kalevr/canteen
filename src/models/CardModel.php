<?php

class CardModel extends BaseModel
{
    const RESULTS_PER_PAGE = 50;

    public static function getCardsWithPagination(int $pageNr, int $resultsPerPage, string $search = "")
    {
        global $DB;

        $offset = ($pageNr - 1) * $resultsPerPage;
        $query = "";

        if ($search) {
            $query = "
            AND (u.first_name LIKE '%?1%' 
            OR u.last_name LIKE '%?1%' 
            OR kc.visible_nr LIKE '%?1%' 
            OR kc.scannable_nr LIKE '%?1%')";
        }

        $txt = "
            SELECT kc.id, kc.visible_nr, kc.scannable_nr, kc.starting_from, kc.valid_until ,u.first_name, u.last_name, 
                   if(u.deleted_at IS NOT NULL, 1, 0) AS is_user_deleted, 
                   if(kc.valid_until IS NOT NULL AND kc.valid_until >= '?0', 1, 0) AS is_card_expired
            FROM keycard kc
            JOIN app_user u ON kc.app_user = u.id
            WHERE (kc.deleted_at IS NULL OR kc.deleted_at >= '?0') {$query}
            LIMIT ?2, ?3
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $search, $offset, $resultsPerPage]);
        return $DB->query($sql);
    }

    public static function getNrOfCards(string $search = "")
    {
        global $DB;

        $query = "";

        if ($search) {
            $query = "
            AND (u.first_name LIKE '%?1%' 
            OR u.last_name LIKE '%?1%' 
            OR kc.visible_nr LIKE '%?1%' 
            OR kc.scannable_nr LIKE '%?1%')";
        }

        $txt = "
            SELECT COUNT(kc.id) as total
            FROM keycard kc
            JOIN app_user u ON kc.app_user = u.id
            WHERE (kc.deleted_at IS NULL OR kc.deleted_at >= '?0') {$query}
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $search]);
        $res = $DB->query($sql);
        $total = 0;

        if ($res && $res[0]) {
            $total = $res[0]['total'];
        }

        return $total;
    }

    public static function getCardById(int $id)
    {
        global $DB;

        $txt = "
            SELECT kc.id, kc.visible_nr, kc.scannable_nr, kc.app_user, kc.starting_from, kc.valid_until ,u.first_name, u.last_name, 
                if(u.deleted_at IS NOT NULL, 1, 0) AS is_user_deleted, 
                if(kc.valid_until IS NOT NULL AND kc.valid_until >= '?1', 1, 0) AS is_card_expired 
            FROM keycard kc
            JOIN app_user u ON kc.app_user = u.id
            WHERE kc.id = ?0 AND (kc.deleted_at IS NULL OR kc.deleted_at >= '?1')
        ";
        $sql = $DB->prepareSQL($txt, [$id, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function getCardByScannableNr(string $scannableNr)
    {
        global $DB;

        if (!$scannableNr) {
            return [];
        }

        $txt = "
            SELECT kc.id, kc.visible_nr, kc.scannable_nr, kc.app_user, kc.starting_from, kc.valid_until ,u.first_name, u.last_name, 
                if(u.deleted_at IS NOT NULL, 1, 0) AS is_user_deleted, 
                if(kc.valid_until IS NOT NULL AND kc.valid_until >= '?1', 1, 0) AS is_card_expired 
            FROM keycard kc
            JOIN app_user u ON kc.app_user = u.id
            WHERE kc.scannable_nr = '?0' AND (kc.deleted_at IS NULL OR kc.deleted_at >= '?1')
        ";
        $sql = $DB->prepareSQL($txt, [$scannableNr, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function updateCard(int $cardId, string $visibleNr, int $userId, string $scannableNr, string $from, string $until): bool
    {
        global $DB;

        $setVisibleNr = "";
        $setValidUntil = "";

        if ($visibleNr) {
            $setVisibleNr = "visible_nr = '?4',";
        }
        if ($until) {
            $setValidUntil = ", valid_until = '?5'";
        }

        $txt = "
            UPDATE keycard
            SET {$setVisibleNr} scannable_nr = '?0', app_user = ?1, starting_from = '?2' {$setValidUntil}
            WHERE id = ?3
        ";
        $sql = $DB->prepareSQL($txt, [$scannableNr, $userId, $from, $cardId, $visibleNr, $until]);
        return $DB->db_edit($sql);
    }

    public static function insertCard(string $visibleNr, string $scannedNr, int $userId, string $startingFrom,
                                      string $validUntil, int $createdBy)
    {
        global $DB;

        $visibleQuery = "'?0'";
        $visibleValidUntil = "'?4'";
        // if visibleNr is empty, then set value in DB as NULL (without single-quotes)
        if (!$visibleNr) {
            $visibleNr = "NULL";
            $visibleQuery = "?0";
        }

        // if validUntil is empty, then set value in DB as NULL (without single-quotes)
        if (!$validUntil) {
            $validUntil = "NULL";
            $visibleValidUntil = "?4";
        }

        $txt = "
            INSERT INTO keycard (visible_nr, scannable_nr, app_user, starting_from, valid_until, created_at, created_by) 
            VALUES ({$visibleQuery}, '?1', ?2, '?3', {$visibleValidUntil}, '?5', ?6)
        ";
        $sql = $DB->prepareSQL($txt, [
            $visibleNr,
            $scannedNr,
            $userId,
            $startingFrom,
            $validUntil,
            Core_Date::now('Y-m-d H:i:s'),
            $createdBy
        ]);

        $res = $DB->db_edit($sql);

        if ($res) {
            return $DB->get_last_insert_id();
        }

        return false;
    }

    public static function markCardAsDeleted(int $id, int $deletedBy): bool
    {
        global $DB;

        if (!$id) {
            return false;
        }

        $txt = "
            UPDATE keycard kc
            SET kc.deleted_at = '?1', kc.deleted_by = ?2
            WHERE kc.id = ?0 AND kc.deleted_at IS NULL
        ";

        $sql = $DB->prepareSQL($txt, [$id, Core_Date::now('Y-m-d H:i:s'), $deletedBy]);
        return $DB->db_edit($sql);
    }
}