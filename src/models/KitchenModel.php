<?php
require __DIR__ . '/../../preventDirectAccess.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Date.php';

class KitchenModel
{
    public static function getAllKitchens(): array
    {
        global $DB;

        $txt = "
            SELECT * 
            FROM kitchen 
            WHERE created_at <= '?0' AND (deleted_at IS NULL OR deleted_at > '?0')
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s')]);

        return $DB->query($sql);
    }

    public static function getKitchenById(int $id): array
    {
        global $DB;

        $txt = "
            SELECT * 
            FROM kitchen
            WHERE id = ?0 
              AND created_at <= '?1' AND (deleted_at IS NULL OR deleted_at >= '?1')
        ";
        $sql = $DB->prepareSQL($txt, [$id, Core_Date::now('Y-m-d H:i:s')]);
        return $DB->query($sql);
    }

    public static function updateKitchen(int $kitchenId, string $kitchenName, array $daysOff): bool
    {
        global $DB;

        $setDaysOff = "NULL";
        $daysOffStr = "";

        if ($daysOff) {
            $setDaysOff = "'?2'";
            $daysOffStr = implode(',', $daysOff);
        }

        $txt = "
            UPDATE kitchen
            SET kitchen_name = '?0', days_off = {$setDaysOff}
            WHERE id = ?1
        ";
        $sql = $DB->prepareSQL($txt, [$kitchenName, $kitchenId, $daysOffStr]);
        return $DB->db_edit($sql);
    }
}