<?php
require __DIR__ . '/../../preventDirectAccess.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Date.php';

class BaseModel
{
    /**
     * Checks how many of the roles does the user have
     * @param int $id - user ID
     * @param array $roles - roles to check
     * @return int
     */
    public static function hasNrOfRoles(int $id, array $roles): int
    {
        global $DB;

        //$rolesStr = implode("', '", $roles);

        $txt = "SELECT COUNT(*) as count
                FROM app_user_role ur
                JOIN app_role r ON ur.app_role = r.id
                WHERE ur.app_user = ?0 AND r.role_code IN(?1)
                AND ur.created_at <= '?2' AND (ur.deleted_at IS NULL OR ur.deleted_at >= '?2');
        ";

        $sql = $DB->prepareSQL($txt, [$id, $roles, Core_Date::now('Y-m-d H:i:s')]);
        $res = $DB->query($sql);

        if ($res && $res[0]) {
            return $res[0]['count'];
        }

        return 0;
    }
}