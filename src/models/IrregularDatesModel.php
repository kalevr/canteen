<?php
require __DIR__ . '/../../preventDirectAccess.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Database.php';

class IrregularDatesModel
{
    public static function getAllIrregularDates(int $mealTypeId, string $start = '', string $end = ''): array
    {
        global $DB;

        $where = ""; // if $mealTypeId = 0, then we are ignoring meal_type

        if ($mealTypeId > 0) {
            $where = "meal_type = ?0 ";
        }

        $txt = "SELECT id, date(irr_date) as date, is_included
                FROM irregular_dates 
                WHERE {$where} AND irr_date BETWEEN '?1' AND '?2' AND created_at <= '?3' AND (deleted_at IS NULL OR deleted_at > '?3')
                ORDER BY date";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, $start, $end, Core_Date::now('Y-m-d H:i:s')]);
        $res = $DB->query($sql);

        $data = ['enabled' => [], 'disabled' => []];

        foreach ($res as $date) {
            $day = Core_Date::get($date['date'], 'j'); // j - day without leading zeroes

            if ($date['is_included']) {
                $data['enabled'][$day] = true;
            } else {
                $data['disabled'][$day] = false;
            }
        }

        return $data;
    }

    public static function addIrregularDate(int $mealTypeId, string $date, bool $value, int $createdBy) {
        global $DB;

        $txt = "
            INSERT INTO irregular_dates
            (meal_type, irr_date, is_included, created_by)
            VALUES (?0, '?1', ?2, ?3); 
        ";

        $sql = $DB->prepareSQL($txt, [$mealTypeId, $date, $value ? 1 : 0, $createdBy]);
        $DB->db_edit($sql);
    }

    public static function deleteIrregularDate(string $date, int $deletedBy) {
        global $DB;

        $txt = "UPDATE irregular_dates 
                SET deleted_at = '?0', deleted_by = ?1
                WHERE irr_date = '?2' AND deleted_at IS NULL";

        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $deletedBy, $date]);
        $DB->db_edit($sql);
    }

    public static function deleteAllMealsOnDates(array $dates, int $deletedBy): bool
    {
        global $DB;

        $txt = "
            UPDATE registration 
            SET deleted_at = '?0', deleted_by = ?1 
            WHERE meal_date IN (?2) AND deleted_at IS NULL        
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $deletedBy, $dates]);
        return $DB->db_edit($sql);
    }

    public static function daysInMonthUpdatedByDb(int $mealTypeId, array $businessDays, string $date, array $data = [], array $totalMeals = []): array {
        global $config;

        if (!$data) {
            $data = self::getAllIrregularDates($mealTypeId, Core_Date::firstDayOfMonth($date), Core_Date::lastDayOfMonth($date));
        }

        $daysInMonth = Core_Date::getAllDaysInMonth($date);
        $enabled = $data['enabled'];
        $disabled = $data['disabled'];
        $now = Core_Date::now();
        $meals = [];

        if ($totalMeals) {
            foreach ($totalMeals as $totalMeal) {
                $meals[Core_Date::get($totalMeal['meal_date'], 'd')] = $totalMeal['total'];
            }
        }

        $dates = [];
        foreach ($daysInMonth as $day) {
            $dayDate = Core_Date::get(Core_Date::get($date, 'Y-m-') . $day, 'Y-m-d');
            $weekdayNr = Core_Date::dayOfWeek($dayDate);

            if (array_key_exists($day, $enabled) || (in_array($weekdayNr, $businessDays) && !array_key_exists($day, $disabled))) {
                $dates[$day]['is_enabled'] = true;
            } elseif (array_key_exists($day, $disabled) || !in_array($weekdayNr, $businessDays)) {
                $dates[$day]['is_enabled'] = false;
            }

            if ($totalMeals && array_key_exists($day, $meals)) {
                $dates[$day]['total'] = $meals[$day];
            }

            $dates[$day]['in_future'] = $dayDate > $now;
        }

        return $dates;
    }

    public static function getTotalMealsInMonthByDate(int $mealTypeId, string $start, string $end): array
    {
        global $DB;

        $where = '';

        if ($mealTypeId > 0) {
            $where = "r.meal_type = ?2 AND";
        }

        $txt = "
            SELECT r.meal_date, COUNT(*) as total
            FROM registration r
            WHERE {$where} r.deleted_at IS NULL AND r.meal_date BETWEEN '?0' AND '?1'
            GROUP BY r.meal_date
            ORDER BY r.meal_date
        ";
        $sql = $DB->prepareSQL($txt, [$start, $end, $mealTypeId]);
        return $DB->query($sql);
    }
}