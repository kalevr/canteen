<?php
require __DIR__ . '/../../preventDirectAccess.php';

class MealModel extends BaseModel
{
    // FIXME: find a better place for this constant?
    public const APP_ROLE_HOMEROOM_TEACHER = 4;

    /**
     * Checks if the user has homeroom teacher role in given group.
     * @param int $userId - app_user ID
     * @param int $groupId - app_group ID
     * @return bool
     */
    public static function isHomeroomTeacher(int $userId, int $groupId): bool
    {
        global $DB;

        $txt = "
            SELECT count(*) as results
            FROM app_user_group
            WHERE app_user = ?0 AND app_group = ?1 AND app_role = ?2 
            AND created_at <= '?3' AND (deleted_at IS NULL OR deleted_at >= '?3')
        ";
        $sql = $DB->prepareSQL($txt, [$userId, $groupId, self::APP_ROLE_HOMEROOM_TEACHER, Core_Date::now('Y-m-d H:i:s')]);
        $res = $DB->query($sql);

        if ($res && $res[0]['results'] > 0) {
            return true;
        }

        return false;
    }

    public static function getAllMealGroups(int $mealTypeId, string $from, string $until, int $userId = null): array
    {
        global $DB;

        $query = $userId ? "JOIN app_user_group ug ON g.id = ug.app_group AND ug.app_role = 4 AND ug.app_user = ?4 AND ug.deleted_by IS NULL" : "";

        $txt = "
            SELECT g.id, g.group_name 
            FROM group_meal_type gmt
            JOIN app_group g ON gmt.app_group = g.id AND g.starting_from <= '?2' 
                AND (g.valid_until IS NULL OR g.valid_until >= '?1')    
            $query
            WHERE gmt.meal_type = ?0 AND gmt.starting_from <= '?2' 
                AND (gmt.valid_until IS NULL OR gmt.valid_until >= '?1')
                AND gmt.created_at <= '?3' AND (gmt.deleted_at IS NULL OR gmt.deleted_at >= '?3')   
            ORDER BY g.sort_order
        ";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, $from, $until, Core_Date::now('Y-m-d H:i:s'), $userId]);
        return $DB->query($sql);
    }

    /**
     * @param int $mealTypeId - meal_type ID
     * @param int $groupId - app_group ID
     * @param string $date - the date the group must have given mealtype
     * @return array
     */
    public static function groupMealType(int $mealTypeId, int $groupId, string $date): array
    {
        global $DB;

        $txt = "
            SELECT mt.meal_type_name, mt.meal_time, mt.registration_cutoff, g.group_name, mt.business_days, mt.comments
            FROM group_meal_type gmt
            JOIN meal_type mt ON gmt.meal_type = mt.id AND mt.created_at <= '?0' AND (mt.deleted_at IS NULL OR mt.deleted_at >= '?0')
            JOIN app_group g on gmt.app_group = g.id AND g.created_at <= '?0' AND (g.deleted_at IS NULL OR g.deleted_at >= '?0')            
            WHERE gmt.app_group = ?1 AND gmt.meal_type = ?2
            AND gmt.starting_from <= '?3' AND (gmt.valid_until IS NULL || gmt.valid_until >= '?3')
            AND gmt.created_at <= '?0' AND (gmt.deleted_at IS NULL OR gmt.deleted_at >= '?0')
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $groupId, $mealTypeId, $date]);
        $res = $DB->query($sql);

        if ($res && $res[0]) {
            return $res[0];
        }
        return [];
    }

    /**
     * Returns random group and meal type (which user has permission to see)
     * @param int $userId
     * @return array
     */
    public static function findRandomGroupAndMealTypeForUser(int $userId, bool $isEntitled = false, bool $isDiner = false) : array
    {
        global $DB;

        $roleId = $isDiner ? "5" : "4";
        $query = $isEntitled ? "" : "ug.app_user = ?0 AND ug.app_role = " . $roleId . " AND";

        $txt = "
            SELECT ug.app_group, gmt.meal_type 
            FROM app_user_group ug
            JOIN group_meal_type gmt ON gmt.app_group = ug.app_group 
                AND gmt.starting_from <= '?1' AND (gmt.valid_until IS NULL OR gmt.valid_until >= '?1')
                AND gmt.created_at <= '?2' AND (gmt.deleted_at IS NULL OR gmt.deleted_at >= '?2')
            WHERE $query 
                ug.created_at <= '?2' AND (ug.deleted_at IS NULL OR ug.deleted_at >= '?2') 
            ORDER BY gmt.meal_type                           
            LIMIT 1
        ";
        $sql = $DB->prepareSQL($txt, [$userId, Core_Date::now('Y-m-d'), Core_Date::now('Y-m-d H:i:s')]);
        $res = $DB->query($sql);

        if ($res && $res[0]) {
            return $res[0];
        }
        return [];
    }



    /**
     * Returns all meals (for meal type) for all users in a group in a given timeframe
     * @param int $mealTypeId - meal_type ID
     * @param int $groupId - app_group ID
     * @param string $from - starting date
     * @param string $until - end date
     * @return array
     */
    public static function getMealsForTypeForGroupBetweenDates(int $mealTypeId, int $groupId, string $from, string $until): array
    {
        global $DB;

        $txt = "
            SELECT u.id, u.first_name, u.last_name, u.deleted_at, r.meal_date 
            FROM app_user u
            JOIN app_user_group ug ON ug.app_user = u.id
            LEFT JOIN  registration r ON u.id = r.app_user AND r.meal_type = ?1 AND r.meal_date BETWEEN '?2' AND '?3'
                AND r.created_at <= '?0' AND (r.deleted_at IS NULL OR r.deleted_at > '?0')
            WHERE ug.app_group = ?4 AND ug.app_role = 5 AND (u.deleted_at IS NULL OR u.deleted_at > '?2')
            ORDER BY CONCAT(u.first_name, ' ', u.last_name), r.meal_date
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $mealTypeId, $from, $until, $groupId]);
        return $DB->query($sql);
    }

    public static function getMealsForUserBetweenDates(int $userId, int $mealTypeId, string $from, string $until): array {
        global $DB;

        $txt = "
            SELECT u.id, u.first_name, u.last_name, r.meal_date 
            FROM app_user u
            JOIN app_user_group ug ON ug.app_user = u.id
            LEFT JOIN  registration r ON u.id = r.app_user AND r.meal_type = ?1 AND r.meal_date BETWEEN '?2' AND '?3'
                AND r.created_at <= '?0' AND (r.deleted_at IS NULL OR r.deleted_at > '?0')
            WHERE u.id = ?4 AND ug.app_role = 5
            ORDER BY CONCAT(u.first_name, ' ', u.last_name), r.meal_date
        ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $mealTypeId, $from, $until, $userId]);
        return $DB->query($sql);
    }

    /**
     * Register user for a meal (of certain type) on a specific date.
     * @param int $mealTypeId - meal_type ID
     * @param int $userId - app_user ID (person who is registered for meal)
     * @param string $mealDate - meal date (Y-m-d)
     * @param string $createdBy - name of the user who adds meal record
     * @return bool
     */
    public static function registerForMeal(int $mealTypeId, int $userId, string $mealDate, int $createdBy)
    {
        global $DB;

        if (!$mealTypeId || !$userId || !$mealDate) {
            return false;
        }

        $txt = "
            INSERT INTO registration (meal_type, app_user, meal_date, created_at, created_by) 
            VALUES (?0, ?1, '?2', '?3', ?4);
        ";

        $sql = $DB->prepareSQL($txt, [
            $mealTypeId,
            $userId,
            $mealDate,
            Core_Date::now('Y-m-d H:i:s'),
            $createdBy
        ]);

        return $DB->db_edit($sql);
    }

    /**
     * Mark user's registration as deleted (does not delete the record from DB).
     * @param int $mealTypeId meal_type ID
     * @param int $userId - app_user ID
     * @param string $mealDate - meal date (Y-m-d)
     * @param string $deletedBy - name of the user who marked meal record as deleted
     * @return bool
     */
    public static function unregisterFromMeal(int $mealTypeId, int $userId, string $mealDate, string $deletedBy)
    {
        global $DB;

        if (!$userId || !$mealDate || !$mealTypeId) {
            return false;
        }

        $txt = "
            UPDATE registration 
            SET deleted_at = '?0', deleted_by = ?1 
            WHERE meal_type = ?2 AND app_user = ?3 AND meal_date = '?4' AND deleted_at IS NULL
        ";

        $sql = $DB->prepareSQL($txt, [
            Core_Date::now('Y-m-d H:i:s'),
            $deletedBy,
            $mealTypeId,
            $userId,
            $mealDate
        ]);

        return $DB->db_edit($sql);
    }


    /**
     * @param array $dayNames - array of (short) day names
     * @return array - returns day names sorted from monday to saturday
     */
    public static function sortDayNamesShort(array $dayNames)
    {
        $names = [];

        if (in_array(Core_Date::DAYS_MONDAY_SHORT, $dayNames)) {
            $names[] = Core_Date::DAYS_MONDAY_SHORT;
        }

        if (in_array(Core_Date::DAYS_TUESDAY_SHORT, $dayNames)) {
            $names[] = Core_Date::DAYS_TUESDAY_SHORT;
        }

        if (in_array(Core_Date::DAYS_WENDSDAY_SHORT, $dayNames)) {
            $names[] = Core_Date::DAYS_WENDSDAY_SHORT;
        }

        if (in_array(Core_Date::DAYS_THURDSDAY_SHORT, $dayNames)) {
            $names[] = Core_Date::DAYS_THURDSDAY_SHORT;
        }

        if (in_array(Core_Date::DAYS_FRIDAY_SHORT, $dayNames)) {
            $names[] = Core_Date::DAYS_FRIDAY_SHORT;
        }

        if (in_array(Core_Date::DAYS_SATURDAY_SHORT, $dayNames)) {
            $names[] = Core_Date::DAYS_SATURDAY_SHORT;
        }

        if (in_array(Core_Date::DAYS_SUNDAY_SHORT, $dayNames)) {
            $names[] = Core_Date::DAYS_SUNDAY_SHORT;
        }

        return $names;
    }

    /**
     * Returns a first day nr, where changing in permitted.
     * @param string $yearMonth - year and month of the meals in question (in Y-m format)
     * @param string $mealTime - time, when the meal (in H:I format)
     * @param string $registrationCutoff - how many hours before meal time registration locks (full hours - integer as string)
     * @return string
     * @throws Exception
     */
    public static function allowChange(string $yearMonth, string $mealTime, string $registrationCutoff): string
    {
        if (empty($mealTime) || empty($registrationCutoff)) {
            throw new Exception('Meal time and registration cutoff must both be set!');
        }

        $yearMonthArr = explode('-', $yearMonth);
        $now = Core_Date::now('Y-m-d H:i:s');
        $nowDayOfWeek = (int) Core_Date::dayOfWeek($now);
        $then = Core_Date::add($now, 'PT' . $registrationCutoff . 'H');
        $thenDayOfWeek = (int) Core_Date::dayOfWeek($then->format('Y-m-d'));
        $kitchenClosed = self::getKitchenDaysOff();

        // if meal was in previous month (or earlier)
        if ($then->format('Y') > $yearMonthArr[0]
            || ($then->format('m') > $yearMonthArr[1] && $then->format('Y') == $yearMonthArr[0])) {

            return '100';
        }

        // if kitchen is closed today, add 1 day to $then
        if (in_array($nowDayOfWeek, $kitchenClosed)) {
            $then = Core_Date::add($then->format('Y-m-d H:i:s'), 'P1D');
            $thenDayOfWeek = (int) Core_Date::dayOfWeek($then->format('Y-m-d'));
        }

        // if kitchen is closed on $then, add 1 day and repeat
        while (in_array($thenDayOfWeek, $kitchenClosed)) {
            $then = Core_Date::add($then->format('Y-m-d H:i:s'), 'P1D');
            $thenDayOfWeek = (int) Core_Date::dayOfWeek($then->format('Y-m-d'));
        }

        // if meal is in next month (or later)
        if ($then->format('Y') < $yearMonthArr[0]
            || ($then->format('m') < $yearMonthArr[1] && $then->format('Y') == $yearMonthArr[0])) {
            return '0';
        }

        // if $then (time) is later than meal time, then it's too late to change (so we pick the next day)
        if ($then->format('H:i') > $mealTime) {
            $then = Core_Date::add($then->format('Y-m-d H:i:s'), 'P1D');
        }

        $thenMonth = $then->format('m');
        // if $then went to next month because of previous step
        if ($then->format('m') > $thenMonth) {
            return '100';
        }

        return $then->format('d');
    }

    public static function getMealHistoryForUser(int $mealTypeId, int $userId, string $from, string $until): array
    {
        global $DB;

        $txt = "
            SELECT CONCAT(u.first_name, ' ' , u.last_name) AS full_name , reg.meal_date, 
                   reg.created_at, CONCAT(u2.first_name, ' ', u2.last_name) as created_by,
                    reg.deleted_at, CONCAT(u3.first_name, ' ', u3.last_name) as deleted_by 
            FROM registration reg
            JOIN app_user u ON reg.app_user = u.id AND (u.deleted_at IS NULL OR u.deleted_at >= '?2')
            LEFT JOIN app_user u2 ON reg.created_by = u2.id
            LEFT JOIN app_user u3 ON reg.deleted_by = u3.id
            WHERE reg.meal_type = ?0 AND reg.app_user = ?1 AND reg.meal_date BETWEEN '?2' AND '?3'
            ORDER BY reg.meal_date DESC, reg.created_at, reg.deleted_at
            LIMIT 100
        ";
        $sql = $DB->prepareSQL($txt, [$mealTypeId, $userId, $from, $until]);

        return $DB->query($sql);
    }

    /**
     * Get weekday numbers, when kitchen is closed.
     * @param int $kitchenId - id of the kitchen - default 1 (since at this point, multiple kitchens are not supported)
     * @return array
     */
    public static function getKitchenDaysOff(int $kitchenId = 1): array
    {
        global $DB;

        $txt = "
            SELECT days_off
            FROM kitchen
            WHERE id = ?0 AND deleted_at IS NULL
        ";
        $sql = $DB->prepareSQL($txt, [$kitchenId]);
        $res = $DB->query($sql);

        if ($res && $res[0] && !empty($res[0]['days_off'])) {
            return explode(',', $res[0]['days_off']);
        }

        return [];
    }
}