<?php
require __DIR__ . '/../../preventDirectAccess.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'Settings.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Date.php';

class AuthModel
{
    public static function getUser(string $email) {
        global $DB;

        $txt = "SELECT u.id, u.first_name, u.last_name, u.email, u.passwd, u.locked, GROUP_CONCAT(r.role_code) as role, 
                    (
                    SELECT COUNT(*)
                    FROM app_user_group ug
                    WHERE ug.app_user = u.id AND (ug.deleted_at IS NULL OR ug.deleted_at >= '?0')
                    ) AS has_meal
                FROM app_user u
                LEFT JOIN app_user_role ur ON u.id = ur.app_user
                LEFT JOIN app_role r ON ur.app_role = r.id AND (ur.deleted_at IS NULL OR ur.deleted_at >= '?0')
                WHERE u.email = '?1' AND u.created_at <= '?0' AND (u.deleted_at IS NULL OR u.deleted_at > '?0') AND u.locked = 0
                GROUP BY u.id
                ";
        $sql = $DB->prepareSQL($txt, [Core_Date::now('Y-m-d H:i:s'), $email]);
        $res = $DB->query($sql);

        if ($res && $res[0]) {
            return $res[0];
        }

        return [];
    }

    public static function isAuthenticatedLocal(string $encryptedPass, string $formPass): bool
    {
        if (!$encryptedPass || !$formPass) {
            return false;
        }

        try {
            $key = file_get_contents(APPLICATION_PATH . DS . 'config' . DS . 'crypto.key');
            $decoded = base64_decode($encryptedPass);
            $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
            $encrypted_result = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');
            $decryptedHash = sodium_crypto_secretbox_open($encrypted_result, $nonce, $key);

            return password_verify($formPass, $decryptedHash);
        } catch (Exception) {
            error_log(__METHOD__ . ' method crashed during password decryption');
            return false;
        }

    }

    public static function isAuthenticatedDomain(string $email, string $pass): bool {
        $email = preg_replace("/[^a-zA-Z.@\d_\ -]/", "", $email);
        $settings = new Settings();
        $settings->populateSettings();

        $ldap = ldap_connect($settings->getDomainUrl());

        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($ldap, $email, $pass); // @ supresses error messages

        if ($bind) {
            return true;
        }

        return false;
    }

    /**
     * Encrypts user password
     * @param string $password - Password to be encrypted (if no password is given, random password will be used).
     * @return string
     * @throws Exception
     */
    public static function encryptPassword(string $password): string
    {
        if (!$password) {
            $key = sodium_crypto_secretbox_keygen();
            $password = sodium_bin2hex($key);
        }
        try {
            $pwdHash = password_hash($password, PASSWORD_DEFAULT);
            $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
            $key = file_get_contents(APPLICATION_PATH . DS . 'config' . DS . 'crypto.key');
            $encrypted_result = sodium_crypto_secretbox($pwdHash, $nonce, $key);
            return base64_encode($nonce . $encrypted_result);
        } catch (Exception) {
            throw new Exception('Password encryption failed!');
        }
    }

    public static function logLoginAttempt(string $email, string $ipAddr, bool $isSuccessful,
                                           string $userAgent, int $userId = -1): bool
    {
        global $DB;

        $column = $userId > 0 ? 'app_user,' : '';
        $value = $userId > 0 ? '?0,' : '';

        $txt = "
            INSERT INTO log_login ({$column} email, ip_addr, login_time, success, user_agent)
            VALUES ({$value} '?1', '?2', '?3', ?4, '?5')
        ";
        $sql = $DB->prepareSQL($txt, [
            $userId,
            $email,
            $ipAddr,
            Core_Date::now('Y-m-d H:i:s'),
            $isSuccessful ? 1 : 0,
            $userAgent]);

        return $DB->db_edit($sql);
    }
}