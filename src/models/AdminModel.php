<?php
require __DIR__ . '/../../preventDirectAccess.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'UserModel.php';

class AdminModel
{
    public function getAdminMenu(int $userId) {
        global $router;

        $roles = [];
        $rolesInDb = UserModel::getUserRoles($userId);

        foreach ($rolesInDb as $r) {
            $roles[] = $r['role_code'];
        }

        if (!$roles) {
            return [];
        }

        $allLinks = [
            ['name' => 'Kasutajad', 'url' => '/' . $router->route('users_by_role', ['unknown']), 'canSee' => ['admin', 'observer']],
            ['name' => 'Kasutajate importimine', 'url' => '/' . $router->route('import'), 'canSee' => ['admin']],
            ['name' => 'Kaardid', 'url' => '/' . $router->route('cards',[1]), 'canSee' => ['admin', 'observer']],
            ['name' => 'Grupid', 'url' => '/' . $router->route('groups'), 'canSee' => ['admin', 'observer']],
            ['name' => 'Erandid', 'url' => '/' . $router->route('irregulars'), 'canSee' => ['admin', 'observer']],
            ['name' => 'Söögikorrad', 'url' => '/' . $router->route('meal-types'), 'canSee' => ['admin']],
            ['name' => 'Köögid', 'url' => '/' . $router->route('kitchens'), 'canSee' => ['admin']],
            ['name' => 'Köögi vaated', 'url' => '/' . $router->route('kitchen-views'), 'canSee' => ['admin', 'observer']]
        ];
        $links = [];

        foreach ($allLinks as $link) {
            if (array_intersect($roles, $link['canSee'])) {
                $links[] = $link;
            }
        }


        return $links;
    }
}