<?php
require __DIR__ . '/../../preventDirectAccess.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealTypeModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'KitchenModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'GroupModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'controllers' . DS . 'MenuController.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Util.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'MealType.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'Logger.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'LogRow.php';

class MealTypeController extends BaseController
{
    public function getMealTypes()
    {
        $this->authorized(['admin']);

        $data['content'] = 'meal-type/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['create_url'] = '/' . $this->router->route('meal-types_create');
        $data['page'] = 'admin';

        $yearId = Core_Session::getVar('selected_year');
        $mealTypesInDb = MealTypeModel::getAllMealTypes($yearId);
        $mealTypes = [];

        foreach ($mealTypesInDb as $key => $mealType) {
            $mealTypeObj = MealTypeModel::createMealTypeObj($mealType);
            $mealTypeObj->setEditUrl('/' . $this->router->route('meal-types_edit', [$mealTypeObj->getId()]));
            $mealTypes[] = $mealTypeObj;
        }

        $data['meal_types'] = $mealTypes;

        return twigRender($data);
    }

    public function getEdit(int $mealTypeId)
    {
        $this->authorized(['admin']);
        $yearId = Core_Session::getVar('selected_year');

        $mealTypeInDb = MealTypeModel::getMealTypeById($mealTypeId, $yearId);
        if (empty($mealTypeInDb)) {
            $this->redirect('error', [], ['error_msg' => 'Sellist söögikorda ei leitud!']);
        }

        $mealType = MealTypeModel::createMealTypeObj($mealTypeInDb[0]);

        $data = $this->getInitialData($mealType);
        $data['mealType'] = $mealType;
        $data['edit'] = true;
        $groups = [];

        foreach ($mealTypeInDb as $mtInDb) {
            if ($mtInDb['app_group']) {
                $groups[] = ['id' => $mtInDb['app_group'], 'group_name' => $mtInDb['group_name']];
            }
        }

        $data['groups'] = $data['groups'] ?: $groups;

        Core_Session::massUnsetVar([
            MealType::NAME,
            MealType::MEAL_TIME,
            MealType::KITCHEN_ID,
            MealType::REG_CUTOFF,
            MealType::COMMENTS,
            MealType::GROUPS,
            MealType::NAME_ERROR,
            MealType::BUSINESS_DAYS_ERROR,
            MealType::MEAL_TIME_ERROR,
            MealType::KITCHEN_ID_ERROR,
            MealType::REG_CUTOFF_ERROR,
            MealType::GENERAL_ERROR,
            MealType::GROUPS_ERROR
        ]);

        $data['groups_api_url'] = '/' . $this->router->route('api_groups_search');

        return twigRender($data);
    }

    public function postEdit()
    {
        $this->authorized(['admin']);

        // create object with form data
        $mealType = new MealType(Core_Util::param(MealType::NAME), Core_Util::param(MealType::MEAL_TIME));
        $mealType->setRegistrationCutoff(Core_Util::param(MealType::REG_CUTOFF) ?: Core_Date::now('H:i'));
        $id = Core_Util::param(MealType::ID);
        if ($id) {
            $mealType->setId(Core_Util::param(MealType::ID));
        }
        $mealType->setKitchenId(Core_Util::param(MealType::KITCHEN_ID));
        $mealType->setComments(Core_Util::param(MealType::COMMENTS));
        $mealType->setGroups(Core_Util::param(MealType::GROUPS) ?: []);

        // create business days
        $businessDays = Core_Util::param(MealType::BUSINESS_DAYS);

        if ($businessDays) {
            $mealType->setBusinessDays($businessDays);
        }


        $groupsData = [];

        // redirect rules (edit or create)
        $routeKey = $id ? 'meal-types_edit' : 'meal-types_create';
        $routeVar = $id ? [$id] : [];

        if ($mealType->getGroups()) {
            $groupsData = GroupModel::getGroupsById($mealType->getGroups());
        }

        // validate form data in object
        if (!$mealType->isValid(count($groupsData))) {
            foreach ($mealType->getAllErrors() as $varName => $error) {
                if (!empty($error)) {
                    Core_Session::setVar($varName, $error);
                }
            }

            error_log('Meal type is not valid!');
            $this->redirect($routeKey, $routeVar, [
                    MealType::NAME => $mealType->getName(),
                    MealType::BUSINESS_DAYS => $mealType->getBusinessDays(),
                    MealType::MEAL_TIME => $mealType->getMealTime(),
                    MealType::REG_CUTOFF => $mealType->getRegistrationCutoff(),
                    MealType::KITCHEN_ID => $mealType->getKitchenId(),
                    MealType::COMMENTS => $mealType->getComments(),
                    MealType::GROUPS => $mealType->getGroups()
                ]
            );
        }

        // if mealTypeId is set, then we are editing, if not, we are creating new
        if ($id) {
            $yearId = Core_Session::getVar('selected_year');
            $mealTypeInDb = MealTypeModel::getMealTypeById($id, $yearId);

            // if meal-type not found in DB, redirect to error page
            if (!$mealTypeInDb) {
                $this->redirect('error', [], ['error_msg' => 'Sellise ID-ga söögikorda ei leitud!']);
            }
            $mtDbObj = MealTypeModel::createMealTypeObj($mealTypeInDb[0]);
            $logger = new Logger();

            $isSuccessful = $this->updateMealType($mtDbObj, $mealType, $logger);

            if ($isSuccessful) {
                $groupsAlreadyInMealType = MealTypeModel::getIdsOfGroupsInMealType($id);
                $this->updateMealTypeGroups($id, $groupsData, $groupsAlreadyInMealType);
                $this->redirect('meal-types');
            }

        } else {
            error_log('Inserting new mealType.');
            $id = $this->insertMealType($mealType);
            if ($id > 0) {
                $this->insertMealTypeGroups($id, $groupsData);
                error_log('Inserting new mealType complete.');
                $this->redirect('meal-types');
            }
        }

        // if program reaches this point, then saving to DB failed
        $mealType->setGeneralError('Söögikorra salvestamine ebaõnnestus!');
        error_log('Söögikorra salvestamine ebaõnnestus');
        $this->redirect($routeKey, $routeVar, [
            MealType::NAME => $mealType->getName(),
            MealType::BUSINESS_DAYS => $mealType->getBusinessDays(),
            MealType::MEAL_TIME => $mealType->getMealTime(),
            MealType::REG_CUTOFF => $mealType->getRegistrationCutoff(),
            MealType::KITCHEN_ID => $mealType->getKitchenId(),
            MealType::COMMENTS => $mealType->getComments(),
            MealType::GENERAL_ERROR => $mealType->getGeneralError()
        ]);
    }

    public function getCreate()
    {
        $this->authorized(['admin']);
        $mealType = new MealType('', '');

        $data = $this->getInitialData($mealType);
        $data['edit'] = false;

        // delete form session variables
        Core_Session::massUnsetVar([
            MealType::NAME,
            MealType::BUSINESS_DAYS,
            MealType::MEAL_TIME,
            MealType::KITCHEN_ID,
            MealType::REG_CUTOFF,
            MealType::COMMENTS,
            MealType::NAME_ERROR,
            MealType::BUSINESS_DAYS_ERROR,
            MealType::MEAL_TIME_ERROR,
            MealType::KITCHEN_ID_ERROR,
            MealType::REG_CUTOFF_ERROR
        ]);

        $data['groups_api_url'] = '/' . $this->router->route('api_groups_search');

        return twigRender($data);
    }

    private function getInitialData(MealType $mealType): array
    {
        $data['content'] = 'meal-type/edit.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['post_url'] = '/' . $this->router->route('meal-types_edit_post');
        $data['page'] = 'admin';
        $data['comment_tooltip'] = 'Kommentaari näidatakse sööjate märkimisel (sööjate tabeli kohal).';

        // get data from session
        $mealType->setNameError(Core_Session::getVar(MealType::NAME_ERROR) ?: '');
        $mealType->setBusinessDaysError(Core_Session::getVar(MealType::BUSINESS_DAYS_ERROR) ?: '');
        $mealType->setMealTimeError(Core_Session::getVar(MealType::MEAL_TIME_ERROR) ?: '');
        $mealType->setKitchenError(Core_Session::getVar(MealType::KITCHEN_ID_ERROR) ?: '');
        $mealType->setRegistrationCutoffError(Core_Session::getVar(MealType::REG_CUTOFF_ERROR) ?: '');
        $mealType->setCommentsError(Core_Session::getVar('comments_error') ?: '');
        $mealType->setGroupsError(Core_Session::getVar(MealType::GROUPS_ERROR) ?: '');
        $mealType->setGeneralError(Core_Session::getVar(MealType::GENERAL_ERROR) ?: '');

        $name = Core_Session::getVar(MealType::NAME);
        $businessDays = Core_Session::getVar(MealType::BUSINESS_DAYS);
        $mealTime = Core_Session::getVar(MealType::MEAL_TIME);
        $registrationCutoff = Core_Session::getVar(MealType::REG_CUTOFF);
        $comments = Core_Session::getVar(MealType::COMMENTS);
        $kitchenId = Core_Session::getVar(MealType::KITCHEN_ID);
        $groups = Core_Session::getVar(MealType::GROUPS);

        if ($name) {
            $mealType->setName($name);
        }

        if ($businessDays) {
            $mealType->setBusinessDays($businessDays);
        }

        if ($mealTime) {
            try {
                $mealType->setMealTime($mealTime);
            } catch (Exception) {
            }
        }
        if ($registrationCutoff) {
            $mealType->setRegistrationCutoff($registrationCutoff);
        }
        if ($comments) {
            $mealType->setComments($comments);
        }
        if ($kitchenId) {
            $mealType->setKitchenId($kitchenId);
        }

        $data['kitchens'] = KitchenModel::getAllKitchens();

        if (!$data['kitchens']) {
            $mealType->setKitchenError("VIGA: Ühtegi kööki ei leitud");
        }

        $data['groups'] = $groups ? GroupModel::getGroupsById($groups) : "";
        $data['meal_type'] = $mealType;

        return $data;
    }

    public function updateMealType(MealType $oldMt, MealType $newMt, Logger $logger): bool
    {
        if ($oldMt->getId() != $newMt->getId()) {
            return false;
        }

        $ins = [];
        $tblName = 'meal_type';
        $changedBy = $this->currentUserId();

        if ($oldMt->getName() != $newMt->getName()) {
            $ins['meal_type_name'] = $newMt->getName();
            $logger->addLogRow(new LogRow($tblName, $oldMt->getId(), 'meal_type_name',
                $oldMt->getName(), $newMt->getName(), $changedBy));
        }

        if ($oldMt->getBusinessDays() != $newMt->getBusinessDays()) {
            $ins['business_days'] = implode(',', $newMt->getBusinessDays());
            $logger->addLogRow(new LogRow($tblName, $oldMt->getId(), 'business_days',
                $oldMt->getEnabledDaysStr(), $newMt->getEnabledDaysStr(), $changedBy));
        }

        if ($oldMt->getMealTime() != $newMt->getMealTime()) {
            $ins['meal_time'] = $newMt->getMealTime();
            $logger->addLogRow(new LogRow($tblName, $oldMt->getId(), 'meal_time',
                $oldMt->getMealTime(), $newMt->getMealTime(), $changedBy
            ));
        }

        if ($oldMt->getKitchenId() != $newMt->getKitchenId()) {
            $ins['kitchen'] = $newMt->getKitchenId();
            $logger->addLogRow(new LogRow($tblName, $oldMt->getId(), 'kitchen',
                $oldMt->getKitchenId(), $newMt->getKitchenId(), $changedBy
            ));
        }

        if ($oldMt->getRegistrationCutoff() != $newMt->getRegistrationCutoff()) {
            $ins['registration_cutoff'] = $newMt->getRegistrationCutoff();
            $logger->addLogRow(new LogRow($tblName, $oldMt->getId(), 'registration_cutoff',
                $oldMt->getRegistrationCutoff(), $newMt->getRegistrationCutoff(), $changedBy));
        }

        if ($oldMt->getComments() != $newMt->getComments()) {
            $ins['comments'] = $newMt->getComments();
            $logger->addLogRow(new LogRow($tblName, $oldMt->getId(), 'comments',
                $oldMt->getComments(), $newMt->getComments(), $changedBy));
        }

        if (!empty($ins)) {
            $successful = MealTypeModel::updateMealType($ins, $oldMt->getId());

            if ($successful) {
                $logger->save();
            }

            return $successful;
        }

        return true;
    }

    public function updateMealTypeGroups(int $mealTypeId, array $groups, array $groupsAlreadyInMealType)
    {
        $existingGroups = [];
        $allGroups = [];
        $currentUser = $this->currentUserId();

        foreach ($groupsAlreadyInMealType as $groupInMealType) {
            $existingGroups[$groupInMealType['app_group']] = $groupInMealType['group_mt_id'];
        }

        foreach ($groups as $group) {
            if (!in_array($group['id'], array_keys($existingGroups))) {
                MealTypeModel::addGroupMealType($group['id'], $mealTypeId, $currentUser);
            }

            $allGroups[$group['id']] = 0;
        }

        foreach (array_diff_key($existingGroups, $allGroups) as $val) {
            MealTypeModel::removeGroupMealType($val, $currentUser);
        }
    }

    public function insertMealType(MealType $mealType): int
    {
        $ins = [];
        $createdBy = $this->currentUserId();

        try {
            $ins[MealType::NAME] = $mealType->getName();
            $ins[MealType::BUSINESS_DAYS] = implode(',', $mealType->getBusinessDays());
            $ins[MealType::MEAL_TIME] = $mealType->getMealTime();
            $ins[MealType::KITCHEN_ID] = $mealType->getKitchenId();
            $ins[MealType::REG_CUTOFF] = $mealType->getRegistrationCutoff();
            $ins[MealType::COMMENTS] = $mealType->getComments();
            $ins[MealType::CREATED_BY] = $createdBy;
            $id = MealTypeModel::insertMealType($ins);
            return $id;
        } catch (Exception) {
            return 0;
        }
    }

    public function insertMealTypeGroups(int $mealTypeId, array $groups)
    {
        $currentUser = $this->currentUserId();

        foreach ($groups as $group) {
            MealTypeModel::addGroupMealType($group['id'], $mealTypeId, $currentUser);
        }
    }
}