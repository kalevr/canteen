<?php
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'UpdateConfig.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'AuthModel.php';

class InstallController extends BaseController
{
    private string $filePath = APPLICATION_PATH . DS . 'config' . DS . 'client_conf.php';
    private string $exampleFilePath = APPLICATION_PATH . DS . 'config' . DS . 'client_conf_example.php';
    private UpdateConfig $config;

    public function getInstall()
    {
        // if client_conf.php doesn't exist, let's copy it from example
        if (!file_exists($this->filePath)) {
            error_log('client_conf.php not found. Copying client_conf_example.php to client_conf.php instead.');
            copy($this->exampleFilePath, $this->filePath);
        }

        $this->config = new UpdateConfig();
        $isInstalled = $this->config->getVariable('IS_INSTALLED');

        if ($isInstalled == 'true') {
            header('location: /');
            return;
        }

        $variablesDirty = $this->config->getVariablePairs();
        $variables = [];

        foreach ($variablesDirty as $key => $variable) {
            $variables[$key] = trim($variable, "'");
        }

        $data = $this->getInitialData($variables);

        $data['session_prefix_tooltip'] = 'Kui samas serveris kasutatakse mitut koopiat söökla tarkvarast, siis peavad sessiooni eesliited unikaalsed olema';
        $data['menu_url_tooltip'] = "Kui söökla menüü asub mõnel välisel veebilehel, siis siia sisestatud link läheb peamenüüsse";
        $data['site_subdir_tooltip'] = "Kui söökla ei asu veebisaidi juurel (exmple.com/) vaid alamkaustas (example.com/canteen), siis tuleb siia alamkataloogi nimi kirjutada... koos kaldkriipsuga (/canteen)";
        $data['unrestricted_roles_tooltip'] = "Järgnevate rollidega kasutajad saavad igal ajal sööjaid lisada ja maha võtta (ka siis kui teiste kasutajate jaoks asi lukku läheb)";
        $data['auto_update_tooltip'] = "Päris automaatset uuendust ei ole. Programmi uuendamine käib nii, et tõmbad uue versiooni internetist ja kopeerid failid serverisse. Kui automaatne uuendus on sisse lülitatud, siis käivitatakse uuenduse skript automaatselt kui keegi lehte külastab.";
        $data['participation_tooltip'] = "Kui sööklas kohalolukontrolli ei soovita, siis siit saab selle välja lülitada.";

        return twigRender($data);
    }

    public function postInstall()
    {
        $this->config = new UpdateConfig();
        $isInstalled = $this->config->getVariable('IS_INSTALLED');

        if ($isInstalled == 'true') {
            header('location: /');
            return;
        }

        $dbUser = Core_Util::param('db_user');
        $dbPass = Core_Util::param('db_pass');
        $dbName = Core_Util::param('db_name');
        $dbServer = Core_Util::param('db_server');
        $sessionPrefix = Core_Util::param('session_prefix');
        $menuUrl = Core_Util::param('menu_url');
        $siteSubdir = Core_Util::param('site_subdir');
        $editRoles = Core_Util::param('edit_roles');
        $autoUpdate = (bool)Core_Util::param('auto_update_enabled');
        $participationDisabled = (bool)Core_Util::param('participation_disabled');

        if (empty($dbUser) || empty($dbPass) || empty($dbName) || empty($dbServer) || empty($editRoles)) {
            Core_Session::massSetVar([
                'db_user' => $dbUser,
                'db_pass' => $dbPass,
                'db_name' => $dbName,
                'db_server' => $dbServer,
                'session_prefix' => $sessionPrefix,
                'menu_url' => $menuUrl,
                'site_subdir' => $siteSubdir,
                'auto_update_enabled' => $autoUpdate,
                'participation_disabled' => $participationDisabled,
                'general_error' => 'Viga: Kõik kohustuslikud väljad ei ole täidetud!']);
            header('location: /install');
            exit();
        }

        try {
            error_log('user: ' . $dbUser . ' - pass: ' . $dbPass . ' - dbName: ' . $dbName . ' - server: ' . $dbServer);
            $DB = new Core_Database($dbUser, $dbPass, $dbName, $dbServer, false);
        } catch (Exception) {
            error_log('Canteen installation failed, because database info was incorrect (or user had wrong permissions)');
            Core_Session::massSetVar([
                'db_user' => $dbUser,
                'db_pass' => $dbPass,
                'db_name' => $dbName,
                'db_server' => $dbServer,
                'session_prefix' => $sessionPrefix,
                'menu_url' => $menuUrl,
                'site_subdir' => $siteSubdir,
                'auto_update_enabled' => $autoUpdate,
                'participation_disabled' => $participationDisabled,
                'general_error' => 'Viga: Andmebaasiga ühendus ebaõnnestus (kontrolli, kas kasutajanimi ja 
                parool on õiged - ning et sellise nimega andmebaas on olemas koos vajalike juurdepääsuõigustega)!']);
            header('location: /install');
            exit();
        }

        $currentConf = new UpdateConfig();
        $currentConf->updateStrVariable('DB_USER', $dbUser);
        $currentConf->updateStrVariable('DB_PASSWORD', $dbPass);
        $currentConf->updateStrVariable('DB_NAME', $dbName);
        $currentConf->updateStrVariable('DB_SERVER', $dbServer);
        $currentConf->updateStrVariable('SESSION_PREFIX', $sessionPrefix);
        $currentConf->updateStrVariable('MENU_URL', $menuUrl);
        $currentConf->updateStrVariable('SITE_SUBDIR', $siteSubdir);
        $currentConf->updateVariable('AUTO_UPDATE_ENABLED', $autoUpdate ? 'true' : 'false');
        $currentConf->updateVariable('PARTICIPATION_DISABLED', $participationDisabled ? 'true' : 'false');

        $editRolesArr = [];

        foreach ($editRoles as $editRole) {
            $editRolesArr[] = "'" . $editRole . "'";
        }

        $editRolesStr = '[' . implode(',', $editRolesArr) . ']';
        $currentConf->updateVariable('EDIT_ANY_TIME_ROLES', $editRolesStr);

        $nrOfTables = $this->getNrOfTablesInDb($DB, $dbName);

        error_log('nrOfTables: ' . $nrOfTables);
        if ($nrOfTables < 15) {
            error_log('less than 15 tables');
            try {
                $this->runDbInstall($DB);
            } catch (Exception $e) {
                exit();
            }
        }

        // create .key file for password encryption if doesn't exist
        if (!file_exists(APPLICATION_PATH . DS . 'config' . DS . 'crypto.key')) {
            $key = sodium_crypto_secretbox_keygen();
            file_put_contents(APPLICATION_PATH . DS . 'config' . DS . 'crypto.key', $key);
        }

        // create and encrypt default admin password
        $pass = AuthModel::encryptPassword('Password123');
        $sql = "UPDATE app_user SET passwd = '{$pass}' WHERE email = 'admin@localhost'";
        $DB->query($sql);

        $currentConf->updateVariable('IS_INSTALLED', 'true');
        $currentConf->saveToFile();

        Core_Session::massUnsetVar(['db_user', 'db_pass', 'db_name', 'db_server', 'session_prefix', 'menu_url', 'site_subdir',
            'auto_update_enabled', 'participation_disabled', 'general_error']);
        header('location: ' . ($siteSubdir ?: '/'));
    }

    private function getInitialData(array $variables): array
    {
        $data['content'] = 'install/index.twig';
        $data['site_subdir'] = $this->subdir;

        // Database details
        $data['db_user'] = Core_Session::getVar('db_user') ?: $variables['DB_USER'];
        $data['db_pass'] = Core_Session::getVar('db_pass') ?: $variables['DB_PASSWORD'];
        $data['db_name'] = Core_Session::getVar('db_name') ?: $variables['DB_NAME'];
        $data['db_server'] = Core_Session::getVar('db_server') ?: $variables['DB_SERVER'];

        $data['session_prefix'] = Core_Session::getVar('session_prefix') ?: $variables['SESSION_PREFIX'];
        $data['menu_url'] = Core_Session::getVar('menu_url') ?: $variables['MENU_URL'];
        $data['site_subdir'] = Core_Session::getVar('site_subdir') ?: $variables['SITE_SUBDIR'];
        $data['edit_any_time_roles'] = $variables['EDIT_ANY_TIME_ROLES'];
        $data['auto_update_enabled'] = Core_Session::getVar('auto_update_enabled') ?: $variables['AUTO_UPDATE_ENABLED'];
        $data['participation_disabled'] = Core_Session::getVar('participation_disabled') ?: $variables['PARTICIPATION_DISABLED'];

        $generalError = Core_Session::getVar('general_error');

        if ($generalError) {
            $data['general_error'] = $generalError;
        }

        Core_Session::massUnsetVar(['db_user', 'db_pass', 'db_name', 'db_server', 'session_prefix', 'menu_url', 'site_subdir',
            'auto_update_enabled', 'participation_disabled', 'general_error']);

        return self::getRolesFromString($data);
    }

    private function getRolesFromString(array $data)
    {
        if (empty($data) || empty($data['edit_any_time_roles']) || $data['edit_any_time_roles'] == '[]') {
            return $data;
        }

        // remove square brackets
        $rolesStr = substr(trim($data['edit_any_time_roles']), 1, -1);
        $rolesArr = explode(', ', $rolesStr);

        foreach ($rolesArr as $role) {
            switch (trim($role, "'")) {
                case 'admin':
                    $data['edit_admin'] = true;
                    break;
                case 'observer':
                    $data['edit_observer'] = true;
                    break;
                case 'cook':
                    $data['edit_cook'] = true;
                    break;
                case 'homeroom-teacher':
                    $data['edit_hr_teacher'] = true;
                    break;
            }
        }

        return $data;
    }

    private function runDbInstall(Core_Database $DB) {
        $path = APPLICATION_PATH . DS . 'install' . DS . 'install.sql';
        $sql = file_get_contents($path);
        $res = $DB->processSqlFile($sql);

        if ($res) {
            error_log('Ran DB install from ' . $path);
        } else {
            error_log('Failed to edit DB. (reason can be find before this line in error log). ');
            throw new Exception();
        }
    }

    /**
     * Get number of tables in database
     * @param Core_Database $DB
     * @param string $dbName
     * @return int
     */
    private function getNrOfTablesInDb(Core_Database $DB, string $dbName): int
    {
        $txt = "
            SELECT COUNT(*) AS nr_of_tables
            FROM information_schema.tables
            WHERE table_schema = '?0';
        ";
        $sql = $DB->prepareSQL($txt, [$dbName]);
        error_log($sql);
        $res = $DB->query($sql);

        if ($res) {
            return (int) $res[0];
        }

        return 0;
    }
}