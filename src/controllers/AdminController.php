<?php
require __DIR__ . '/../../preventDirectAccess.php';

require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'AdminModel.php';

class AdminController extends BaseController
{
    public function getAdminMenu(): string
    {
        $this->authorized(['admin','observer']);

        $model = new AdminModel();
        $data['adm_links'] = $model->getAdminMenu($this->currentUserId());
        $data['content'] = 'admin/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['page'] = 'admin';

        return twigRender($data);
    }
}