<?php
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'ParticipationModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'controllers' . DS . 'MealController.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'MealType.php';

class ParticipationController extends BaseController
{
    public function getParticipations(int $mealTypeId, int $groupId, string $date = ''): string
    {
        global $config;
        $this->authorized(['admin', 'observer', 'cook', 'homeroom-teacher']);
        $this->ifDisabledRedirect();

        if (!$date) {
            $date = Core_Date::now();
        }


        $isAdminObserverOrCook = $this->hasRole(['admin', 'observer', 'cook']);

        $isValid = MealController::isNotAllowedOrInvalidInput($this->currentUserId(), $groupId, $mealTypeId, $isAdminObserverOrCook);

        if (!$isValid) {
            $this->redirect('error');
        }

        $groupMealType = MealModel::groupMealType($mealTypeId, $groupId, $date);

        if (!$groupMealType) {
            $this->redirect('error', [], ['error_msg' => 'Sellel grupil ei ole sellist söögikorda.']);
        }

        try {
            $dateData = MealController::getDateData($date);
        } catch (Exception) {
            $this->redirect('error', [], ['error_msg' => 'Viga: Kuupäevaga paistab mingi probleem olevat!']);
        }

        $pageName = $groupMealType['meal_type_name'] . ' - ' . $groupMealType['group_name'];
        $allDaysInMonth = IrregularDatesModel::daysInMonthUpdatedByDb($mealTypeId, $groupMealType[MealType::BUSINESS_DAYS], $date);
        $businessDays = [];
        $data['all_daynames'] = Core_Date::getWeekdayNamesShort();


        /** @var $users User[] */
        $users = [];

        $meals = MealModel::getMealsForTypeForGroupBetweenDates($mealTypeId, $groupId, $dateData['from'], $dateData['until']);
        $participations = ParticipationModel::getParticipationsForTypeForGroupBetweenDates($mealTypeId, $groupId,
            $dateData['from'], $dateData['until']);

        foreach ($meals as $meal) {
            $userId = $meal['id'];
            if (!array_key_exists($userId, $users)) {
                $users[$userId] = new User($meal['first_name'], $meal['last_name']);
                $users[$userId]->setId($userId);
                $users[$userId]->setHistoryUrl('/' . $this->router->route('participation_history', [$mealTypeId, $userId, $date]));
            }

            if ($meal['meal_date']) {
                $newMeal = new Meal($mealTypeId, $meal['meal_date']);
                $newMeal->setIsSelected(true);

                $users[$userId]->addMeal($newMeal);
            }
        }

        foreach ($participations as $participation) {
            $userId = $participation['id'];
            $mealDate = $participation['meal_date'];

            if ($mealDate) {
                $userMeal = $users[$userId]->getMealByDate($mealDate);

                if ($userMeal) {
                    $userMeal->setDidParticipate(true);
                } else {
                    $newMeal = new Meal($mealTypeId, $participation['meal_date']);
                    $newMeal->setDidParticipate(true);
                    $users[$userId]->addMeal($newMeal);
                }
            }
        }


        $dayNames = [];
        $data['first_weekday'] = -1;
        $data['first_weekday_mobile'] = Core_Date::dayOfWeek($dateData['from']);

        foreach ($allDaysInMonth as $k => $v) {
            $day = Core_Date::get($dateData['yearMonth'] . $k, 'Y-m-d');
            $dayName = Core_Date::getDayName($day, true);
            //$kPadded = str_pad($k, 2, '0', STR_PAD_LEFT);

            if ($v['is_enabled'] == 1) {
                $businessDays[$k] = $dayName;

                if ($data['first_weekday'] == -1) {
                    $data['first_weekday'] = Core_Date::dayOfWeek($day);
                }

                if (!in_array($dayName, $dayNames)) {
                    $dayNames[] = $dayName;
                }
            }
        }

        foreach ($users as $user) {
            $user->addEmptyMeals($allDaysInMonth, $mealTypeId);
        }

        if ($isAdminObserverOrCook) {
            $allMtGroupsFromDb = MealModel::getAllMealGroups($mealTypeId, $dateData['from'], $dateData['until']);
        } else {
            $allMtGroupsFromDb = MealModel::getAllMealGroups($mealTypeId, $dateData['from'], $dateData['until'], $this->currentUserId());
        }

        $groups = [];

        foreach ($allMtGroupsFromDb as $mtGroupFromDb) {
            $groups[] = ['name' => $mtGroupFromDb['group_name'],
                'url' => '/' . $this->router->route('participations', [$mealTypeId, $mtGroupFromDb['id'], $date])];
        }

        $data['business_days'] = $businessDays;
        $data['can_edit'] = $this->hasRole($config['EDIT_ANY_TIME_ROLES']);
        $data['day_names'] = MealModel::sortDayNamesShort($dayNames);
        $data['number_of_weekdays'] = count($dayNames);
        $data['month_year'] = Core_Date::getMonthName($date) . ' ' . Core_Date::get($date, 'Y');
        $data['link_next'] = '/' . $this->router->route('participations', [$mealTypeId, $groupId, $dateData['nextMonth']]);
        $data['link_previous'] = '/' . $this->router->route('participations', [$mealTypeId, $groupId, $dateData['previousMonth']]);
        $data['users'] = $users;
        $data['meal_type_id'] = $mealTypeId;
        $data['group_id'] = $groupId;
        $data['groups'] = $groups;
        $data['date'] = $date;
        $data['post_url'] = '/' . $this->router->route('participations_post');
        $data['content'] = 'participation/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['title'] = $pageName;
        $data['page_name'] = $pageName;
        $data['isMobile'] = Core_Util::isMobileDevice($_SERVER['HTTP_USER_AGENT']);

        return twigRender($data);
    }

    public function getCurrentParticipations(int $mealTypeId = null): string
    {
        $this->authorized(['admin', 'cook']); // TODO: remove admin (it's for testing only).
        $this->ifDisabledRedirect();

        $mealTypes = [];
        $yearId = AcademicYear::getCurrent();

        $mealTypesInDb = ParticipationModel::getAllMealTypes();

        if ($mealTypesInDb[0] && $mealTypesInDb[0]['id']) {
            $mealTypeId = $mealTypeId ?: $this->getClosestMealTypeByTime($mealTypesInDb);

            foreach ($mealTypesInDb as $mt) {
                $mealTypes[] = [
                    'name' => $mt[MealType::NAME],
                    'url' => '/' . $this->router->route('current-participations', [$mt['id']]),
                    'active' => $mt['id'] == $mealTypeId ? 1 : 0
                ];

                if ($mt['id'] == $mealTypeId) {
                    $data['meal_type_name'] = $mt[MealType::NAME];
                }
            }
        }

        if ($mealTypeId) {
            $participations = ParticipationModel::getCurrentParticipations($mealTypeId, $yearId);
            $participationsByGroup = [];

            foreach ($participations as $participation) {
                $status = 0;
                $class = 'participation-missing-noreg';

                if ($participation['registration'] == 1 && $participation['participation'] == 0) {
                    $status = 1;
                    $class = 'participation-missing';
                } else if ($participation['registration'] == 1 && $participation['participation'] == 1) {
                    $status = 2;
                    $class = 'participation-ok';
                } else if ($participation['registration'] == 0 && $participation['participation'] == 1) {
                    $status = 3;
                    $class = 'participation-noreg';
                }

                $participationsByGroup[$participation['group_name']][] = [
                    'app_user' => $participation['app_user'],
                    'full_name' => $participation['first_name'] . ' ' . $participation['last_name'],
                    'meal_type_id' => $participation['meal_type_id'],
                    'group_name' => $participation['group_name'],
                    'status' => $status,
                    'class' => $class
                ];
            }
        }

        $data['content'] = 'participation/now.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['page'] = 'current-participations';
        $data['groups'] = $participationsByGroup;
        $data['meal_types'] = $mealTypes;
        $data['date'] = Core_Date::now();
        $data['onscan_enabled'] = true;
        $data['participation_api_url'] = '/' . $this->router->route('api_participation_add');
        $data['participation_api2_url'] = '/' . $this->router->route('api_participation_add-by-id');

        if ($mealTypeId > 0) {
            $data['meal_type_id'] = $mealTypeId;
        }

        return twigRender($data);
    }

    public function postParticipations()
    {
        $this->authorized(['admin', 'observer', 'cook']);
        $this->ifDisabledRedirect();

        // get general POST data
        $mealTypeId = Core_Util::param('meal_type_id');
        $groupId = Core_Util::param('group_id');
        $date = Core_Util::param('date');
        $currentUserId = $this->currentUserId();
        $isMyMeal = (bool)Core_Session::getVar('is_my_meal');

        if ($isMyMeal) {
            Core_Session::unsetVar('is_my_meal');
        }

        if (empty($mealTypeId) || empty($groupId) || empty($date)) {
            $this->redirect('error', [], ['error_msg' => 'Puudulikud lähteandmed!']);
        }

        $isAdminObserverOrCook = $this->hasRole(['admin', 'observer', 'cook']);
        $isValid = MealController::isNotAllowedOrInvalidInput($this->currentUserId(), $groupId, $mealTypeId, $isAdminObserverOrCook);

        if (!$isValid) {
            $this->redirect('error');
        }

        $groupMealType = MealModel::groupMealType($mealTypeId, $groupId, $date);

        if (!$groupMealType) {
            $this->redirect('error', [], ['error_msg' => 'Sellel grupil ei ole sellist söögikorda.']);
        }

        // take meal dates from $_POST
        $postDates = [];
        foreach ($_POST as $k => $v) {
            if (str_starts_with($k, 'date_')) {
                $data = explode('_', $k);
                $id = $data[1];
                $day = $data[2];

                $postDates[$id]['days'][$day] = $v;
            }
        }

        $allDaysInMonth = IrregularDatesModel::daysInMonthUpdatedByDb($mealTypeId, $groupMealType[MealType::BUSINESS_DAYS], $date);
        $disabledDays = [];

        foreach ($allDaysInMonth as $dayInMonth => $value) {
            if (!$value['is_enabled']) {
                $disabledDays[] = $dayInMonth;
            }
        }

        $from = Core_Date::firstDayOfMonth($date);
        $until = Core_Date::lastDayOfMonth($date);
        $yearMonth = Core_Date::get($date, 'Y-m-');

        $dbUsers = [];
        $postUsers = [];
        $participationsInDb = ParticipationModel::getParticipationsForTypeForGroupBetweenDates($mealTypeId, $groupId, $from, $until);

        foreach ($participationsInDb as $participation) {
            $userId = $participation['id'];
            $participationDate = $participation['meal_date'] ? Core_Date::get($participation['meal_date'], 'd') : null;

            if (!array_key_exists($userId, $dbUsers)) {
                $dbUsers[$userId] = new User();
                $dbUsers[$userId]->setId($userId);
            }

            if ($participationDate && !in_array($participationDate, $disabledDays)) {
                $newMeal = new Meal($mealTypeId, $participation['meal_date']);
                $dbUsers[$userId]->addMeal($newMeal);
            }
        }

        // create objects out of POST data (User and Meal)
        foreach ($postDates as $uId => $days) {
            if (!array_key_exists($uId, $postUsers)) {
                $postUsers[$uId] = new User();
                $postUsers[$uId]->setId($uId);
            }

            foreach ($days['days'] as $day => $status) {
                if (($status == 1 || $status == 3) && !in_array($day, $disabledDays)) {
                    $myDate = Core_Date::get($yearMonth . $day, 'Y-m-d');

                    $postUsers[$uId]->addMeal(new Meal($mealTypeId, $myDate));
                }
            }
        }

        // add participations
        foreach ($postUsers as $postUser) {
            if (array_key_exists($postUser->getId(), $dbUsers)) {
                foreach ($postUser->findUniqueParticipationsForUser($dbUsers[$postUser->getId()]) as $postMeal) {
                    $postMealDate = $postMeal->getMealDate()->format('Y-m-d');

                    $success = ParticipationModel::addParticipation($mealTypeId, $postUser->getId(), $postMealDate, $this->currentUserId());
                    if (!$success) {
                        error_log("Adding new meal to DB failed! (mealTypeId = {$mealTypeId}, userId = {$postUser->getId()},
                    mealDate = {$postMealDate}, createdBy = {$this->currentUserId()}");
                    }
                }
            }
        }

        // remove participations
        foreach ($dbUsers as $dbUser) {
            if (array_key_exists($dbUser->getId(), $postUsers)) {
                foreach ($dbUser->findUniqueParticipationsForUser($postUsers[$dbUser->getId()]) as $dbMeal) {
                    $dbMealDate = $dbMeal->getMealDate()->format('Y-m-d');

                    $success = ParticipationModel::removeParticipation($mealTypeId, $dbUser->getId(), $dbMealDate, $this->currentUserId());
                    if (!$success) {
                        error_log("Setting meal to 'deleted' in DB failed! (mealTypeId = {$mealTypeId},
                    userId = {$dbUser->getId()}, mealDate = {$dbMealDate}, createdBy = {$this->currentUserId()}");
                    }
                }
            }
        }

        $this->redirect('participations', [$mealTypeId, $groupId, $date]);
    }

    public function getParticipationHistoryForUser(int $mealTypeId, int $userId, string $date = ''): string
    {
        if ($this->currentUserId() != $userId && !$this->hasRole(['admin', 'observer', 'cook', 'homeroom-teacher'])) {
            $this->redirect('error', [], ['error_msg' => 'Teil puudub õigus selle lehe vaatamiseks!']);
        }

        try {
            $from = Core_Date::firstDayOfMonth($date);
            $until = Core_Date::lastDayOfMonth($date);
            $nextMonth = Core_Date::add($until, 'P1D')->format('Y-m-d');
            $previousMonth = Core_Date::sub($from, 'P1D')->format('Y-m-d');
            $yearMonth = Core_Date::get($date, 'Y-m-');
        } catch (Exception) {
            $this->redirect('error', [], ['error_msg' => 'Viga: Kuupäevaga paistab mingi probleem olevat!']);
        }

        $participationsInDb = ParticipationModel::getParticipationHistoryForUser($mealTypeId, $userId, $from, $until);
        $participations = [];

        foreach ($participationsInDb as $key => $participationInDb) {
            if ($key == 0) {
                $data['user_full_name'] = $participationInDb['full_name'];
            }
            $participationDate = Core_Date::get($participationInDb['meal_date'], 'd.m.Y');
            $createdAt = Core_Date::get($participationInDb['created_at'], 'd.m.Y H:i:s');

            $participations[$participationDate][$createdAt]['created'] = true;
            $participations[$participationDate][$createdAt]['edited_by'] = $participationInDb['created_by'];

            if ($participationInDb['deleted_at']) {
                $deletedAt = Core_Date::get($participationInDb['deleted_at'], 'd.m.Y H:i:s');
                $participations[$participationDate][$deletedAt]['created'] = false;
                $participations[$participationDate][$deletedAt]['edited_by'] = $participationInDb['deleted_by'];
            }
        }

        foreach ($participations as $participation) {
            krsort($participation);
        }

        $myMealTypes = MealTypeModel::getAllMealTypesForUser($userId);
        $mealTypes = [];

        foreach ($myMealTypes as $myMealType) {
            $mealTypes[] = ['name' => $myMealType['meal_type_name'],
                'url' => '/' . $this->router->route('meal_history', [$myMealType['id'], $userId, $date])];
        }

        $data['meal_types'] = $mealTypes;
        $data['participations'] = $participations;
        $data['user_id'] = $userId;
        $data['month_year'] = Core_Date::getMonthName($date) . ' ' . Core_Date::get($date, 'Y');
        $data['link_next'] = '/' . $this->router->route('participation_history', [$mealTypeId, $userId, $nextMonth]);
        $data['link_previous'] = '/' . $this->router->route('participation_history', [$mealTypeId, $userId, $previousMonth]);
        $data['content'] = 'participation/history.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];

        return twigRender($data);
    }

    public function postParticipationAddApi()
    {
        global $config;

        if (!$this->hasRole(['cook', 'admin']) || $config['PARTICIPATION_DISABLED']) {
            error_log('Puudub õigus kohalolu märkida (kas vale roll või kohalolu kontroll on välja lülitatud)');
            return '{}';
        }

        $sCode = Core_Util::param('sCode');
        $mealType = Core_Util::param('mealtype');
        $createdBy = $this->currentUserId();
        $date = Core_Date::now();
        // $date = '2022-04-15'; // for debugging

        if (empty($sCode) || empty($mealType)) {
            error_log('Puudulikud andmed. (sCode: ' . $sCode . ' - mealType: ' . $mealType . ')');
            return '{"status" : 7, "message" : "Puudulikud andmed!"}';
        }

        $userId = ParticipationModel::getUserIdByCardCode($sCode);

        if (!$userId) {
            error_log('Tundmatu kaart: ' . $sCode);
            return '{ "status" : 8, "message" : "Tundmatu kaart!"}';
        }

        $userData = ParticipationModel::getCurrentParticipationByUserIdAndMealTypeId($userId[0]['id'], $mealType);
        $status = 0;

        $registration = $userData[0]['registration'];
        $participation = $userData[0]['participation'];
        $name = $userData[0]['first_name'] . ' ' . $userData[0]['last_name'];
        $message = "";

        if ($registration && !$participation) {

            $res = ParticipationModel::addParticipation($mealType, $userId[0]['id'], $date, $createdBy);

            if ($res) {
                $status = 2;
                $message = "Kasutaja lisatud";
            } else {
                $status = 9;
                $message = "Kasutaja lisamine ebaõnnestus";
            }
        }

        if ($registration && $participation) {
            $status = 2;
            $message = "Kasutaja juba sööma märgitud";
        }

        if (!$registration && $participation) {
            $status = 3;
            $message = "Kasutaja juba lisatud aga sööjaks märkimata";
        }

        return '{ "uid" : "' . $userData[0]['app_user'] . '", "status" : "' . $status . '", "name" : "' . $name . '", "message" : "' . $message . '" }';
    }

    public function postParticipationAppApiAdd() {
        global $config;

        // TODO: add some kind of verification (JWS token etc...)

        header('Content-Type: application/json; charset=utf-8');

        $rawData = file_get_contents('php://input');

        if (!Core_Util::isJson($rawData)) {
            http_response_code(400);
            return '{"error": "JSON sent is not valid"}';
        }

        $data = json_decode($rawData, true);

        try {
            $mealTypeId = $data['mealTypeId'];
            $cardNr = $data['cardNr'];
            $force = (bool)$data['force'];
        } catch (Exception $e) {
            // json didn't contain all required information
            return '{"error": "JSON query did not contain all required parameters!"}';
        }

        $userId = ParticipationModel::getUserIdByCardCode($cardNr);

        if (!$userId) {
            return '{"error": "Tundmatu kaart"}';
        }

        $userData = ParticipationModel::getCurrentParticipationByUserIdAndMealTypeId($userId[0]['id'], $mealTypeId);
        $registration = $userData[0]['registration'];
        $participation = $userData[0]['participation'];
        $today = Core_Date::now();
        $name = $userData[0]['first_name'] . ' ' . $userData[0]['last_name'];
        $json = '{"error": "Tundmatu viga!"}';

        if (!$registration && !$participation && !$force) {
            // kui registreeritud ei ole (ega juba kohalolu märgitud ja ei sunnita)
            error_log('ei ole registreeritud ega kohalolu märgitud (no force)');
        } else if (!$registration && !$participation && $force) {
            // kui registreeritud ei ole (ega juba kohalolu märgitud aga sunnitakse)
            error_log('ei ole registreeritud ega kohalolu märgitud (forced)');
        } else if ($registration && !$participation) {
            // kui registreeritud aga kohalolu pole märgitud
            error_log('registreeritud aga kohalolu pole märgitud');
            $res = ParticipationModel::addParticipation($mealTypeId, $userId[0]['id'], $today, $userId[0]['id']);
            error_log(print_r($res, true));

            if ($res) {
                $status = 2;
                $message = "Kasutaja lisatud";
                $participationData = ParticipationModel::getParticipationById($res);
                $json = '{
                    "idInDb": ' . $participationData[0]['id'] . ', 
                    "mealTypeId": ' . $participationData[0]['meal_type'] . ', 
                    "userId": ' . $participationData[0]['app_user'] . ',
                    "mealDate": "' . $participationData[0]['meal_date'] . '",
                    "name": "' . $name . '",
                    "error": ""}';

            } else {
                $status = 9;
                $message = "Kasutaja lisamine ebaõnnestus";
            }
        } else {
            // juba regatud
            error_log('juba regatud');
            $json = '{"error": "Juba registreeritud"}';
        }

        http_response_code(200);
        error_log($json);
        return $json;
    }

    public function postParticipationApiAddById()
    {
        global $config;

        $json = '';
        if (!$this->hasRole(['cook', 'admin']) || $config['PARTICIPATION_DISABLED']) {
            error_log('Puudub õigus kohalolu märkida (kas vale roll või kohalolu kontroll on välja lülitatud)');
            return '{}';
        }

        $post = $this->getApiPostData();

        if (empty($post['userId']) || empty($post['mealType'])) {
            error_log('Puudulikud andmed. (userId: ' . $post['userId'] . ' - mealType: ' . $post['mealType'] . ')');
            return '{"status" : 7, "message" : "Puudulikud andmed (köök)!"}';
        }

        $userData = ParticipationModel::getCurrentParticipationByUserIdAndMealTypeId($post['userId'], $post['mealType']);
        $add = true;

        if ($userData) {
            if ($userData[0]['participation'] > 0) {
                // remove
                $isSuccessful = ParticipationModel::removeParticipation(
                    $post['mealType'], $post['userId'], $post['mealDate'], $post['createdBy']);
                $add = false;
                $status = $userData[0]['registration'] == 0 ? 0 : 1;
            } else {
                // add
                $isSuccessful = ParticipationModel::addParticipation(
                    $post['mealType'], $post['userId'], $post['mealDate'], $post['createdBy']);
                $status = $userData[0]['registration'] == 0 ? 3 : 2;
            }

            if ($isSuccessful) {
                $json = '{"uid" : ' . $post['userId'] . ', "status" : ' . $status . ', "name" : "' .
                    $userData[0]['first_name'] . ' ' . $userData[0]['last_name'] .
                    '", "message" : "' . ($add ? 'Kasutaja lisatud (köök)!' : 'Kasutaja eemaldatud (köök)!') . '"}';
            } else {
                $json = '{"uid" : "' . $post['userId'] . '", "status" : "-1", "name" : "' .
                    $userData[0]['first_name'] . ' ' . $userData[0]['last_name'] .
                    '", "message" : "' . ($add ? 'Kasutaja lisamine ebaõnnestus (köök)!' : 'Kasutaja eemaldamine ebaõnnestus (köök)!') . '"}';
            }
        }

        header('Content-Type: application/json; charset=utf-8');
        return $json ?: '{ "status": "7", "message" : "Puudulikud andmed (köök)!" }';
    }

    public function getClosestMealTypeByTime(array $mealTypes)
    {
        $timestamp = strtotime(Core_Date::now('Y-m-d H:i:s'));
        $diff = null;
        $mealTypeId = null;

        foreach ($mealTypes as $mealType) {
            $mtTime = Core_Date::now() . ' ' . $mealType[MealType::MEAL_TIME];
            $currDiff = abs($timestamp - strtotime($mtTime));

            if (is_null($diff) || $currDiff < $diff) {
                $mealTypeId = $mealType['id'];
                $diff = $currDiff;
            }
        }

        return $mealTypeId;
    }

    /**
     * Get API data from POST parameters. If parameter doesn't exist, use empty string
     * @return array
     */
    public function getApiPostData(): array
    {
        return [
            'sCode' => Core_Util::param('sCode'),
            'userId' => Core_Util::param('userid'),
            'mealType' => Core_Util::param('mealtype'),
            'mealDate' => Core_Date::now(),
            'createdBy' => $this->currentUserId()];
    }
}