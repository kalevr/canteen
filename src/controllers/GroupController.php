<?php
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'GroupModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'UserModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'controllers' . DS . 'MenuController.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'AcademicYear.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Util.php';

class GroupController extends BaseController
{
    public function getGroups(): string
    {
        $this->authorized(['admin', 'observer']);

        $data['content'] = 'group/groups.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['create_url'] = '/' . $this->router->route('groups_create');
        $data['page'] = 'admin';

        $yearId = Core_Session::getVar('selected_year');

        $groups = GroupModel::getAllGroups($yearId);

        foreach ($groups as $key => $group) {
            $groups[$key]['edit_url'] = '/' . $this->router->route('groups_edit', [$group['id']]);
        }

        $data['groups'] = $groups;

        return twigRender($data);
    }

    public function getEdit(int $groupId): string
    {
        $this->authorized(['admin', 'observer']);

        $data = $this->getInitialData();

        // delete form session variables
        Core_Session::massUnsetVar(['group_name_error', 'starting_from_error', 'valid_until_error', 'teachers_error',
            'students_error', 'general_error', 'sort_order_error', 'group_name', 'starting_from', 'valid_until',
            'sort_order', 'teachers', 'students']);

        $data['edit'] = true;

        $firstAndLast = AcademicYear::getFirstAndLastDay(Core_Session::getVar('selected_year'));
        $data['first_date'] = $firstAndLast[0];
        $data['last_date'] = $firstAndLast[1];

        $group = [];
        $groupTeachers = [];
        $groupMembers = [];

        $groupData = GroupModel::getGroupWithMembers($groupId);

        if ($groupData && $groupData[0]) {
            // if following values are not present, set them
            $data['group_name'] = $data['group_name_error'] ? $data['group_name'] : $groupData[0]['group_name'];
            $data['starting_from'] = $data['starting_from_error'] ? $data['starting_from'] : $groupData[0]['starting_from'];
            $data['valid_until'] = $data['valid_until_error'] ? $data['valid_until'] : $groupData[0]['valid_until'];
            $data['sort_order'] = $data['sort_order_error'] ? $data['sort_order'] : $groupData[0]['sort_order'];
        }

        foreach ($groupData as $gData) {
            if ($gData['app_role'] == 4) {
                $groupTeachers[] = $gData;
            } elseif ($gData['app_role'] == 5) {
                $groupMembers[] = $gData;
            }
        }

        $data['group_id'] = $groupId;
        $data['group'] = $group;

        // if students/teachers array is empty, fill it with data from DB (otherwise use data from session)
        $data['teachers'] = $data['teachers'] ?: $groupTeachers;
        $data['students'] = $data['students'] ?: $groupMembers;

        $data['users_api_url'] = '/' . $this->router->route('api_users_search');

        return twigRender($data);
    }

    public function postCreateEdit()
    {
        $this->authorized(['admin', 'observer']);

        // get form data
        $groupId = Core_Util::param('group_id');
        $groupName = Core_Util::param('group_name');
        $startingFrom = Core_Util::param('starting_from');
        $validUntil = Core_Util::param('valid_until');
        $sortOrder = Core_Util::param('sort_order') ?: 9999999;
        $teachers = Core_Util::param('teachers');
        $students = Core_Util::param('students');
        $teachersData = [];
        $studentsData = [];

        // redirection rules (edit or create)
        $routeKey = $groupId ? 'groups_edit' : 'groups_create';
        $routeVar = $groupId ? [$groupId] : [];

        if ($teachers) {
            $teachersData = UserModel::getUsersById($teachers);
        } else {
            $teachers = [];
        }

        if ($students) {
            $studentsData = UserModel::getUsersById($students);
        } else {
            $students = [];
        }

        // validate form data
        $isValidForm = $this->isGroupDataValid($groupName, $startingFrom, $validUntil,
            count($teachers) == count($teachersData), count($students) == count($studentsData));
        if (!$isValidForm) {
            $this->redirect($routeKey, $routeVar, ['group_name' => $groupName, 'starting_from' => $startingFrom,
                'valid_until' => $validUntil, 'sort_order' => $sortOrder, 'teachers' => $teachers, 'students' => $students]);
        }

        // if groupID is set, then we are editing, if not, we are creating new
        $isSuccessful = false;
        if ($groupId) {
            $res = GroupModel::updateGroup($groupId, $groupName, $startingFrom, $validUntil, $sortOrder);

            if ($res) {
                $usersAlreadyInGroup = GroupModel::getIdsOfGroupMembers($groupId);
                $this->updateGroupMembers($groupId, $teachersData, $studentsData, $usersAlreadyInGroup);
                $isSuccessful = true;
            }
        } else {
            $yearId = Core_Session::getVar('selected_year');
            $res = GroupModel::insertGroup($yearId, $groupName, $startingFrom, $validUntil, $sortOrder, $this->currentUserId());

            if ($res) {
                $usersAlreadyInGroup = [];
                $this->updateGroupMembers($res, $teachersData, $studentsData, $usersAlreadyInGroup);
                $isSuccessful = true;
            }
        }

        if ($isSuccessful) {
            $this->redirect('groups');
        } else {
            error_log('Error: saving group failed - groupId: ' . $groupId);
            $this->redirect($routeKey, $routeVar, ['group_name' => $groupName,
                'starting_from' => $startingFrom, 'valid_until' => $validUntil,
                'error' => 'Grupi muudatuste lisamine andmebaasi ebaõnnestus!']);
        }
    }

    public function getCreate(): string
    {
        $this->authorized(['admin', 'observer']);

        $data = $this->getInitialData();
        $data['edit'] = false;

        // delete form session variables
        Core_Session::massUnsetVar(['group_name_error', 'starting_from_error', 'valid_until_error', 'teachers_error',
            'students_error', 'general_error', 'sort_order_error', 'group_name', 'starting_from', 'valid_until',
            'sort_order', 'teachers', 'students']);

        $firstAndLast = AcademicYear::getFirstAndLastDay(Core_Session::getVar('selected_year'));
        $data['first_date'] = $firstAndLast[0];
        $data['last_date'] = $firstAndLast[1];
        $data['users_api_url'] = '/' . $this->router->route('api_users_search');

        return twigRender($data);
    }

    public function getGroupsApi(): string
    {
        if (!$this->hasRole(['admin', 'observer'])) {
            return '{}';
        }

        $search = Core_Util::param('q');
        $currentYear = Core_Session::getVar('selected_year');

        $groups = GroupModel::searchGroups($search, $currentYear ?: 0);

        return json_encode($groups);
    }

    private function getInitialData(): array
    {
        $data['content'] = 'group/edit.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['post_url'] = '/' . $this->router->route('groups_edit_post');
        $data['page'] = 'admin';

        // get data from session
        $data['group_name_error'] = Core_Session::getVar('group_name_error');
        $data['starting_from_error'] = Core_Session::getVar('starting_from_error');
        $data['valid_until_error'] = Core_Session::getVar('valid_until_error');
        $data['sort_order_error'] = Core_Session::getVar('sort_order_error');
        $data['general_error'] = Core_Session::getVar('general_error');
        $data['teachers_error'] = Core_Session::getVar('teachers_error');
        $data['students_error'] = Core_Session::getVar('students_error');
        $data['group_name'] = Core_Session::getVar('group_name');
        $data['starting_from'] = Core_Session::getVar('starting_from');
        $data['valid_until'] = Core_Session::getVar('valid_until');
        $data['sort_order'] = Core_Session::getVar('sort_order');

        // get teachers and students from session variables
        $teachers = Core_Session::getVar('teachers');
        $students = Core_Session::getVar('students');
        $data['teachers'] = $teachers ? UserModel::getUsersById($teachers) : "";
        $data['students'] = $students ? UserModel::getUsersById($students) : "";

        return $data;
    }

    /**
     * Check if date is in range (between min and max). If not, set session variable with error message.
     * @param string $date - date to check
     * @param string $min - smallest date allowed
     * @param string $max - biggest date allowed
     * @param string $varKey - session variable key (name).
     * @param bool $nullable - can date be empty/null?
     * @return bool - returns true if date is not empty and is between min and max.
     * @throws Exception
     */
    private function isDateInRange(string $date, string $min, string $max, string $varKey, bool $nullable = false): bool
    {
        $invalid = false;

        // if date exists, let's validate. if it doesnt (and is nullable), return true
        if ($date) {
            try {
                $date = Core_Date::get($date, 'Y-m-d');
            } catch (Exception) {
                $invalid = true;
            }
        } elseif ($nullable) {
            return true;
        }

        if (empty($date) || $invalid || $date < $min || $date > $max) {
            Core_Session::setVar($varKey, 'Kuupäev peab olema vahemikus '
                . Core_Date::get($min, 'd.m.Y') . ' - ' . Core_Date::get($max, 'd.m.Y'));

            return false;
        }

        return true;
    }


    /**
     * Check if groupName, startingFrom and validUntil have valid values. Also add error msg for teachers/students
     * if needed
     *
     * @param string $groupName
     * @param string $startingFrom
     * @param string $validUntil
     * @param bool $teachersValid - set as 'true' if
     * @param bool $studentsValid
     * @return bool
     */
    private function isGroupDataValid(string $groupName, string $startingFrom, string $validUntil,
                                      bool   $teachersValid, bool $studentsValid): bool
    {
        $valid = true;

        // check if groupName is set
        if (empty($groupName)) {
            Core_Session::setVar('group_name_error', 'Grupi nimi on kohustuslik');
            $valid = false;
        }

        $firstAndLast = AcademicYear::getFirstAndLastDay(Core_Session::getVar('selected_year'));

        // check if startingFrom/validUntil are valid dates (set error message if not)
        try {
            $startingFromValid = $this->isDateInRange($startingFrom, $firstAndLast[0], $firstAndLast[1], 'starting_from_error');
            $validUntilValid = $this->isDateInRange($validUntil, $firstAndLast[0], $firstAndLast[1], 'valid_until_error', true);
        } catch (Exception) {
            $startingFromValid = false;
            $validUntilValid = false;
        }


        if (!$startingFromValid || !$validUntilValid) {
            $valid = false;
        }

        if (!$teachersValid) {
            Core_Session::setVar('teatchers_error', 'Valitud kasutajate hulgas on kasutaja, keda süsteemist ei leitud!');
        }

        if (!$studentsValid) {
            Core_Session::setVar('students_error', 'Valitud kasutajate hulgas on kasutaja, keda süsteemist ei leitud!');
        }

        return $valid;
    }

    private function updateGroupMembers(int $groupId, array $teachers, array $students, array $usersAlreadyInGroup)
    {
        $existingTeachers = [];
        $existingStudents = [];
        $allTeachers = [];
        $allStudents = [];

        foreach ($usersAlreadyInGroup as $user) {
            if ($user['app_role'] == 4) {
                $existingTeachers[$user['id']]['user_group_id'] = $user['user_group_id'];
            } elseif ($user['app_role'] == 5) {
                $existingStudents[$user['id']]['user_group_id'] = $user['user_group_id'];
            }
        }

        foreach ($teachers as $teacher) {
            if (!in_array($teacher['id'], array_keys($existingTeachers))) {
                // add teacher (role 4)
                GroupModel::addUserToGroup($groupId, $teacher['id'], 4, $this->currentUserId());
            }
            $allTeachers[$teacher['id']] = 4;
        }

        foreach ($students as $student) {
            if (!in_array($student['id'], array_keys($existingStudents))) {
                // add student (role 5)
                GroupModel::addUserToGroup($groupId, $student['id'], 5, $this->currentUserId());
            }
            $allStudents[$student['id']] = 5;
        }

        foreach (array_diff_key($existingTeachers, $allTeachers) as $key => $val) {
            // remove teacher
            GroupModel::removeUserFromGroup($val['user_group_id'], $key, 4, $this->currentUserId());
        }

        foreach (array_diff_key($existingStudents, $allStudents) as $key => $val) {
            // remove student
            GroupModel::removeUserFromGroup($val['user_group_id'], $key, 5, $this->currentUserId());
        }
    }
}