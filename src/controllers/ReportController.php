<?php
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'ReportModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealTypeModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'AcademicYear.php';

class ReportController extends BaseController
{
    public function getRandomGeneralReport() {
        $mealTypeId = ReportModel::getRandomMealTypeId();
        $date = Core_Date::now();

        if (!$mealTypeId) {
            $this->redirect('error', [], ['error_msg' => 'Ühtegi söögikorda ei leitud!']);
        }

        return $this->getGeneralReportForMealTypePerMonth($mealTypeId[0]['id'], $date);
    }

    public function getGeneralReportForMealTypePerMonth(int $mealTypeId, string $date = '') {
        global $config;

        $this->authorized(['admin', 'observer', 'accountant']);

        $date = $date ?: Core_Date::now();
        $meals = [];

        try {
            $from = Core_Date::firstDayOfMonth($date);
            $until = Core_Date::lastDayOfMonth($date);
            $nextMonth = Core_Date::add($until, 'P1D')->format('Y-m-d');
            $previousMonth = Core_Date::sub($from, 'P1D')->format('Y-m-d');
        } catch (Exception) {
            $this->redirect('error', [], ['error_msg' => 'Viga: Kuupäevaga paistab mingi probleem olevat!']);
        }
        $currentMonth = false;
        if (Core_Date::now('Y-m') == Core_Date::get($date, 'Y-m')) {
            $currentMonth = true;
        }

        $mealsInDb = ReportModel::getRegistrationAndParticipationSummaryForMealType($mealTypeId, $from, $until, Core_Session::getVar('selected_year'), $currentMonth);

        foreach ($mealsInDb as $mealInDb) {
            $groupId = $mealInDb['id'];

            if (!array_key_exists($groupId, $meals)) {
                $meals[$groupId]['group_name'] = $mealInDb['group_name'];
                $meals[$groupId]['total_reg'] = 0;
                $meals[$groupId]['total_part'] = 0;
                $meals[$groupId]['total_absent'] = 0;
            }
            $part = $mealInDb['participations'];
            $regInPast = array_key_exists('reg_in_past', $mealInDb) ? $mealInDb['reg_in_past'] : $mealInDb['registrations'];

            $meals[$groupId]['users'][] = [
                'full_name' => $mealInDb['first_name'] . ' ' . $mealInDb['last_name'],
                'registrations' => $mealInDb['registrations'],
                'participations' => $part,
                'absent' => $regInPast - $part
            ];

            $meals[$groupId]['total_reg'] += $mealInDb['registrations'];
            $meals[$groupId]['total_part'] += $mealInDb['participations'];
            $meals[$groupId]['total_absent'] += $regInPast - $part;
        }

        // find all mealTypes for sidemenu
        $yearId = Core_Session::getVar('selected_year');
        $mealTypesInDb = MealTypeModel::getAllMealTypes($yearId);
        $data['kitchen_views'] = ReportModel::getMealTotalsByKitchenViews($mealTypeId, $from, $until, $yearId);
        $mealTypes = [];

        foreach ($mealTypesInDb as $mealTypeInDb) {
            $mealTypes[] = ['name' => $mealTypeInDb['meal_type_name'], 'url' => '/' . $this->router->route('general_report', [$mealTypeInDb['id'], $date])];
            if ($mealTypeId == $mealTypeInDb['id']) {
                $data['selected_mt_name'] = $mealTypeInDb['meal_type_name'];
            }
        }

        $data['participation_enabled'] = !$config['PARTICIPATION_DISABLED'];
        $data['month_year'] = Core_Date::getMonthName($date) . ' ' . Core_Date::get($date, 'Y');
        $data['link_next'] = '/' . $this->router->route('general_report', [$mealTypeId, $nextMonth]);
        $data['link_previous'] = '/' . $this->router->route('general_report', [$mealTypeId, $previousMonth]);
        $data['groups'] = $meals;
        $data['meal_types'] = $mealTypes;
        $data['content'] = 'report/general.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['page'] = 'reports';

        return twigRender($data);
    }
}