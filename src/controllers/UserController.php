<?php
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'UserModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'AuthModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'GroupModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'Logger.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'LogRow.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'controllers' . DS . 'MenuController.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Util.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'AcademicYear.php';

class UserController extends BaseController
{
    public function getRoleUsers(string $roleName = 'admin'): string
    {
        $this->authorized(['admin', 'observer']);

        if (!in_array($roleName, ['admin', 'observer', 'accountant', 'cook', 'homeroom-teacher', 'unknown'])) {
            error_log('Role "' . $roleName . '" does not exist!');
            $this->redirect('error', [], ['error_msg' => 'Sellist rolli ei ole olemas!']);
        }

        $data['content'] = 'user/users.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['left_menu'] = 'user/left_menu.twig';
        $data['create_url'] = '/' . $this->router->route('users_create');
        $currentYear = Core_Session::getVar('selected_year');

        if ($roleName == 'unknown') {
            $users = UserModel::getUsersWithoutRoleOrGroup($currentYear);
        } else {
            $users = UserModel::getUsersByRoleName($roleName, $currentYear);
        }

        $myRoles = UserModel::getUserRoles($this->currentUserId());

        $data['users'] = $this->addEditDeleteLinksToUsers($users, $myRoles, $roleName);
        $data['users_api_url'] = '/' . $this->router->route('api_users_search');
        $data['page'] = 'admin';

        $data['role_links'] = $this->getRoleLinks();
        $data['group_links'] = $this->getGroupLinks();

        return twigRender($data);
    }

    public function getGroupUsers(int $groupId): string
    {
        $this->authorized(['admin', 'observer']);

        if (!$groupId || $groupId < 1) {
            error_log('Invalid group ID (' . $groupId . ').');
            $this->redirect('error', [], ['error_msg' => 'Vigane grupi ID!']);
        }

        $data['content'] = 'user/users.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['left_menu'] = 'user/left_menu.twig';
        $data['create_url'] = '/' . $this->router->route('users_create');

        $users = UserModel::getUsersByGroupId($groupId);
        $myRoles = UserModel::getUserRoles($this->currentUserId());

        $data['users'] = $this->addEditDeleteLinksToUsers($users, $myRoles, '');
        $data['users_api_url'] = '/' . $this->router->route('api_users_search');
        $data['page'] = 'admin';

        $data['role_links'] = $this->getRoleLinks();
        $data['group_links'] = $this->getGroupLinks();

        return twigRender($data);
    }

    public function getEdit(int $id): string
    {
        $this->authorized(['admin', 'observer']);

        // TODO: if user being edited is admin, but user editing is not, redirect

        $data = [];
        $user = UserModel::getUser($id);

        if ($user && $user[0]) {
            $data['user'] = $user[0];
        } else {
            $this->redirect('error', [], ['error_msg' => 'Kasutajat ei leitud!']);
        }
        // TODO: add "Activate account" button for deleted accounts
        if ($user[0]['is_deleted']) {
            $data['message'] = 'Konto on kustutatud!';
            $data['del_restore_url'] = '/' . $this->router->route('users_restore', [$user[0]['id']]);
        } else {
            $data['del_restore_url'] = '/' . $this->router->route('users_delete', [$user[0]['id']]);
        }

        $data = $this->getInitialDataForUser($data, false);

        $data['role_links'] = $this->getRoleLinks();
        $data['group_links'] = $this->getGroupLinks();
        $data['create_url'] = '/' . $this->router->route('users_create');
        $data['page'] = 'admin';

        return twigRender($data);
    }

    public function postEdit()
    {
        $this->authorized(['admin', 'observer']);

        // TODO: if user being edited is admin, but user editing is not, redirect

        $data = $this->getPostData(true);

        $userId = $data['id'];
        $orig = UserModel::getUser($userId);
        $logger = new Logger();
        $tblName = 'app_user';
        $ins = [];

        if ($orig && $orig[0]) {
            if ($data['passwd']) {
                $data['passwd'] = AuthModel::encryptPassword($data['passwd']);
            }

            foreach ($orig[0] as $key => $value) {
                if (array_key_exists($key, $data) && $data[$key] != $value) {
                    if ($key != 'user_role') {
                        $ins[$key] = $data[$key];
                        $logger->addLogRow(new LogRow($tblName, $userId, $key, $orig[0][$key], $data[$key], $this->currentUserId()));
                    }
                }
            }
        }

        if ($data['passwd']) {
            $ins['passwd'] = $data['passwd'];
            $logger->addLogRow(new LogRow($tblName, $userId, 'passwd', 'REDACTED', 'REDACTED', $this->currentUserId()));
        }

        $res = UserModel::editUser('app_user', $ins, $userId);

        if ($res) {
            if ($orig[0]['user_role'] != $data['user_role']) {
                if ($orig[0]['user_role'] > 0) {
                    // remove old user_role
                    $isUserRoleRemoved = UserModel::removeUserRole($orig[0]['id'], $orig[0]['user_role'], $this->currentUserId());

                    if (!$isUserRoleRemoved) {
                        $this->redirect('error', [], ['error_msg' =>
                            'Kasutaja muutmine õnnestus aga eriõiguste muutmine ei õnnestunud (vana rolli eemaldamine)!']);
                    }
                }

                if ($data['user_role'] > 0) {
                    // add new user_role
                    $isUserRoleSet = UserModel::setUserRole($data['id'], $data['user_role'], $this->currentUserId());

                    if (!$isUserRoleSet) {
                        $this->redirect('error', [], ['error_msg' =>
                            'Kasutaja muutmine õnnestus aga eriõiguste muutmine ei õnnestunud (uue rolli lisamine)!']);
                    }
                }
            }

            $logger->save();
            $this->redirect('users_by_role');
        } else {
            error_log('Error: Kasutaja muutmine ebaõnnestus (id: ' . $data['id'] . ')');
        }
    }

    public function getDelete(int $id)
    {
        $this->authorized(['admin', 'observer']);

        // TODO: if user being deleted is admin, but user deleting is not, redirect

        $res = UserModel::markUserAsDeleted($id, $this->currentUserId());

        if ($res) {
            $this->redirect('users_by_role');
        } else {
            error_log("Error: Kasutaja kustutamine ebaõnnestus");
            $this->redirect('error',[],['error_msg' => 'Kasutaja kustutamine ebaõnnestus!']);
        }
    }

    public function getRestore(int $id) {
        $this->authorized(['admin']);
        error_log('Taastan kasutajat: ' . $id);

        // TODO: Should log account restore/delete events in app_log table. (currently can't see who restored and when)
        // TODO: if user being restored is admin, but user restoring is not, redirect

        $res = UserModel::markUserAsNotDeleted($id);

        if ($res) {
            error_log('Kasutaja ' . $id . 'taastatud!');
            $this->redirect('users_edit', [$id], ['message' => 'Kasutaja taastatud!']);
        } else {
            error_log("Error: Kasutaja taastamine ebaõnnestus");
            $this->redirect('users_edit', [$id], ['message' => 'Kasutaja taastamine ebaõnnestus!']);
        }
    }

    public function getCreate(): string
    {
        $this->authorized(['admin', 'observer']);

        // TODO: if user being created is admin, but user creating is not, redirect

        $data = $this->getInitialDataForUser();

        $data['role_links'] = $this->getRoleLinks();
        $data['group_links'] = $this->getGroupLinks();

        return twigRender($data);
    }

    public function postCreate()
    {
        $this->authorized(['admin', 'observer']);

        // TODO: if user being created is admin, but user creating is not, redirect

        $data = $this->getPostData();
        $error = [];

        if (!$data['email']) {
            $error[] = 'Email on kohustuslik!';
        }

        if (!$data['passwd']) {
            $error[] = 'Parool ei saa olla tühi!';
        }


        if (!$error) {
            $data['passwd'] = AuthModel::encryptPassword($data['passwd']);
            $data['created_by'] = $this->currentUserId();
            $res = UserModel::createUser($data);

            if ($res && $res > 0) {
                $roleId = $data['user_role'];
                if ($roleId > 0) {
                    $isUserRoleSet = UserModel::setUserRole($res, $roleId, $this->currentUserId());

                    if (!$isUserRoleSet) {
                        $this->redirect('error', [], ['error_msg' => 'Kasutaja loomine õnnestus aga eriõiguste määramine ei õnnestunud!']);
                    }
                }
                $this->redirect('users_by_role');
            } else {
                $error[] = "User creation failed (creator ID {$data['created_by']} user -> {$data['first_name']} {$data['last_name']}";
                error_log(__METHOD__ . ' - ' . implode(', ', $error));
            }
        }

        $this->redirect('users_create', [],
            [
                'errors' => implode('<br>', $error),
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'comments' => $data['comments'],
                'locked' => $data['locked'],
                'user_role' => $data['user_role'] ?: -1
            ]);
    }

    public function getUsersApi(): string
    {
        if (!$this->hasRole(['admin', 'observer'])) {
            return '{}';
        }

        $search = Core_Util::param('q');
        $onlyActive = Core_Util::param('all') == '';

        $users = UserModel::searchUsers($search, $onlyActive);

        return json_encode($users ?: '{}');
    }

    private function getPostData(bool $isEdit = false): array
    {
        if ($isEdit) {
            $data['id'] = Core_Util::param('id');
        }
        $data['first_name'] = Core_Util::param('first_name');
        $data['last_name'] = Core_Util::param('last_name');
        $data['email'] = Core_Util::param('email');
        $data['passwd'] = Core_Util::param('password');
        $data['comments'] = Core_Util::param('comments');
        $data['locked'] = Core_Util::param('locked') ?: 0;
        $data['user_role'] = Core_Util::param('user_role') ?: -1;

        unset($_POST['passwd']);

        return $data;
    }

    private function getInitialDataForUser(array $data = [], bool $isNew = true): array
    {
        $data['content'] = $isNew ? 'user/create.twig' : 'user/edit.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = 'menu.twig';
        $data['links'] = $this->menu['links'];
        $data['left_menu'] = 'user/left_menu.twig';

        $data['post_url'] = '/' . $this->router->route($isNew ? 'users_create_post' : 'users_edit_post');
        $data['users_api_url'] = '/' . $this->router->route('api_users_search');
        $data['page'] = 'admin';

        if ($isNew) {
            $data['user'] = [];
        }

        $firstName = Core_Session::getVar('first_name');
        $lastName = Core_Session::getVar('last_name');
        $email = Core_Session::getVar('email');
        $comments = Core_Session::getVar('comments');
        $locked = Core_Session::getVar('locked');
        $roleId = Core_Session::getVar('user_role');
        $message = Core_Session::getVar('message');
        $data['errors'] = Core_Session::getVar('errors');

        if ($firstName) {
            $data['user']['first_name'] = $firstName;
        }

        if ($lastName) {
            $data['user']['last_name'] = $lastName;
        }

        if ($email) {
            $data['user']['email'] = $email;
        }

        if ($comments) {
            $data['user']['comments'] = $comments;
        }

        if ($locked) {
            $data['user']['locked'] = $locked;
        }

        if ($roleId) {
            $data['user_role'] = $roleId;
        } else {
            $data['user_role'] = $isNew ? '-1' : $data['user']['user_role'];
        }

        if ($message) {
            $data['message'] = Core_Session::getVar('message');
        }

        $data['roles'] = UserModel::getAllSpecialRoles();

        Core_Session::massUnsetVar(['first_name', 'last_name', 'email', 'comments', 'locked', 'error', 'user_role',
            'errors', 'message']);

        return $data;
    }

    private function getRoleLinks(): array
    {
        $roles = [];
        $noGroupOrRole = 0;
        $rolesInDb = UserModel::getUserCountsByRole(Core_Session::getVar('selected_year'));

        foreach ($rolesInDb as $role) {
            $roles[$role['role_code']] = $role['role_count'];
            $noGroupOrRole = $role['no_role_group'];
        }
        return [
            ['name' => 'Admin', 'url' => '/' . $this->router->route('users_by_role', ['admin']),
                'nr_of_users' => $roles['admin']],
            ['name' => 'Vaatleja', 'url' => '/' . $this->router->route('users_by_role', ['observer']),
                'nr_of_users' => $roles['observer']],
            ['name' => 'Raamatupidaja', 'url' => '/' . $this->router->route('users_by_role', ['accountant']),
                'nr_of_users' => $roles['accountant']],
            ['name' => 'Kokk', 'url' => '/' . $this->router->route('users_by_role', ['cook']),
                'nr_of_users' => $roles['cook']],
            ['name' => 'Klassijuhataja', 'url' => '/' . $this->router->route('users_by_role', ['homeroom-teacher']),
                'nr_of_users' => $roles['homeroom-teacher']],
            ['name' => 'Grupita/Rollita', 'url' => '/' . $this->router->route('users_by_role', ['unknown']),
                'nr_of_users' => $noGroupOrRole]
        ];
    }

    private function getGroupLinks(): array
    {
        $groupsInDb = GroupModel::getAllGroups(AcademicYear::getCurrent(), false);
        $groups = [];

        foreach ($groupsInDb as $groupInDb) {
            $groups[] = ['name' => $groupInDb['group_name'], 'url' => '/' . $this->router->route('users_by_group',
                    [$groupInDb['id']]), 'nr_of_eaters' => $groupInDb['nr_of_eaters']];
        }

        return $groups;
    }

    private function addEditDeleteLinksToUsers(array $users, array $myRoles, string $roleName): array
    {
        if ($users) {
            foreach ($users as $key => $user) {
                // don't show edit/delete links if user group is 'admin' but user is not in that group
                if ($roleName != 'admin' || in_array('admin', $myRoles)) {
                    $users[$key]['edit_url'] = '/' . $this->router->route('users_edit', [$user['id']]);
                    $users[$key]['delete_url'] = '/' . $this->router->route('users_delete', [$user['id']]);
                } else {
                    $users[$key]['edit_url'] = '';
                    $users[$key]['delete_url'] = '';
                }

                $users[$key]['last_login'] = $users[$key]['last_login'] ? Core_Date::get($users[$key]['last_login'], 'd.m.Y H:i:s') : ' - ';
            }
        }

        return $users;
    }
}