<?php
require __DIR__ . '/../../preventDirectAccess.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'CardModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'UserModel.php';

class CardController extends BaseController
{
    public function getCards(int $page, string $search = "")
    {
        $this->authorized(['admin', 'observer']);

        if (!Core_Util::isPositiveInteger($page)) {
            $page = 1;
        }

        $data['content'] = 'card/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['create_url'] = '/' . $this->router->route('cards_create');

        $totalNrOfCards = CardModel::getNrOfCards($search);
        $cards = CardModel::getCardsWithPagination($page, CardModel::RESULTS_PER_PAGE, $search);

        foreach ($cards as $key => $card) {
            $cards[$key]['edit_url'] = '/' . $this->router->route('cards_edit', [$card['id']]);
            $cards[$key]['delete_url'] = '/' . $this->router->route('cards_delete', [$card['id']]);
        }

        $data['cards'] = $cards;
        $data['current_page'] = $page;
        $data['nr_of_pages'] = ceil($totalNrOfCards / CardModel::RESULTS_PER_PAGE);
        $data['cards_api_url'] = '/' . $this->router->route('api_cards_search');
        $data['page'] = 'admin';

        return twigRender($data);
    }

    public function getEdit(int $cardId): string
    {
        $this->authorized(['admin', 'observer']);

        $data = [];
        $card = CardModel::getCardById($cardId);

        if ($card && $card[0]) {
            $data['card'] = $card[0];
        }

        $data = $this->getInitialDataForCard($data, false);

        return twigRender($data);
    }

    public function getCreate()
    {
        $this->authorized(['admin', 'observer']);

        $data = $this->getInitialDataForCard();

        return twigRender($data);
    }

    public function postCreateEdit()
    {
        $this->authorized(['admin', 'observer']);

        // get form data
        $cardId = Core_Util::param('id');
        $visibleNr = Core_Util::param('visible_nr');
        $scannableNr = Core_Util::param('scannable_nr');
        $userId = Core_Util::param('card_user_id');
        $from = Core_Util::param('starting_from');
        $until = Core_Util::param('valid_until');

        // redirection rules (edit or create)
        $routeKey = $cardId ? 'cards_edit' : 'cards_create';
        $routeVar = $cardId ? [$cardId] : [];

        // validate form data
        $isValidForm = $this->isCardDataValid($scannableNr, $userId, $from, $until);

        if (!$isValidForm) {
            $this->redirect($routeKey, $routeVar, ['visible_nr' => $visibleNr, 'scannable_nr' => $scannableNr,
                'card_user_id' => $userId, 'starting_from' => $from, 'valid_until' => $until]);
        }

        $isSuccessful = false;
        // if cardId is set, then we are editing, if not, we are creating new
        if ($cardId) {
            $res = CardModel::updateCard($cardId, $visibleNr, $userId, $scannableNr, $from, $until);

            if ($res) {
                $isSuccessful = true;
            }
        } else {

            $res = CardModel::insertCard($visibleNr, $scannableNr, $userId, $from, $until, $this->currentUserId());

            if ($res) {
                $isSuccessful = true;
            }
        }

        if ($isSuccessful) {
            $this->redirect('cards', [1]);
        } else {
            $this->redirect($routeKey, $routeVar, ['visible_nr' => $visibleNr, 'scannable_nr' => $scannableNr,
                'card_user_id' => $userId, 'starting_from' => $from, 'valid_until' => $until, 'general_error' => 'Kaardi salvestamine ebaõnnestus!']);
        }
    }

    private function getInitialDataForCard(array $data = [], bool $isNew = true): array
    {
        $data['content'] = 'card/edit.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['post_url'] = '/' . $this->router->route('cards_edit_post');
        $data['page'] = 'admin';

        if ($isNew) {
            $data['card'] = [];
        }

        // get data from session
        $visibleNr = Core_Session::getVar('visible_nr');
        $scannableNr = Core_Session::getVar('scannable_nr');
        $userId = Core_Session::getVar('card_user_id');
        $firstName = array_key_exists('first_name', $data['card']) ? $data['card']['first_name'] : '';
        $lastName = array_key_exists('last_name', $data['card']) ? $data['card']['last_name'] : '';
        $from = Core_Session::getVar('starting_from');
        $until = Core_Session::getVar('valid_until');
        $cardNrError = Core_Session::getVar('scannable_nr_error');
        $fromError = Core_Session::getVar('starting_from_error');
        $data['users_api_url'] = '/' . $this->router->route('api_users_search');

        if ($visibleNr) {
            $data['card']['visible_nr'] = $visibleNr;
        }

        if ($scannableNr) {
            $data['card']['scannable_nr'] = $scannableNr;
        }

        if ($userId) {
            $data['card']['app_user'] = $userId;
        }

        if ($from) {
            $data['card']['starting_from'] = $from;
        } elseif (!array_key_exists('starting_from', $data['card']) || !$data['card']['starting_from']) {
            $data['card']['starting_from'] = Core_Date::now();
        }

        // does this work if I want to remove 'until' date?
        if ($until) {
            $data['card']['valid_until'] = $until;
        }

        if ($cardNrError) {
            $data['scannable_nr_error'] = $cardNrError;
        }

        if ($fromError) {
            $data['starting_from_error'] = $fromError;
        }

        if (array_key_exists('app_user', $data['card']) && ($firstName || $lastName)) {
            $data['users'][] = ['id' => $data['card']['app_user'], 'user_name' => $firstName . ' ' . $lastName];
        } elseif ($userId) {
            $user = UserModel::getUser($userId);
            if ($user) {
                $data['users'][] = ['id' => $user[0]['id'], 'user_name' => $user[0]['first_name'] . ' ' . $user[0]['last_name']];
            }
        }

        Core_Session::massUnsetVar(['visible_nr', 'scannable_nr', 'card_user_id', 'starting_from', 'valid_until', 'card_nr_error',
            'scannable_nr_error', 'starting_from_error', 'general_error']);

        $data['edit'] = !$isNew;

        return $data;
    }

    public function isCardDataValid(string $scannableNr, int $userId, string $from, string $until, int $cardId = null)
    {
        $isValid = true;

        if (!$scannableNr) {
            $isValid = false;
            Core_Session::setVar('scannable_nr_error', 'Kaardi number on kohustuslik!');
        } else {
            // check if the scannableNr is unique (don't count deleted cards).
            $duplicates = CardModel::getCardByScannableNr($scannableNr);

            if ($duplicates && $duplicates[0]['id'] != $cardId) {
                Core_Session::setVar('scannable_nr_error', 'VIGA: Sellise numbriga kaart on juba olemas (' .
                    $duplicates[0]['visible_nr'] . ' - ' . $duplicates[0]['first_name'] . ' ' . $duplicates[0]['last_name'] . ')!');
                $isValid = false;
            }
        }

        if (!$from) {
            $isValid = false;
            Core_Session::setVar('starting_from_error', 'Algus kuupäev on kohustuslik!');
        } else {
            try {
                Core_Date::get($from);
            } catch (Exception $e) {
                $isValid = false;
                Core_Session::setVar('starting_from_error', 'Algus kuupäev ei ole sobiv!');
            }
        }

        if ($until) {
            try {
                Core_Date::get($until);
            } catch (Exception $e) {
                $isValid = false;
                Core_Session::setVar('valid_until_error', 'Kuni kuupäev ei ole sobiv!');
            }
        }

        if ($userId) {
            $user = UserModel::getUser($userId);

            if (!$user) {
                $isValid = false;
                Core_Session::setVar('card_user_id_error', 'Sellist kasutajat ei eksisteeri!');
            }
        } else {
            $isValid = false;
            Core_Session::setVar('card_user_id_error', 'Iga kaartiga peab olema seotud kasutaja!');
        }

        return $isValid;
    }

    public function getCardsApi(): string
    {
        if (!$this->hasRole(['admin', 'observer'])) {
            return '{}';
        }

        $search = Core_Util::param('q');
        $page = Core_Util::param('page');

        $totalNrOfCards = CardModel::getNrOfCards($search);
        $nrOfPages = $totalNrOfCards / CardModel::RESULTS_PER_PAGE;

        if (!$totalNrOfCards) {
            return '{}';
        }

        if (!Core_Util::isPositiveInteger($page) || $nrOfPages <= $page) {
            $page = 1;
        }


        $cards = CardModel::getCardsWithPagination($page, CardModel::RESULTS_PER_PAGE, $search);
        $resultObj = ["current_page" => $page, "nr_of_pages" => $nrOfPages, "cards" => []];

        foreach ($cards as $card) {
            $resultObj['cards'][] = $card;
        }

        return json_encode($resultObj);
    }

    public function getDelete(int $id) {
        $this->authorized(['admin', 'observer']);

        $res = CardModel::markCardAsDeleted($id, $this->currentUserId());

        if ($res) {
            $this->redirect('cards', [1]);
        }
    }
}