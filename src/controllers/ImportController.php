<?php
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'GroupModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'ImportModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'AuthModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'UserModel.php';

class ImportController extends BaseController
{
    public function getImport()
    {
        $this->authorized(['admin']);

        $data['csv_import'] = Core_Session::getVar('csv_import') ?: '';
        $data['errors'] = Core_Session::getVar('errors') ?: '';
        Core_Session::massUnsetVar(['csv_import', 'errors']);

        $data['template_url'] = '/' . $this->router->route('import_download_template');

        $data['content'] = 'import/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['post_url'] = '/' . $this->router->route('import_post');
        $data['page'] = 'admin';

        return twigRender($data);
    }

    public function postImport()
    {
        $input = Core_Util::param('csv_import');
        // TODO: Add some kind of verification / escape so user can't enter bs...
        $users = [];
        $rows = explode(PHP_EOL, $input);
        $errors = [];
        $groupNumbers = [];
        $rowNr = 1;

        if ($rows) {
            foreach ($rows as $row) {
                if ($row != 'firstname,lastname,email,password,comment,group_nr') {
                    $rowArr = explode(',', $row);

                    if (count($rowArr) != 6) {
                        $errors[] = 'Viga real ' . $rowNr . '. Real peab olema 6 väärtust (5 koma). Leiti ainult ' .
                            count($rowArr) . ' väärtust!';;
                        continue;
                    }

                    $firstName = trim($rowArr[0]);
                    $lastName = trim($rowArr[1]);
                    $email = trim($rowArr[2]);
                    $pass = $rowArr[3];
                    $comment = trim($rowArr[4]);
                    $group_nr = trim($rowArr[5]) ?: '0';
                    $groupNumbers[$group_nr] = (array_key_exists((int) $group_nr, $groupNumbers) ? $groupNumbers[$group_nr] + 1 : 1);

                    if (!$firstName || !$lastName || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $errors[] = 'Viga real ' . $rowNr . '. Väljad firstname, lastname ja email on kohustuslikud!';
                    }

                    if (!is_numeric($group_nr)) {
                        $errors[] = 'Viga real ' . $rowNr . '. Grupi number peab olema täisarv!';
                    }

                    $users[] = ['firstName' => $firstName, 'lastName' => $lastName, 'email' => $email, 'pass' => $pass, 'comment' => $comment, 'group_nr' => $group_nr];
                }

                $rowNr++;
            }

            if ($errors) {
                error_log('User import error: ' . print_r($errors, true));
                $this->redirect('import',[],['csv_import' => $input, 'errors' => $errors]);
            }

            Core_Session::setVar('import-users', $users);
            Core_Session::setVar('import-group_numbers', $groupNumbers);
            $this->redirect('import-preview');
        }
    }

    public function getImportPreview() {
        $this->authorized(['admin']);

        $users = Core_Session::getVar('import-users');
        $groupNumbers = Core_Session::getVar('import-group_numbers');
        $allGroups = [];

        ksort($groupNumbers);

        if ($users) {
            $yearId = Core_Session::getVar('selected_year');
            $allGroups = GroupModel::getAllGroups($yearId);
        }

        $data['users'] = $users;
        $data['group_numbers'] = $groupNumbers;
        $data['groups'] = $allGroups;

        $data['content'] = 'import/preview.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['post_url'] = '/' . $this->router->route('import_preview_post');
        $data['page'] = 'admin';

        return twigRender($data);
    }

    public function postImportPreview() {
        $groups = [];
        $users = [];
        $emails = [];
        $log = [];

        foreach ($_POST as $key => $val) {
            $arr = explode('_', $key);

            if ($arr[0] == 'group') {
                $groups[$arr[1]] = $val;
            } elseif ($arr[0] == 'firstname') {
                $users[$arr[1]]['first_name'] = $val;
            } elseif ($arr[0] == 'lastname') {
                $users[$arr[1]]['last_name'] = $val;
            } elseif ($arr[0] == 'email') {
                $users[$arr[1]]['email'] = $val;
                $emails[] = $val;
            } elseif ($arr[0] == 'password') {
                try {
                    $users[$arr[1]]['passwd'] = AuthModel::encryptPassword($val);
                } catch (Exception) {
                    error_log('Failed to encrypt password on user import.');
                }

            } elseif ($arr[0] == 'comment') {
                $users[$arr[1]]['comments'] = $val;
            } elseif ($arr[0] == 'usergroup') {
                $users[$arr[1]]['group_id'] = $groups[$val];
            }
        }

        // Check if any of the e-mails already exists in database
        $alreadyExistingEmailsArr = ImportModel::findEmailsAlreadyInDb($emails);
        $alreadyExistingEmails = [];

        foreach ($alreadyExistingEmailsArr as $email) {
            $alreadyExistingEmails[] = $email['email'];
        }

        foreach ($users as $user) {
            $nameAndEmail = $user['first_name'] . ' ' . $user['last_name'] . ' (' . $user['email'] . ')';

            if (!filter_var($user['email'], FILTER_VALIDATE_EMAIL)) {
                $log[$user['email']]['class'] = 'text-danger';
                $log[$user['email']]['msg'] = $nameAndEmail . ' - kasutaja lisamine ebaõnnestus (ebasobiv email)!';
                continue;
            }

            if (!in_array($user['email'], $alreadyExistingEmails)) {
                // Add user account
                $createdBy = $this->currentUserId();
                $user['locked'] = 0;
                $user['created_by'] = $createdBy;
                $userId = UserModel::createUser($user);

                if ($userId > 0) {
                    $isAddedToGroup = GroupModel::addUserToGroup($user['group_id'],$userId,5, $createdBy);
                    $log[$user['email']]['class'] = 'fw-bold text-success';
                    $log[$user['email']]['msg'] = $nameAndEmail . ' - kasutaja edukalt lisatud.';

                    if (!$isAddedToGroup) {
                        $log[$user['email']]['class'] = 'fw-bold text-warning';
                        $log[$user['email']]['msg'] = $nameAndEmail . ' - kasutaja lisatud aga grupi määramine ebaõnnestus';
                    }
                } else {
                    $log[$user['email']]['class'] = 'fw-bold text-danger';
                    $log[$user['email']]['msg'] = $nameAndEmail . ' - kasutaja lisamine ebaõnnestus!';
                }
            } else {
                $log[$user['email']]['class'] = 'text-muted';
                $log[$user['email']]['msg'] = $nameAndEmail . ' - selle e-mailiga kasutaja on juba olemas.';
            }
        }

        Core_Session::setVar('import-log', $log);
        $this->redirect('import_results');

    }

    public function getImportResults() {
        $this->authorized(['admin']);

        $logs = Core_Session::getVar('import-log');
        Core_Session::unsetVar('import-log');

        $data['logs'] = $logs;

        $data['content'] = 'import/results.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['page'] = 'admin';

        return twigRender($data);
    }

    public function getTemplateFile()
    {
        $file = 'import_template.csv';

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            flush(); // Flush system output buffer
            readfile($file);
        } else {
            http_response_code(404);
        }
        die();
    }
}