<?php
require __DIR__ . '/../../preventDirectAccess.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Session.php';
require APPLICATION_PATH . DS . 'src' . DS . 'controllers' . DS . 'MenuController.php';

use Phroute\Phroute\RouteCollector;

class BaseController
{
    public Core_Session $sess;
    public RouteCollector $router;
    public array $menu;
    public string $subdir;

    public function __construct()
    {

        global $router;
        global $config;

        $this->sess = new Core_Session();
        $this->router = $router;

        $menuObj = new MenuController();
        $menu['links'] = $menuObj->getMenu();
        $menu['template'] = 'menu.twig';
        $this->menu = $menu;

        $this->subdir = $config['SITE_SUBDIR'];
    }

    /**
     * Sets header for redirection and exits (unless $continue = true). Also adds/deletes global variables if specified.
     * @param string $key - route name
     * @param array $routeVars - add route variables if any ['value1', 'value2']
     * @param array $addVars - add/update global vars ['name' => 'value']
     * @param array $delVars - delete global vars ['name1', 'name2']
     * @param bool $continue - if false, exit() is run
     */
    public function redirect(string $key, array $routeVars = [], array $addVars = [], array $delVars = [],
                             bool $continue = false): void {
        global $router;

        if ($addVars) {
            foreach ($addVars as $k => $v) {
                Core_Session::setVar($k, $v);
            }
        }

        if ($delVars) {
            foreach ($addVars as $key) {
                Core_Session::unsetVar($key);
            }
        }

        if ($routeVars) {
            header('location: /' . $router->route($key, $routeVars));
        } else {
            header('location: /' . $router->route($key));
        }

        if (!$continue) {
            exit();
        }
    }

    /**
     * If participation is disabled, redirect to error page
     * @return void
     */
    public function ifDisabledRedirect(): void
    {
        global $config;

        if ($config['PARTICIPATION_DISABLED']) {
            $this->redirect('error', [], ['error_msg' => 'See funktsioon on välja lülitatud!']);
        }
    }

    /**
     * Check if logged-in user has any of the following roles
     * @param array $roles - list of roles
     * @return bool - returns true if the user has at least one of the roles
     */
    public function hasRole(array $roles): bool {
        $userId = $this->sess->getVar('user_id');

        if (!$userId) {
            return false;
        }

        $roleNr = BaseModel::hasNrOfRoles($userId, $roles);

        return $roleNr > 0;
    }

    public function authorized(array $roles, string $redirect = 'error', string $error = 'Sul puudub õigus selle lehe vaatamiseks'): void
    {
        if (!$this->hasRole($roles)) {
            $this->redirect($redirect, [], ['error_msg' => $error]);
        }
    }

    public function currentUserId(bool $redirectIfNotLoggedIn = true): int {
        $userId = Core_Session::getVar('user_id');

        if (!$userId) {
            $this->redirect('login', [], ['login_error' => 'Session on aegunud. Te peate uuesti sisse logima!']);
        }

        return $userId;
    }
}