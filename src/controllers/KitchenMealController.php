<?php
require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'KitchenMealModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealTypeModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'KitchenViewModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'MealType.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'IrregularDatesModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealModel.php';



class KitchenMealController extends BaseController
{
    public function getRandomKitchenMeal()
    {
        $this->authorized(['admin', 'observer', 'cook']);
        $mealType = KitchenMealModel::getFirstMealType();
        $date = Core_Date::now('Y-m-' . '01');

        if (!$mealType) {
            $this->redirect('error', [], ['error_msg' => 'Ühtegi sobivat söögikorda ei leitud!']);
        }

        return $this->getKitchenMeal((int)$mealType[0]['id'], $date, $mealType);
    }

    public function getKitchenMeal(int $mealTypeId, string $date = '', array $mealType = null)
    {
        if (!$mealType) {
            $this->authorized(['admin', 'observer', 'cook']);
            $mealType = KitchenMealModel::getMealTypeById($mealTypeId);
            $date = $date ?: Core_Date::now();
        }

        if (!$mealType) {
            $this->redirect('error', [], ['error_msg' => 'Ühtegi sobivat söögikorda ei leitud!']);
        }

        try {
            $from = Core_Date::firstDayOfMonth($date);
            $until = Core_Date::lastDayOfMonth($date);
            $nextMonth = Core_Date::add($until, 'P1D')->format('Y-m-d');
            $previousMonth = Core_Date::sub($from, 'P1D')->format('Y-m-d');
            $yearMonth = Core_Date::get($date, 'Y-m-');
        } catch (Exception) {
            $this->redirect('error', [], ['error_msg' => 'Viga: Kuupäevaga paistab mingi probleem olevat!']);
        }

        $leftLinks = [];
        $yearId = Core_Session::getVar('selected_year');
        $mealTypes = MealTypeModel::getAllMealTypes($yearId);
        foreach ($mealTypes as $mT) {
            $leftLinks[] = ['name' => $mT[MealType::NAME], 'url' => '/' . $this->router->route('kitchen-meal', [$mT['id'], $date])];
        }
        $data['left_links'] = $leftLinks;

        $mealTypeObj = MealTypeModel::createMealTypeObj($mealType[0]);

        $data['title'] = $mealTypeObj->getName();
        $allDaysInMonth = IrregularDatesModel::daysInMonthUpdatedByDb($mealTypeId, $mealTypeObj->getBusinessDays(), $date);
        $data['all_daynames'] = Core_Date::getWeekdayNamesShort();

        $kitchenMeals = KitchenMealModel::getKitchenMealTotalsByMealTypeId($mealTypeObj->getId(), $from, $until, $yearId);
        $mealTotals = [];
        $data['is_current_month'] = Core_Date::now('m') == Core_Date::get($date, 'm');
        $data['current_day'] = Core_Date::now('d');
        foreach ($kitchenMeals as $kitchenMeal) {
            $mealDate = null;
            if ($kitchenMeal['meal_date']) {
                $mealDate = (int)substr($kitchenMeal['meal_date'], -2);
            }

            $kitchenViewId = $kitchenMeal['id'];
            if ($mealDate) {
                if (array_key_exists($mealDate, $allDaysInMonth) && $allDaysInMonth[$mealDate]['is_enabled']) {
                    // should i check if key exists? (or is it faster to overwrite it on every iteration?
                    if (!array_key_exists($kitchenViewId, $mealTotals)) {
                        $mealTotals[$kitchenViewId]['kvName'] = $kitchenMeal['kitchen_view_name'];
                    }

                    $mealTotals[$kitchenViewId]['dates'][$mealDate] = $kitchenMeal['total'];
                }
            } else {
                $mealTotals[$kitchenViewId]['kvName'] = $kitchenMeal['kitchen_view_name'];
                $mealTotals[$kitchenViewId]['dates'] = [];
            }
        }

        $data['first_weekday'] = -1;
        // $data['business_days'] = [];
        $businessDays = [];
        $dayNames = [];
        $colTotals = [];
        $grandTotal = 0;
        $yearMonth = Core_Date::get($date, 'Y-m-');
        foreach ($allDaysInMonth as $day => $enabled) {
            $dayDate = Core_Date::get($yearMonth . $day, 'Y-m-d');
            $dayName = Core_Date::getDayName($dayDate, true);
            if ($enabled['is_enabled']) {
                $colTotals[$day] = 0;
                foreach (array_keys($mealTotals) as $key) {
                    if (!array_key_exists('total', $mealTotals[$key])) {
                        $mealTotals[$key]['total'] = 0;
                    }
                    if (!array_key_exists($day, $mealTotals[$key]['dates'])) {
                        $mealTotals[$key]['dates_sorted'][$day] = 0;
                    } else {
                        $nr = $mealTotals[$key]['dates'][$day];
                        $mealTotals[$key]['dates_sorted'][$day] = $nr;
                        $mealTotals[$key]['total'] = $mealTotals[$key]['total'] + $nr;
                        $colTotals[$day] = $colTotals[$day] + $nr;
                        $grandTotal = $grandTotal + $nr;
                    }
                }

                $businessDays[$day] = $dayName;
                if ($data['first_weekday'] == -1) {
                    $data['first_weekday'] = Core_Date::dayOfWeek($dayDate);
                }

                if (!in_array($dayName, $dayNames)) {
                    $dayNames[] = $dayName;
                }
            }
        }

        $data['meal_totals'] = $mealTotals;
        $data['business_days'] = $businessDays;
        $data['day_names'] = MealModel::sortDayNamesShort($dayNames);
        $data['month_year'] = Core_Date::getMonthName($date) . ' ' . Core_Date::get($date, 'Y');
        $data['col_totals'] = $colTotals;
        $data['grand_total'] = $grandTotal;
        $data['link_next'] = '/' . $this->router->route('kitchen-meal', [$mealTypeId, $nextMonth]);
        $data['link_previous'] = '/' . $this->router->route('kitchen-meal', [$mealTypeId, $previousMonth]);
        $data['content'] = 'kitchen-meal/index.twig';
        $data['sidenav'] = 'left_menu.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['page'] = 'kitchen-meal';

        return twigRender($data);
    }
}