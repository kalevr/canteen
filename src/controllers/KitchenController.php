<?php
require_once APPLICATION_PATH . DS . 'config' . DS . 'client_conf.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Database.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'KitchenModel.php';

class KitchenController extends BaseController
{
    public function getKitchens() {
        $this->authorized(['admin']);

        $kitchens = KitchenModel::getAllKitchens();

        foreach ($kitchens as $key => $kitchen) {
            $kitchens[$key]['days_off_str'] = '';

            if ($kitchen['days_off']) {
                $kitchens[$key]['days_off_str'] = $this->getDaysOffDaysStr(explode(',', $kitchen['days_off']));
            }

            $kitchens[$key]['edit_url'] = '/' . $this->router->route('kitchens_edit', [$kitchen['id']]);
        }

        $data['kitchens'] = $kitchens;
        $data['content'] = 'kitchen/kitchens.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];

        return twigRender($data);
    }

    public function getEdit(int $kitchenId) {
        $this->authorized(['admin']);

        $kitchenInDb = KitchenModel::getKitchenById($kitchenId);

        if (empty($kitchenInDb)) {
            $this->redirect('error', [], ['error_msg' => 'Sellist kööki ei leitud!']);
        }

        $data['kitchen'] = $kitchenInDb[0];

        $data = $this->getInitialDataForKitchen($data, false);

        return twigRender($data);
    }

    public function postCreateEdit() {
        $this->authorized(['admin']);

        // get form data
        $kitchenId = Core_Util::param('id');
        $kitchenName = Core_Util::param('kitchen_name');
        $daysOff = Core_Util::param('days_off') ?: [];

        // redirection rules (edit or create)
        $routeKey = $kitchenId ? 'kitchens_edit' : 'kitchens_create';
        $routeVar = $kitchenId ? [$kitchenId] : [];

        if (empty($kitchenName)) {
            Core_Session::setVar('kitchen_name_error', 'Köögi nimi on kohustuslik (kuigi ebatähtis)');
            $this->redirect($routeKey, $routeVar, ['kitchen_name' => $kitchenName, $daysOff => $daysOff]);
        }

        $isSuccessful = false;
        // if kitchenId is set, then we are editing, if not, we are creating new
        if ($kitchenId) {
            $res = KitchenModel::updateKitchen($kitchenId, $kitchenName, $daysOff);

            $isSuccessful = $res;
        } else {
            error_log('Adding a new kitchen is not yet implemented!');
        }

        if ($isSuccessful) {
            $this->redirect('kitchens');
        } else {
            $this->redirect($routeKey, $routeVar, ['kitchen_name' => $kitchenName, 'days_off' => $daysOff,
                'general_error' => 'Köögi salvestamine ebaõnnestus']);
        }
    }

    private function getInitialDataForKitchen(array $data = [], bool $isNew = true) {
        $data['content'] = 'kitchen/edit.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['post_url'] = '/' . $this->router->route('kitchens_edit_post');
        $data['page'] = 'admin';

        if ($isNew) {
            $data['kitchen'] = [];
        }

        // get data from session
        $kitchenName = Core_Session::getVar('kitchen_name');
        $daysOff = Core_Session::getVar('days_off');
        $kitchenNameError = Core_Session::getVar('kitchen_name_error');
        $daysOffError = Core_Session::getVar('days_off_error');
        $generalError = Core_Session::getVar('general_error');

        if ($kitchenName) {
            $data['kitchen']['kitchen_name'] = $kitchenName;
        }

        if ($daysOff) {
            $data['kitchen']['days_off'] = $daysOff;
        }

        if ($kitchenNameError) {
            $data['kitchen']['kitchen_name_error'] = $kitchenNameError;
        }

        if ($daysOffError) {
            $data['kitchen']['days_off_error'] = $daysOffError;
        }

        if ($generalError) {
            $data['general_error'] = $generalError;
        }

        Core_Session::massUnsetVar(['kitchen_name', 'days_off', 'kitchen_name_error', 'days_off_error', 'general_error']);

        $data['edit'] = !$isNew;

        return $data;
    }

    /**
     * Return days off as short str (E, T, K, N, R, L, P)
     * @param array $daysOff - array with numbers (1-7)
     * @param string $sep - what to separate the weekdays in final string with
     * @return string
     */
    private function getDaysOffDaysStr(array $daysOff, string $sep = ' '): string
    {
        $resultArr = [];
        $daysOfWeek = [1 => 'E', 2 => 'T', 3 => 'K', 4 => 'N', 5 => 'R', 6 => 'L', 7 => 'P'];

        if (empty($daysOff)) {
            return '';
        }

        foreach ($daysOff as $dayOff) {
            $resultArr[] = $daysOfWeek[$dayOff];
        }

        return implode($sep, $resultArr);
    }
}