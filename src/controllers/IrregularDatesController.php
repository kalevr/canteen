<?php

require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'IrregularDatesModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealTypeModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Date.php';

class IrregularDatesController extends BaseController
{
    /**
     * Return irregular dates in the given timeframe
     * @param string $date - date of the month being viewed
     * @return string - returns view HTML (template)
     * @throws Exception
     */
    public function getIrregularDates(int $mealTypeId, string $date = ''): string
    {
        $this->authorized(['admin', 'observer']);

        // if date is not specified, use today
        if (!$date) {
            $date = Core_Date::now();
        }

        $yearId = Core_Session::getVar('selected_year');
        $allMealTypes = MealTypeModel::getAllMealTypes($yearId);

        if (empty($allMealTypes)) {
            $this->redirect('error', [], ['error_msg' => 'Ühtegi söögikorda ei leitud. Lisa kõigepealt mõni (aktiivne) söögikord!']);
        }

        if ($mealTypeId == 0) {
            $mealTypeId = $allMealTypes[0]['id'];
        }

        $mealTypes = [];
        $businessDays = [];

        foreach ($allMealTypes as $mealType) {
            $mealTypes[] = ['name' => $mealType['meal_type_name'],
                'url' => '/' . $this->router->route('irregulars', [$mealType['id'], $date])];

            if ($mealTypeId == $mealType['id']) {
                $data['meal_name'] = $mealType['meal_type_name'];
                $businessDays = explode(',', $mealType['business_days']);
            }
        }

        $start = Core_Date::firstDayOfMonth($date);
        $end = Core_Date::lastDayOfMonth($date);
        $nextMonth = Core_Date::add($end, 'P1D')->format('Y-m-d');
        $previousMonth = Core_Date::sub($start, 'P1D')->format('Y-m-d');

        $irregulars = IrregularDatesModel::getAllIrregularDates($mealTypeId, $start, $end);

        $totalMeals = IrregularDatesModel::getTotalMealsInMonthByDate($mealTypeId, $start, $end);
        $dates = IrregularDatesModel::daysInMonthUpdatedByDb($mealTypeId, $businessDays, $date, $irregulars, $totalMeals);

        $data['meal_types'] = $mealTypes;
        $data['meal_id'] = $mealTypeId;
        $data['first_weekday'] = Core_Date::dayOfWeek($start);
        $data['month_year'] = Core_Date::getMonthName($date) . ' ' . Core_Date::get($date, 'Y');
        $data['link_next'] = '/' . $this->router->route('irregulars', [$mealTypeId, $nextMonth]);
        $data['link_previous'] = '/' . $this->router->route('irregulars', [$mealTypeId, $previousMonth]);
        $data['irregulars'] = $dates;
        $data['date'] = $start;
        $data['content'] = 'irregular/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['post_url'] = '/' . $this->router->route('irregulars_post');
        $data['page'] = 'admin';

        return twigRender($data);
    }

    public function postIrregularDates()
    {
        global $config;

        $this->authorized(['admin', 'observer']);

        $date = Core_Util::param('irregular_date');
        $mealTypeId = (int) Core_Util::param('meal_id');

        if (!$mealTypeId) {
            $this->redirect('error', [], ['error_msg' => 'Erandi salvestamine ebaõnnestus. Söögikorda ei leitud.']);
        }

        $mealType = MealTypeModel::getMealTypeById($mealTypeId);

        if (empty($mealType)) {
            $this->redirect('error', [], ['error_msg' => 'Erandi salvestamine ebaõnnestus. Sellise ID-ga söögikorda ei leitud.']);
        }
        $businessDays = explode(',', $mealType[0]['business_days']);

        $dateObj = Core_Date::get($date);
        $yearMonthPrefix = $dateObj->format('Y-m-');
        $start = Core_Date::firstDayOfMonth($date);
        $end = Core_Date::lastDayOfMonth($date);
        $irregulars = IrregularDatesModel::getAllIrregularDates($mealTypeId, $start, $end);

        $dates = IrregularDatesModel::daysInMonthUpdatedByDb($mealTypeId, $businessDays, $date, $irregulars);

        foreach ($_POST as $k => $v) {
            if (str_starts_with($k, 'date_')) {
                $day = explode('_', $k)[1];
                $fullDate = Core_Date::get($yearMonthPrefix . $day, 'Y-m-d');
                $weekdayNr = Core_Date::dayOfWeek($fullDate);

                if (($v && !$dates[$day]['is_enabled'] && !in_array($weekdayNr, $businessDays)) ||
                    (!$v && $dates[$day]['is_enabled'] && in_array($weekdayNr, $businessDays))) {
                    // add to db
                    IrregularDatesModel::addIrregularDate($mealTypeId, $fullDate, $v, $this->currentUserId());
                } elseif (($v && !$dates[$day]['is_enabled'] && in_array($weekdayNr, $businessDays)) ||
                    (!$v && $dates[$day]['is_enabled'] && !in_array($weekdayNr, $businessDays))) {
                    // remove from db
                    IrregularDatesModel::deleteIrregularDate($fullDate, $this->currentUserId());
                }
            }
        }

        $deleteMeals = Core_Util::param('delete_meals');

        if ($deleteMeals) {
            IrregularDatesModel::deleteAllMealsOnDates($deleteMeals, $this->currentUserId());
        }

        $this->redirect('irregulars', [$date]);
    }
}