<?php
require __DIR__ . '/../../preventDirectAccess.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'KitchenViewModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'GroupModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealTypeModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'controllers' . DS . 'MenuController.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Util.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'KitchenView.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'Logger.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'LogRow.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'AcademicYear.php';

class KitchenViewController extends BaseController
{
    public function getKitchenViews()
    {
        $this->authorized(['admin', 'observer']);

        $data['content'] = 'kitchen-view/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['create_url'] = '/' . $this->router->route('kitchen-views_create');
        $data['page'] = 'admin';

        $yearId = AcademicYear::getCurrent();
        $kitchenViewsInDb = KitchenViewModel::getAllKitchenViews($yearId);
        $kitchenViews = [];

        foreach ($kitchenViewsInDb as $kv) {
            $kitchenView = KitchenViewModel::createKitchenViewObj($kv);
            $kitchenView->setEditUrl('/' . $this->router->route('kitchen-views_edit', [$kitchenView->getId()]));
            $kitchenViews[] = $kitchenView;
        }

        $data['kitchen_views'] = $kitchenViews;

        return twigRender($data);
    }

    public function getEdit(int $kitchenViewId)
    {
        $this->authorized(['admin', 'observer']);

        $kitchenViewInDb = KitchenViewModel::getKitchenViewById($kitchenViewId);
        if (empty($kitchenViewInDb)) {
            $this->redirect('error', [], ['error_msg' => 'Sellist köögi vaadet ei leitud!']);
        }

        $groupsInKv = KitchenViewModel::getIdsOfGroupsInKitchenView($kitchenViewInDb[0][KitchenView::ID]);

        $kitchenView = KitchenViewModel::createKitchenViewObj($kitchenViewInDb[0], $groupsInKv);

        $data = $this->getInitialData($kitchenView);

        $data['kitchenView'] = $kitchenView;
        $data['groups'] = $kitchenView->getGroups();
        $data['edit'] = true;

        return twigRender($data);
    }

    public function getCreate()
    {
        $this->authorized(['admin', 'observer']);
        $kitchenView = new KitchenView('');

        $data = $this->getInitialData($kitchenView);
        $data['kitchenView'] = $kitchenView;
        $data['edit'] = false;

        return twigRender($data);
    }

    public function postCreateEdit()
    {
        $this->authorized(['admin', 'observer']);
        $id = (int)Core_Util::param(KitchenView::ID);
        $kitchenView = new KitchenView(Core_Util::param(KitchenView::NAME));

        if ($id) {
            $kitchenView->setId(Core_Util::param(KitchenView::ID));
        }

        $this->getPostData($kitchenView);

        $errors = $kitchenView->getAllErrors();
        $groupsData = [];

        $routeKey = $id ? 'kitchen-views_edit' : 'kitchen-views_create';
        $routeVar = $id ? [$id] : [];

        if ($kitchenView->getGroups()) {
            $groupsData = GroupModel::getGroupsById($kitchenView->getGroups());
        }

        if ($errors) {
            foreach ($errors as $key => $error) {
                Core_Session::setVar($key, $error);
            }

            $this->redirect($routeKey, $routeVar, [
                KitchenView::NAME => $kitchenView->getKitchenViewName(),
                KitchenView::MEAL_TYPE_ID => $kitchenView->getMealTypeId(),
                KitchenView::STARTING_FROM => $kitchenView->getStartingFrom(),
                KitchenView::VALID_UNTIL => $kitchenView->getValidUntil() ?: '',
                KitchenView::SORT_ORDER => $kitchenView->getSortOrder() ?: ''
            ]);
        }

        // if kitchenView id is set, then we are editing, if not, we are creating new
        if ($id) {
            $kitchenViewInDb = KitchenViewModel::getKitchenViewById($id);
            if (!$kitchenViewInDb) {
                $this->redirect('error', [], ['error_msg' => 'Sellise ID-ga köögi vaadet ei leitud!']);
            }
            $kvDbObj = KitchenViewModel::createKitchenViewObj($kitchenViewInDb[0]);
            $logger = new Logger();

            $isSuccessful = $this->updateKitchenView($kvDbObj, $kitchenView, $logger);

            if ($isSuccessful) {
                $groupsAlreadyInKitchenView = KitchenViewModel::getIdsOfGroupsInKitchenView($id);
                $this->updateKitchenViewGroups($id, $groupsData, $groupsAlreadyInKitchenView);
                $this->redirect('kitchen-views');
            }
        } else {
            // adding new kitchenView
            $yearId = AcademicYear::getCurrent();
            $kitchenView->setAppYear($yearId);
            $id = $this->insertKitchenView($kitchenView);

            if ($id > 0) {
                $this->insertKitchenViewGroups($id, $groupsData);
                $this->redirect('kitchen-views');
            }
        }

        $this->redirect($routeKey, $routeVar, [
            KitchenView::NAME => $kitchenView->getKitchenViewName(),
            KitchenView::MEAL_TYPE_ID => $kitchenView->getMealTypeId(),
            KitchenView::STARTING_FROM => $kitchenView->getStartingFrom(),
            KitchenView::VALID_UNTIL => $kitchenView->getValidUntil(),
            'general_error' => 'Köögivaate salvestamine ebaõnnestus!'
        ]);
    }

    private function getInitialData(KitchenView $kitchenView): array
    {
        $data['content'] = 'kitchen-view/edit.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['post_url'] = '/' . $this->router->route('kitchen-views_edit_post');
        $data['groups_api_url'] = '/' . $this->router->route('api_groups_search');
        $data['page'] = 'admin';

        // get data from session
        $kitchenView->setNameError(Core_Session::getVar(KitchenView::NAME_ERROR));
        $kitchenView->setMealTypeError(Core_Session::getVar(KitchenView::MEAL_TYPE_ID_ERROR));
        $kitchenView->setStartingFromError(Core_Session::getVar(KitchenView::STARTING_FROM_ERROR));
        $kitchenView->setValidUntilError(Core_Session::getVar(KitchenView::VALID_UNTIL_ERROR));
        $kitchenView->setSortOrderError(Core_Session::getVar(KitchenView::SORT_ORDER_ERROR));
        $kitchenView->setGroupsError(Core_Session::getVar(KitchenView::GROUPS_ERROR));

        $name = Core_Session::getVar(KitchenView::NAME);
        $mealTypeId = Core_Session::getVar(KitchenView::MEAL_TYPE_ID);
        $startingFrom = Core_Session::getVar(KitchenView::STARTING_FROM);
        $validUntil = Core_Session::getVar(KitchenView::VALID_UNTIL);
        $sortOrder = Core_Session::getVar(KitchenView::SORT_ORDER);
        $groups = Core_Session::getVar(KitchenView::GROUPS);

        if ($name) {
            $kitchenView->setKitchenViewName($name);
        }

        if ($mealTypeId) {
            $kitchenView->setMealTypeId($mealTypeId);
            $data['selected_mealType'] = $mealTypeId;
        } else {
            $data['selected_mealType'] = $kitchenView->getMealTypeId();
        }

        if ($startingFrom) {
            try {
                $kitchenView->setStartingFrom($startingFrom);
            } catch (Exception) {
                $kitchenView->setStartingFromError("VIGA: Kuupäeva formaat vale");
            }
        } else {
            $kitchenView->setStartingFrom(Core_Date::now());
        }

        if ($sortOrder) {
            try {
                $kitchenView->setSortOrder($sortOrder);
            } catch (Exception) {
                $kitchenView->setSortOrderError("VIGA: Jrk. nr peab olema positiivne täisarv!");
            }
        }

        if ($validUntil) {
            try {
                $kitchenView->setValidUntil($validUntil);
            } catch (Exception) {
                $kitchenView->setValidUntilError("VIGA: Kuupäeva formaat vale");
            }
        }

        $data['groups'] = $groups ? GroupModel::getGroupsById($groups) : "";

        $yearId = Core_Session::getVar('selected_year');
        $data['meal_types'] = MealTypeModel::getAllMealTypes($yearId);

        if (!$data['meal_types']) {
            $kitchenView->setMealTypeError("VIGA: Ühtegi söögikorda ei leitud");
        }

        // delete form session variables
        Core_Session::massUnsetVar([
            KitchenView::NAME,
            KitchenView::MEAL_TYPE_ID,
            KitchenView::STARTING_FROM,
            KitchenView::VALID_UNTIL,
            KitchenView::SORT_ORDER,
            KitchenView::NAME_ERROR,
            KitchenView::MEAL_TYPE_ID_ERROR,
            KitchenView::STARTING_FROM_ERROR,
            KitchenView::VALID_UNTIL_ERROR,
            KitchenView::SORT_ORDER_ERROR
        ]);

        return $data;
    }

    public function getPostData(KitchenView $kitchenView) {
        $mealTypeId = (int) Core_Util::param(KitchenView::MEAL_TYPE_ID);
        $startingFrom = Core_Util::param(KitchenView::STARTING_FROM);
        $validUntil = Core_Util::param(KitchenView::VALID_UNTIL);
        $sortOrder = Core_Util::param(KitchenView::SORT_ORDER);
        $groups = Core_Util::param(KitchenView::GROUPS);

        try {
            $kitchenView->setMealTypeId($mealTypeId);
        } catch (Exception) {
            $kitchenView->setMealTypeError('Vale söögikord!');
        }

        try {
            $kitchenView->setStartingFrom($startingFrom);
        } catch (Exception) {
            $kitchenView->setStartingFromError('Ebasobiv kuupäev!');
        }

        if ($validUntil){
            try {
                $kitchenView->setValidUntil($validUntil);
            } catch (Exception) {
                $kitchenView->setValidUntilError('Ebasobiv kuupäev!');
            }
        }

        if ($sortOrder) {
            if (Core_Util::isPositiveInteger($sortOrder)) {
                $kitchenView->setSortOrder($sortOrder);
            } else {
                $kitchenView->setSortOrderError('Jrk. nr peab olema positiivne täisarv!');
            }
        }

        if ($groups) {
            $isValid = true;
            foreach ($groups as $group) {
                if (!Core_Util::isPositiveInteger($group)) {
                    $isValid = false;
                }
            }

            if ($isValid) {
                $kitchenView->setGroups($groups);
            } else {
                $kitchenView->setGroupsError('Gruppide valik vigane!');
            }
        }
    }

    public function updateKitchenView(KitchenView $oldKv, KitchenView $newKv, Logger $logger): bool
    {
        if ($oldKv->getId() != $newKv->getId()) {
            return false;
        }

        $ins = [];
        $tblName = 'kitchen_view';
        $changedBy = $this->currentUserId();

        if ($oldKv->getKitchenViewName() != $newKv->getKitchenViewName()) {
            $ins[KitchenView::NAME] = $newKv->getKitchenViewName();
            $logger->addLogRow(new LogRow($tblName, $oldKv->getId(), KitchenView::NAME,
                $oldKv->getKitchenViewName(), $newKv->getKitchenViewName(), $changedBy));
        }

        if ($oldKv->getMealTypeId() != $newKv->getMealTypeId()) {
            $ins[KitchenView::MEAL_TYPE_ID] = $newKv->getMealTypeId();
            $logger->addLogRow(new LogRow($tblName, $oldKv->getId(), KitchenView::MEAL_TYPE_ID,
                $oldKv->getMealTypeId(), $newKv->getMealTypeId(), $changedBy));
        }

        if ($oldKv->getStartingFrom() != $newKv->getStartingFrom()) {
            $ins[KitchenView::STARTING_FROM] = $newKv->getStartingFrom();
            $logger->addLogRow(new LogRow($tblName, $oldKv->getId(), KitchenView::STARTING_FROM,
                $oldKv->getStartingFrom(), $newKv->getStartingFrom(), $changedBy));
        }

        if ($oldKv->getValidUntil() != $newKv->getValidUntil()) {
            $ins[KitchenView::VALID_UNTIL] = $newKv->getValidUntil();
            $logger->addLogRow(new LogRow($tblName, $oldKv->getId(), KitchenView::VALID_UNTIL,
                $oldKv->getValidUntil(), $newKv->getValidUntil(), $changedBy));
        }

        if ($oldKv->getSortOrder() != $newKv->getSortOrder()) {
            $ins[KitchenView::SORT_ORDER] = $newKv->getSortOrder();
            $logger->addLogRow(new LogRow($tblName, $oldKv->getId(), KitchenView::SORT_ORDER,
                $oldKv->getSortOrder(), $newKv->getSortOrder(), $changedBy));
        }

        if (!empty($ins)) {
            $isSuccessful = KitchenViewModel::updateKitchenView($ins, $oldKv->getId());

            if ($isSuccessful) {
                $logger->save();
            }

            return $isSuccessful;
        }

        return true;
    }

    public function updateKitchenViewGroups(int $kitchenViewId, array $groups, array $groupsAlreadyInKitchenView)
    {
        $existingGroups = [];
        $allGroups = [];
        $currentUser = $this->currentUserId();

        foreach ($groupsAlreadyInKitchenView as $groupInKitchenView) {
            $existingGroups[$groupInKitchenView['app_group']] = $groupInKitchenView['group_kv_id'];
        }

        foreach ($groups as $group) {
            if (!in_array($group['id'], array_keys($existingGroups))) {
                KitchenViewModel::addGroupKitchenView($group['id'], $kitchenViewId, $currentUser);
            }

            $allGroups[$group['id']] = 0;
        }


        foreach (array_diff_key($existingGroups, $allGroups) as $val) {
            KitchenViewModel::removeGroupKitchenView($val, $currentUser);
        }
    }

    public function insertKitchenView(KitchenView $kitchenView): int
    {
        $ins = [];
        $createdBy = $this->currentUserId();

        try {
            $ins[KitchenView::APP_YEAR] = $kitchenView->getAppYear();
            $ins[KitchenView::NAME] = $kitchenView->getKitchenViewName();
            $ins[KitchenView::MEAL_TYPE_ID] = $kitchenView->getMealTypeId();
            $ins[KitchenView::STARTING_FROM] = $kitchenView->getStartingFrom();
            if ($kitchenView->getValidUntil()) {
                $ins[KitchenView::VALID_UNTIL] = $kitchenView->getValidUntil();
            }

            $ins[KitchenView::SORT_ORDER] = $kitchenView->getSortOrder();
            $ins[KitchenView::CREATED_BY] = $createdBy;
            return KitchenViewModel::insertKitchenView($ins);
        } catch (Exception) {
            return 0;
        }
    }

    public function insertKitchenViewGroups(int $kitchenViewId, array $groups)
    {
        $currentUser = $this->currentUserId();

        foreach ($groups as $group) {
            KitchenViewModel::addGroupKitchenView($group['id'], $kitchenViewId, $currentUser);
        }
    }
}