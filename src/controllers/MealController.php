<?php
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealTypeModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'MealType.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'IrregularDatesModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'User.php';
require APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'Meal.php';


class MealController extends BaseController
{
    public function getMeals(int $mealTypeId, int $groupId, string $date = ''): string
    {
        global $config;

        $this->authorized(['admin', 'observer', 'cook', 'homeroom-teacher']);

        if (!$date) {
            $date = Core_Date::now();
        }

        $isAdminObserverOrCook = $this->hasRole(['admin', 'observer', 'cook']); // viewing rights
        $isValid = self::isNotAllowedOrInvalidInput($this->currentUserId(), $groupId, $mealTypeId, $isAdminObserverOrCook);

        if (!$isValid) {
            $this->redirect('error');
        }

        $groupMealType = MealModel::groupMealType($mealTypeId, $groupId, $date);

        if (!$groupMealType) {
            $this->redirect('error', [], ['error_msg' => 'Sellel grupil ei ole sellist söögikorda.']);
        }

        try {
            $dateData = self::getDateData($date);
        } catch (Exception) {
            $this->redirect('error', [], ['error_msg' => 'Viga: Kuupäevaga paistab mingi probleem olevat!']);
        }

        $businessDays = explode(',',$groupMealType[MealType::BUSINESS_DAYS]);
        $pageName = $groupMealType['meal_type_name'] . ' - ' . $groupMealType['group_name'];
        $allDaysInMonth = IrregularDatesModel::daysInMonthUpdatedByDb($mealTypeId, $businessDays, $date);
        $businessDays = [];
        $data['all_daynames'] = Core_Date::getWeekdayNamesShort();
        Core_Session::unsetVar('is_my_meal');



        /** @var $users User[] */
        $users = [];

        $meals = MealModel::getMealsForTypeForGroupBetweenDates($mealTypeId, $groupId, $dateData['from'], $dateData['until']);

        foreach ($meals as $meal) {
            $userId = $meal['id'];
            if (!array_key_exists($userId, $users)) {
                $users[$userId] = new User($meal['first_name'], $meal['last_name']);
                $users[$userId]->setId($userId);
                $users[$userId]->setDeletedAt($meal['deleted_at'] ?: '');
                $users[$userId]->setHistoryUrl('/' . $this->router->route('meal_history', [$mealTypeId, $userId, $date]));
            }

            if ($meal['meal_date']) {
                $newMeal = new Meal($mealTypeId, $meal['meal_date']);
                $newMeal->setIsSelected(true);
                $users[$userId]->addMeal($newMeal);
            }
        }

        $dayNames = [];
        $data['first_weekday'] = -1;
        $data['first_weekday_mobile'] = Core_Date::dayOfWeek($dateData['from']);

        foreach ($allDaysInMonth as $k => $v) {
            $day = Core_Date::get($dateData['yearMonth'] . $k, 'Y-m-d');
            $dayName = Core_Date::getDayName($day, true);
            //$kPadded = str_pad($k, 2, '0', STR_PAD_LEFT);

            if ($v['is_enabled'] == 1) {
                $businessDays[$k] = $dayName;

                if ($data['first_weekday'] == -1) {
                    $data['first_weekday'] = Core_Date::dayOfWeek($day);
                }

                if (!in_array($dayName, $dayNames)) {
                    $dayNames[] = $dayName;
                }
            }
        }

        foreach ($users as $user) {
            $user->addEmptyMeals($allDaysInMonth, $mealTypeId);
        }

        if ($isAdminObserverOrCook) {
            $allMtGroupsFromDb = MealModel::getAllMealGroups($mealTypeId, $dateData['from'], $dateData['until']);
        } else {
            $allMtGroupsFromDb = MealModel::getAllMealGroups($mealTypeId, $dateData['from'], $dateData['until'], $this->currentUserId());
        }

        $groups = [];

        foreach ($allMtGroupsFromDb as $mtGroupFromDb) {
            $groups[] = ['name' => $mtGroupFromDb['group_name'],
                'url' => '/' . $this->router->route('meals', [$mealTypeId, $mtGroupFromDb['id'], $date])];
        }

        $data['first_changeable_date'] = MealModel::allowChange(Core_Date::get($date, 'Y-m'),
            $groupMealType['meal_time'], $groupMealType['registration_cutoff']);
        $data['meal_type_comment'] = $groupMealType['comments'];
        $data['business_days'] = $businessDays;
        $data['can_edit'] = $this->hasRole($config['EDIT_ANY_TIME_ROLES']);
        $data['day_names'] = MealModel::sortDayNamesShort($dayNames);
        $data['number_of_weekdays'] = count($dayNames);
        $data['month_year'] = Core_Date::getMonthName($date) . ' ' . Core_Date::get($date, 'Y');
        $data['link_next'] = '/' . $this->router->route('meals', [$mealTypeId, $groupId, $dateData['nextMonth']]);
        $data['link_previous'] = '/' . $this->router->route('meals', [$mealTypeId, $groupId, $dateData['previousMonth']]);
        $data['users'] = $users;
        $data['meal_type_id'] = $mealTypeId;
        $data['group_id'] = $groupId;
        $data['groups'] = $groups;
        $data['date'] = $date;
        $data['post_url'] = '/' . $this->router->route('meals_post');
        $data['content'] = 'meal/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['title'] = $pageName;
        $data['page_name'] = $pageName;
        $data['isMobile'] = Core_Util::isMobileDevice($_SERVER['HTTP_USER_AGENT']);

        return twigRender($data);
    }

    public function postMeals()
    {
        global $config;

        // had to disable the following line (cuz my-meals page also uses this method).
        // $this->authorized(['admin', 'observer', 'cook', 'homeroom-teacher']);
        // instead let's check if user is at least logged in
        if (empty(Core_Session::getVar('my_email'))) {
            $this->redirect('error',[],['error_msg' => 'Teil puudub õigus selle lehe nägemiseks. Kas te olete sisse logitud?']);
        }

        // get general POST data
        $mealTypeId = Core_Util::param('meal_type_id');
        $groupId = Core_Util::param('group_id');
        $date = Core_Util::param('date');
        $currentUserId = $this->currentUserId();
        $isMyMeal = (bool)Core_Session::getVar('is_my_meal');

        if ($isMyMeal) {
            Core_Session::unsetVar('is_my_meal');
        }

        if (empty($mealTypeId) || empty($groupId) || empty($date)) {
            $this->redirect('error', [], ['error_msg' => 'Puudulikud lähteandmed!']);
        }

        $isAdminObserverOrCook = $this->hasRole(['admin', 'observer', 'cook']); // viewing rights
        $isValid = self::isNotAllowedOrInvalidInput($this->currentUserId(), $groupId, $mealTypeId, $isAdminObserverOrCook, $isMyMeal);

        if (!$isValid) {
            $this->redirect('error');
        }

        $groupMealType = MealModel::groupMealType($mealTypeId, $groupId, $date);
        if (!$groupMealType) {
            $this->redirect('error', [], ['error_msg' => 'Sellel grupil ei ole sellist söögikorda.']);
        }

        // take meal dates from $_POST
        $postDates = [];
        foreach ($_POST as $k => $v) {
            if (str_starts_with($k, 'date_')) {
                $data = explode('_', $k);
                $id = $data[1];
                $day = $data[2];

                $postDates[$id]['days'][$day] = $v;
            }
        }

        $businessDays = explode(',',$groupMealType[MealType::BUSINESS_DAYS]);
        $allDaysInMonth = IrregularDatesModel::daysInMonthUpdatedByDb($mealTypeId, $businessDays, $date);
        $disabledDays = [];

        foreach ($allDaysInMonth as $dayInMonth => $value) {
            if (!$value['is_enabled']) {
                $disabledDays[] = $dayInMonth;
            }
        }

        $from = Core_Date::firstDayOfMonth($date);
        $until = Core_Date::lastDayOfMonth($date);
        $yearMonth = Core_Date::get($date, 'Y-m-');
        $firstDay = 0;

        if (!$this->hasRole($config['EDIT_ANY_TIME_ROLES'])) {
            $firstDay = MealModel::allowChange(Core_Date::get($date, 'Y-m'),
                $groupMealType['meal_time'], $groupMealType['registration_cutoff']);
        }

        $dbUsers = [];
        $postUsers = [];
        $mealsInDb = MealModel::getMealsForTypeForGroupBetweenDates($mealTypeId, $groupId, $from, $until);

        // create objects out of DB data (User and Meal)
        foreach ($mealsInDb as $meal) {
            $userId = $meal['id'];
            $mealDate = $meal['meal_date'] ? Core_Date::get($meal['meal_date'], 'd') : null;

            if (!array_key_exists($userId, $dbUsers)) {
                $dbUsers[$userId] = new User();
                $dbUsers[$userId]->setId($userId);
            }

            if ($mealDate && !in_array($mealDate, $disabledDays) && (int) $mealDate >= $firstDay) {
                $newMeal = new Meal($mealTypeId, $meal['meal_date']);
                $dbUsers[$userId]->addMeal($newMeal);
            }
        }

        // create objects out of POST data (User and Meal)
        foreach ($postDates as $uId => $days) {
            if (!array_key_exists($uId, $postUsers)) {
                $postUsers[$uId] = new User();
                $postUsers[$uId]->setId($uId);
            }

            foreach ($days['days'] as $day => $enabled) {
                if ($enabled  && !in_array($day, $disabledDays) && (int) $day >= $firstDay) {
                    $myDate = Core_Date::get($yearMonth . $day, 'Y-m-d');
                    $postUsers[$uId]->addMeal(new Meal($mealTypeId, $myDate));
                }
            }
        }

        // if changing 'my_meal', redirect if trying to change somebody else's meal
        if ($isMyMeal) {
            foreach ($postUsers as $postUser) {
                if ($postUser->getId() != $currentUserId) {
                    error_log('User ' . $currentUserId . ' tried to edit user ' . $userId . ' with "my_meal" (hacking attempt?)');
                    $this->redirect('error', [], ['error_msg' => 'Sul puuduvad õigused ühe (või enama) kasutaja söömiste muutmiseks!']);
                }
            }
        }

        // add meals
        foreach ($postUsers as $postUser) {
            if (array_key_exists($postUser->getId(), $dbUsers)) {
                foreach ($postUser->findUniqueMealsForUser($dbUsers[$postUser->getId()]) as $postMeal) {
                    $postMealDate = $postMeal->getMealDate()->format('Y-m-d');
                    $success = MealModel::registerForMeal($mealTypeId, $postUser->getId(), $postMealDate, $this->currentUserId());
                    if (!$success) {
                        error_log("Adding new meal to DB failed! (mealTypeId = {$mealTypeId}, userId = {$postUser->getId()}, 
                    mealDate = {$postMealDate}, createdBy = {$this->currentUserId()}");
                    }
                }
            }
        }

        // remove meals
        foreach ($dbUsers as $dbUser) {
            if (array_key_exists($dbUser->getId(), $postUsers)) {
                foreach ($dbUser->findUniqueMealsForUser($postUsers[$dbUser->getId()]) as $dbMeal) {
                    $dbMealDate = $dbMeal->getMealDate()->format('Y-m-d');
                    $success = MealModel::unregisterFromMeal($mealTypeId, $dbUser->getId(), $dbMealDate, $this->currentUserId());
                    if (!$success) {
                        error_log("Setting meal to 'deleted' in DB failed! (mealTypeId = {$mealTypeId}, 
                    userId = {$dbUser->getId()}, mealDate = {$dbMealDate}, createdBy = {$this->currentUserId()}");
                    }
                }
            }
        }

        if ($isMyMeal) {
            $this->redirect('my-meals', [$currentUserId, $mealTypeId, $groupId, $date]);
        }

        $this->redirect('meals', [$mealTypeId, $groupId, $date]);
    }

    public function getRandomMyMeal() {
        if (!$this->currentUserId()) {
            $this->redirect('login');
        }
        $userId = $this->currentUserId();

        $mealData = MealModel::findRandomGroupAndMealTypeForUser($userId, false, true);

        if (!$mealData) {
            $this->redirect('error', [], ['error_msg' => 'Viga: Ühtegi söögikorda ei leitud!']);
        }

        return $this->getMyMeal($userId, $mealData['meal_type'], $mealData['app_group']);

    }

    public function getMyMeal(int $userId, int $mealTypeId, int $groupId, string $date = ''): string {
        global $config;

        if (!$this->currentUserId()) {
            $this->redirect('login');
        }

        if (!$date) {
            $date = Core_Date::now();
        }

        $groupMealType = MealModel::groupMealType($mealTypeId, $groupId, $date);

        if (!$groupMealType) {
            $this->redirect('error', [], ['error_msg' => 'Sellel grupil ei ole sellist söögikorda.']);
        }

        try {
            $from = Core_Date::firstDayOfMonth($date);
            $until = Core_Date::lastDayOfMonth($date);
            $nextMonth = Core_Date::add($until, 'P1D')->format('Y-m-d');
            $previousMonth = Core_Date::sub($from, 'P1D')->format('Y-m-d');
            $yearMonth = Core_Date::get($date, 'Y-m-');
        } catch (Exception) {
            $this->redirect('error', [], ['error_msg' => 'Viga: Kuupäevaga paistab mingi probleem olevat!']);
        }

        $businessDays = explode(',',$groupMealType[MealType::BUSINESS_DAYS]);
        $pageName = $groupMealType['meal_type_name'];
        $allDaysInMonth = IrregularDatesModel::daysInMonthUpdatedByDb($mealTypeId, $businessDays, $date);
        $businessDays = [];
        $data['all_daynames'] = Core_Date::getWeekdayNamesShort();

        /** @var $users User[] */
        $users = [];

        $meals = MealModel::getMealsForUserBetweenDates($userId, $mealTypeId, $from, $until);

        foreach ($meals as $meal) {
            $userId = $meal['id'];
            if (!array_key_exists($userId, $users)) {
                $users[$userId] = new User($meal['first_name'], $meal['last_name']);
                $users[$userId]->setId($userId);
            }

            if ($meal['meal_date']) {
                $newMeal = new Meal($mealTypeId, $meal['meal_date']);
                $newMeal->setIsSelected(true);
                $users[$userId]->addMeal($newMeal);
            }
        }

        $dayNames = [];
        $data['first_weekday'] = -1;
        $data['first_weekday_mobile'] = Core_Date::dayOfWeek($from);

        foreach ($allDaysInMonth as $k => $v) {
            $day = Core_Date::get($yearMonth . $k, 'Y-m-d');
            $dayName = Core_Date::getDayName($day, true);
            //$kPadded = str_pad($k, 2, '0', STR_PAD_LEFT);

            if ($v['is_enabled'] == 1) {
                $businessDays[$k] = $dayName;

                if ($data['first_weekday'] == -1) {
                    $data['first_weekday'] = Core_Date::dayOfWeek($day);
                }

                if (!in_array($dayName, $dayNames)) {
                    $dayNames[] = $dayName;
                }
            }
        }

        foreach ($users as $user) {
            $user->addEmptyMeals($allDaysInMonth, $mealTypeId);
        }

        $myMealTypes = MealTypeModel::getAllMealTypesForUser($userId);
        $mealTypes = [];

        foreach ($myMealTypes as $myMealType) {
            $mealTypes[] = ['name' => $myMealType['meal_type_name'],
                'url' => '/' . $this->router->route('my-meals', [$userId, $myMealType['id'], $myMealType['app_group'], $date])];
        }

        Core_Session::setVar('is_my_meal', true);
        $data['get_history_url'] = '/' . $this->router->route('meal_history', [$mealTypeId, $userId, $date]); // currently removed from .twig file
        $data['meal_type_comment'] = $groupMealType['comments'];
        $data['meal_types'] = $mealTypes;
        $data['user_id'] = $userId;
        $data['first_changeable_date'] = MealModel::allowChange(Core_Date::get($date, 'Y-m'),
            $groupMealType['meal_time'], $groupMealType['registration_cutoff']);
        $data['business_days'] = $businessDays;
        $data['can_edit'] = $this->hasRole($config['EDIT_ANY_TIME_ROLES']);
        $data['day_names'] = MealModel::sortDayNamesShort($dayNames);
        $data['number_of_weekdays'] = count($dayNames);
        $data['month_year'] = Core_Date::getMonthName($date) . ' ' . Core_Date::get($date, 'Y');
        $data['link_next'] = '/' . $this->router->route('my-meals', [$userId, $mealTypeId, $groupId, $nextMonth]);
        $data['link_previous'] = '/' . $this->router->route('my-meals', [$userId, $mealTypeId, $groupId, $previousMonth]);
        $data['users'] = $users;
        $data['meal_type_id'] = $mealTypeId;
        $data['group_id'] = $groupId;
        $data['date'] = $date;
        $data['post_url'] = '/' . $this->router->route('meals_post');
        $data['content'] = 'meal/my_meal.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['title'] = $pageName;
        $data['page_name'] = $pageName;
        $data['isMobile'] = true;

        return twigRender($data);
    }

    public function getMealHistoryForUser(int $mealTypeId, int $userId, string $date = '') {
        if ($this->currentUserId() != $userId && !$this->hasRole(['admin', 'observer', 'cook', 'homeroom-teacher'])) {
            $this->redirect('error', [], ['error_msg' => 'Teil puudub õigus selle lehe vaatamiseks!']);
        }

        try {
            $from = Core_Date::firstDayOfMonth($date);
            $until = Core_Date::lastDayOfMonth($date);
            $nextMonth = Core_Date::add($until, 'P1D')->format('Y-m-d');
            $previousMonth = Core_Date::sub($from, 'P1D')->format('Y-m-d');
            $yearMonth = Core_Date::get($date, 'Y-m-');
        } catch (Exception) {
            $this->redirect('error', [], ['error_msg' => 'Viga: Kuupäevaga paistab mingi probleem olevat!']);
        }

        $mealsInDb = MealModel::getMealHistoryForUser($mealTypeId, $userId, $from, $until);
        $meals = [];

        foreach ($mealsInDb as $key => $mealInDb) {
            if ($key == 0) {
                $data['user_full_name'] = $mealInDb['full_name'];
            }
            $mealDate = Core_Date::get($mealInDb['meal_date'], 'd.m.Y');
            $createdAt = Core_Date::get($mealInDb['created_at'], 'd.m.Y H:i:s');

            $meals[$mealDate][$createdAt]['created'] = true;
            $meals[$mealDate][$createdAt]['edited_by'] = $mealInDb['created_by'];

            if ($mealInDb['deleted_at']) {
                $deletedAt = Core_Date::get($mealInDb['deleted_at'], 'd.m.Y H:i:s');
                $meals[$mealDate][$deletedAt]['created'] = false;
                $meals[$mealDate][$deletedAt]['edited_by'] = $mealInDb['deleted_by'];
            }
        }

        foreach ($meals as $meal) {
            krsort($meal);
        }

        $myMealTypes = MealTypeModel::getAllMealTypesForUser($userId);
        $mealTypes = [];

        foreach ($myMealTypes as $myMealType) {
            $mealTypes[] = ['name' => $myMealType['meal_type_name'],
                'url' => '/' . $this->router->route('meal_history', [$myMealType['id'], $userId, $date])];
        }

        $data['meal_types'] = $mealTypes;
        $data['meals'] = $meals;
        $data['user_id'] = $userId;
        $data['month_year'] = Core_Date::getMonthName($date) . ' ' . Core_Date::get($date, 'Y');
        $data['link_next'] = '/' . $this->router->route('meal_history', [$mealTypeId, $userId, $nextMonth]);
        $data['link_previous'] = '/' . $this->router->route('meal_history', [$mealTypeId, $userId, $previousMonth]);
        $data['content'] = 'meal/history.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];

        return twigRender($data);
    }

    /**
     * Is user not allowed to see that info or is input invalid
     * @param int $userId - ID of logged-in user
     * @param int $groupId - ID of group whos data is requested
     * @param bool $isMyMeal - Is logged-in user watching his/her own meal?
     * @param bool $isAdminOrObserver - Does user have admin or observer role?
     * @return bool
     */
    public static function isNotAllowedOrInvalidInput(int  $userId, int $groupId, int $mealTypeId,
                                                       bool $isAdminOrObserver = false, bool $isMyMeal = false): bool
    {
        if (empty($groupId)) {
            Core_Session::setVar('error_msg', 'Antud söögikorrale ei ole ühtegi gruppi veel lisatud');
            return false;
        }

        if (empty($mealTypeId)) {
            Core_Session::setVar('error_msg', 'Teie otsitud lehekülge ei leitud!');
        }

        $isHomeroomTeacher = MealModel::isHomeroomTeacher($userId, $groupId);

        if (!$isAdminOrObserver && !$isHomeroomTeacher && !$isMyMeal) {
            error_log($userId . ' has no right to view meals of group ' . $groupId);
            Core_Session::setVar('error_msg', 'Teil puudub õigus selle lehe vaatamiseks.');
            return false;
        }

        return true;
    }

    public static function getDateData(string $date): array
    {
        $dateData = [];
        $dateData['from'] = Core_Date::firstDayOfMonth($date);
        $dateData['until'] = Core_Date::lastDayOfMonth($date);
        $dateData['nextMonth'] = Core_Date::add($dateData['until'], 'P1D')->format('Y-m-d');
        $dateData['previousMonth'] = Core_Date::sub($dateData['from'], 'P1D')->format('Y-m-d');
        $dateData['yearMonth'] = Core_Date::get($date, 'Y-m-');

        return $dateData;
    }
}