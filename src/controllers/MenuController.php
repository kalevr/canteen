<?php

require_once APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MenuModel.php';

/**
 * Displays top menu. Most data is taken from session variables, so you don't have to make DB queries on every page
 * Pages themselves will check if you have the right to see them.
 */
class MenuController
{
    public function getMenu() {
        global $router;
        global $config;

        $links = [];

        // for showing menu items, we can use session variables to check roles (every page checks from DB when accessed)
        $roles = Core_Session::getVar('role');
        $roles = $roles ?: []; //if there are no roles (logged out), set $roles as empty array
        $isAdmin = in_array('admin', $roles);
        $isObserver = in_array('observer', $roles);
        $isCook = in_array('cook', $roles);
        $isAccountant = in_array('accountant', $roles);
        $hasMeal = Core_Session::getVar('has_meal');
        $participationDisabled = $config['PARTICIPATION_DISABLED'];

        if ($isAdmin || $isObserver) {
            $links[] = ['name' => 'Admin','url' =>'/' . $router->route('admin'), 'page' => 'admin'];
        }

        if ($isAdmin || $isObserver || $isCook) {
            $links[] = ['name' => 'Köök', 'url' => '/' . $router->route('random-kitchen-meal'), 'page' => 'kitchen-meal'];
        }

        if (!$participationDisabled && ($isAdmin || $isCook)) {
            $links[] = ['name' => 'Praegune söögikord', 'url' => '/' . $router->route('current-participations'), 'page' => 'current-participations'];
        }

        if ($hasMeal) {
            $links[] = ['name' => 'Minu söömised', 'url' => '/' . $router->route('random_my-meal'), 'page' => 'my-meals'];
        }

        if ($isAdmin || $isObserver || $isAccountant) {
            $links[] = ['name' => 'Aruanded', 'url' => '/' . $router->route('random_report'), 'page' => 'reports'];
        }

        $mealTypeLinks = Core_Session::getVar('meal_type_links');
        $participationsLinks = Core_Session::getVar('participations_links');

        if ($mealTypeLinks) {
            $links['meal_type_links'] = $mealTypeLinks;
        }

        if ($participationsLinks && !$participationDisabled) {
            $links['participations_links'] = $participationsLinks;
        }

        // if menu link is set, show it
        if (array_key_exists('MENU_URL', $config) && $config['MENU_URL']) {
            $links[] = ['name' => 'Menüü', 'url' => $config['MENU_URL'], 'page' =>'doesntexist', 'blank' => true];
        }

        // show logout if logged in, show login if logged out
        if ($roles) {
            $links[] = ['name' => 'Logout','url' =>'/' . $router->route('logout'), 'page' => 'logout'];
        } else {
            $links[] = ['name' => 'Login','url' =>'/' . $router->route('domain-login'), 'page' => 'login'];
        }

        return $links;
    }
}