<?php
require __DIR__ . '/../../preventDirectAccess.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Util.php';

class ErrorController extends BaseController
{
    public function getError() {
        $data['content'] = 'error.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['error_msg'] = Core_Session::getVar('error_msg');

        return twigRender($data);
}
}