<?php
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'UpdateConfig.php';

class HomeController extends BaseController
{
    public function getHome() {
        global $config;

        $data = [];
        $data['content'] = 'home/index.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['message'] = 'Fly you fools! ';
        $data['img_url'] = $config['SITE_SUBDIR'] . '/img/homepage_img.png';

        return twigRender($data);
    }

    public function getTest() {
        global $config;
        // require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Date.php';

        return '';
    }
}