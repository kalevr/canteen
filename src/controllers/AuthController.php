<?php
require __DIR__ . '/../../preventDirectAccess.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'AuthModel.php';
require APPLICATION_PATH . DS . 'src' . DS . 'models' . DS . 'MealTypeModel.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Util.php';
require_once APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'AcademicYear.php';

class AuthController extends BaseController
{
    public function getLogin(bool $domainLogin = false): string
    {
        $data['content'] = 'auth/login.twig';
        $data['site_subdir'] = $this->subdir;
        $data['menu'] = $this->menu['template'];
        $data['links'] = $this->menu['links'];
        $data['login_error'] = $this->sess->getVar('login_error');
        $data['login_email'] = $this->sess->getVar('login_email');
        $data['domain_login'] = $domainLogin ? "1" : "0";
        $this->sess->unsetVar('login_error');
        $this->sess->unsetVar('login_email');


        $data['post_url'] = '/' . $this->router->route('login_post');
        $data['local_url'] = '/' . $this->router->route('login');
        $data['domain_url'] = '/' . $this->router->route('domain-login');
        $data['page'] = 'login';

        return twigRender($data);
    }

    public function postLogin()
    {
        $error = 'Vale kasutajanimi või parool!';
        $email = Core_Util::param('my_email');
        $passwd = Core_Util::param('passwd');
        $domainLogin = Core_Util::param('domain-login');

        if (empty($email) || empty($passwd)) {
            $this->redirect('login', [], ['login_error' => $error]);
        }

        $this->sess->setVar('login_email', $email);

        $user = AuthModel::getUser($email);
        $ip = Core_Util::getUserIP();
        $userAgent = $_SERVER['HTTP_USER_AGENT'] ?: 'UNKNOWN';
        $userId = count($user) > 0 ? $user['id'] : -1;

        if ($userId > 0) {
            if ($domainLogin) {
                $isAuthenticated = AuthModel::isAuthenticatedDomain($email, $passwd);
            } else {
                $isAuthenticated = AuthModel::isAuthenticatedLocal($user['passwd'] ?: '', $passwd);
            }

            if ($isAuthenticated) {
                $yearId = AcademicYear::getCurrent();

                if ($yearId == -1) {
                    // TODO: if current year not in DB, redirect somewhere to fix it (case: admin is too late with changing to new schoolyear)
                }

                // if user is admin / observer / cook, get all mealtypes, otherwise get only the ones tied to user
                $roles = explode(',', $user['role']);
                $mealTypes = [];
                $mealTypeLinks = [];
                $participationLinks = [];

                if (in_array('admin', $roles) || in_array('observer', $roles) || in_array('cook', $roles)) {
                    $mealTypes = MealTypeModel::getAllMealTypes($yearId);
                } else {
                    $mealTypes = MealTypeModel::getAllMealTypesForUser($user['id'], true, $yearId);
                }

                foreach ($mealTypes as $mealType) {
                    $mealTypeLinks[] = ['name' => $mealType['meal_type_name'],
                        'url' => '/' . $this->router->route('meals', [$mealType['id'], $mealType['app_group'] ?: 0, Core_Date::now()]), 'page' => 'meals'];
                    $participationLinks[] = ['name' => $mealType['meal_type_name'],
                        'url' => '/' . $this->router->route('participations', [$mealType['id'], $mealType['app_group'] ?: 0, Core_Date::now()]), 'page' => 'participations'];
                }

                Core_Session::setVar('meal_type_links', $mealTypeLinks);
                Core_Session::setVar('participations_links', $participationLinks);
                AuthModel::logLoginAttempt($email, $ip, 1, $userAgent, $userId);

                $this->redirect('home', [], [
                    'my_email' => $email,
                    'user_id' => $user['id'],
                    'fullname' => $user['first_name'] . ' ' . $user['last_name'],
                    'role' => explode(',', $user['role']),
                    'has_meal' => $user['has_meal'] > 0,
                    'current_year' => $yearId,
                    'selected_year' => $yearId
                ]);
            }

            // password didn't match
        }
        AuthModel::logLoginAttempt($email, $ip, 0, $userAgent, $userId);

        if ($domainLogin) {
            $this->redirect('domain-login', [], ['login_error' => $error]);
        }

        $this->redirect('login', [], ['login_error' => $error]);
    }

    public function getLogout()
    {
        $this->sess::destroy();
        $this->redirect('home');
    }
}