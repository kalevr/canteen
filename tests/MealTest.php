<?php
declare(strict_types = 1);
use PHPUnit\Framework\TestCase;
const DS = DIRECTORY_SEPARATOR;
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . DS . '..'));
defined('DIRECT_ACCESS') || define('DIRECT_ACCESS', 'not a good idea');
include APPLICATION_PATH . DS . 'src' . DS . 'Canteen' . DS . 'Meal.php';
include APPLICATION_PATH . DS . 'src' . DS . 'core' . DS . 'Core_Date.php';

final class MealTest extends TestCase {
    public function testConstructorSetsValues() {
        // Data
        $mealType = 16;
        $mealDate = '2022-04-09';

        // Given & When
        $meal = new Meal($mealType, $mealDate);

        // Then
        self::assertSame($mealType, $meal->getMealType());
        self::assertSame($mealDate, $meal->getMealDate()->format('Y-m-d'));
    }
}