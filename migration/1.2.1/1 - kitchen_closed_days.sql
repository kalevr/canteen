-- add days_off column to kitchen table (6,7 = Saturday and Sunday)
ALTER TABLE kitchen
    ADD COLUMN days_off VARCHAR(20) AFTER kitchen_name;

-- add comment to the new column
ALTER TABLE kitchen
    MODIFY COLUMN days_off VARCHAR(20) COMMENT 'Weekdays, that the kitchen is not working on (6, 7 = Saturday, Sunday)';


-- remove days_off column from kitchen table
-- ALTER TABLE kitchen DROP COLUMN days_off;