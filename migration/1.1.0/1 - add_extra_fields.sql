-- add business_days field to meal_type table (1-5 = from Monday to Friday)
ALTER TABLE meal_type
    ADD COLUMN business_days VARCHAR(20) DEFAULT ('1,2,3,4,5') AFTER meal_time;

-- add meal_type FK to irregular_dates table
ALTER TABLE irregular_dates
    ADD COLUMN meal_type INT(1) NOT NULL DEFAULT (1) AFTER id,
    ADD CONSTRAINT `fk_irregular_dates_2_meal_type` FOREIGN KEY (`meal_type`) REFERENCES `meal_type` (`id`);


-- remove constraint
-- ALTER TABLE irregular_dates DROP FOREIGN KEY `fk_irregular_dates_2_meal_type`;

-- remove meal_type column from irregular_dates and business_days from meal_type
-- ALTER TABLE irregular_dates DROP COLUMN meal_type;
-- ALTER TABLE meal_type DROP COLUMN business_days;