<?php
$conf = new UpdateConfig();

if (!$conf->checkIfVariableExists('MENU_URL')) {
    $conf->insertEmptyRow(16);
    $conf->insertComment('Menu URL (opens in new window)', 17);
    $conf->insertNewStrVariable('MENU_URL','', 18);
    $conf->saveToFile();
    error_log('UPDATE: Added MENU_URL variable to the conf file.');
}