<?php
$conf = new UpdateConfig();

if (!$conf->checkIfVariableExists('IS_INSTALLED')) {
    $conf->insertNewVariable('IS_INSTALLED', 'true');
    $conf->saveToFile();
} else {
    error_log('UPDATER: IS_INSTALLED variable already present in client_config.php. SKIPPING.');
}